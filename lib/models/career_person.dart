import 'package:jolt/utils/uidata.dart';

class CareerPerson{
  dynamic avatar;
  dynamic fullname;
  dynamic email;
  dynamic phoneNumber;
  dynamic description;
  dynamic city;
  dynamic country;
  dynamic age;
  dynamic businessConnects;
  dynamic userID;
  dynamic session;

  CareerPerson(
    this.avatar,
    this.fullname,
    this.email,
    this.userID,
    this.phoneNumber,
    this.description,
    this.city,
    this.country,
    this.age,
    this.businessConnects,
    this.session
  );
}

//CareerPerson careerPerson = CareerPerson(UIData.victorImage, "Victor Shoaga", "victorshoaga@gmail.com","", "08185476560", "Hardware Engineer", "Victoria Island, Lagos", "Nigeria", "24", "3.5K","career");

