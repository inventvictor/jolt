import 'package:jolt/utils/uidata.dart';

class IntroItem {
  IntroItem({
    this.title,
    this.category,
    this.imageUrl,
  });

  final String title;
  final String category;
  final String imageUrl;
}

final sampleItems = <IntroItem>[
  new IntroItem(title: ' A new chapter, welcome to the team.', category: 'NEW EMPLOYEE', imageUrl: UIData.newEmployee,),
  new IntroItem(title: 'We give our best regardless the situations ahead.', category: 'CURRENT EMPLOYEE', imageUrl: UIData.currentEmployee,),
  new IntroItem(title: 'We strive to be 10x ahead of our followers.', category: 'LEADER', imageUrl: UIData.leader,),
  new IntroItem(title: 'We need to take on new challenges. We are no mediocre.', category: 'EMPLOYMENT TRANSITION', imageUrl: UIData.employTransition,),
];