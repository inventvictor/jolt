class JobModel {
  String description, iconUrl, location, salary, title, url;

  JobModel(
      {this.description,
      this.iconUrl,
      this.location,
      this.salary,
      this.title,
      this.url});
}