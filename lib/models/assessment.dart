class Assessment{
  int id;
  String topQuestion;
  String bottomQuestion;
  Map<String, bool> answer;

  Assessment(
    this.id,
    this.topQuestion,
    this.bottomQuestion,
    this.answer
  );
}