class CareerAdvice{
  int id;
  String title;
  String content;
  String imgUrl;
  String excerpt;
  int likes;

  CareerAdvice(
    this.id,
    this.title,
    this.content,
    this.imgUrl,
    this.excerpt,
    this.likes
  );

}