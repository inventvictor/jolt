import 'package:flutter/material.dart';
import 'package:jolt/utils/uidata.dart';
import 'package:font_awesome_flutter/font_awesome_flutter.dart';

class HomeMenu {
  String title;
  String image;
  String instruction;
  Color color;
  List<MenuItem> menuItems;

  HomeMenu(
      this.title,
      this.image,
      this.instruction,
      this.color,
      this.menuItems);
}

final List<HomeMenu> menus = [ 
  HomeMenu("Career Planning", UIData.careerPlanningImage, '', UIData.joltBlueAccent, [MenuItem("Daily Todo", Icon(FontAwesomeIcons.solidCalendarCheck, color: UIData.joltBlueAccent)), MenuItem("Career Checklist", Icon(FontAwesomeIcons.solidCheckSquare, color: UIData.joltBlueAccent)), MenuItem("Career Planner", Icon(FontAwesomeIcons.calendarPlus, color: UIData.joltBlueAccent))]),
  HomeMenu("Coaching", UIData.coachingImage, '', UIData.joltBlueAccent, [MenuItem("Career Advice", Icon(FontAwesomeIcons.chessKnight, color: UIData.joltBlueAccent,)), MenuItem("Career Path", Icon(FontAwesomeIcons.objectGroup,color: UIData.joltBlueAccent,)),]),
  HomeMenu("Assessment Test", UIData.assessmentTestImage, 'Check the answer that best reflects your behaviour', UIData.joltBlueAccent, []),
  HomeMenu("View/Edit Profile", UIData.profileImage, '', UIData.joltBlueAccent, [MenuItem("View Profile", Icon(FontAwesomeIcons.eye, color: UIData.joltBlueAccent)), MenuItem("Edit Profile", Icon(FontAwesomeIcons.edit, color: UIData.joltBlueAccent,))])
];

class MenuItem{
  String menuName;
  Icon menuIcon;

  MenuItem(this.menuName, this.menuIcon);

}