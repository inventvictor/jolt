import 'dart:math';
import 'dart:ui';
import 'package:flutter/material.dart';

class UIData {

  static const double _rHeight = 968.0898253993886;
  static const double _rWidth = 485.3932272197492;
  static const Color joltBlue = Color(0xff214764);
  static const Color joltBlueAccent = Color(0xff246f84);

  static double percentHeight(BuildContext context, double height){
    double _deviceHeight = MediaQuery.of(context).size.height.toDouble();
    return (_deviceHeight * height)/_rHeight;
  }
  
  static double percentWidth(BuildContext context, double width){
    double _deviceWidth = MediaQuery.of(context).size.width.toDouble();
    return (_deviceWidth * width)/_rWidth;
  }
  //routes
  // static const String homeRoute = "/home";
  // static const String profileOneRoute = "/View Profile";
  // static const String profileTwoRoute = "/Profile 2";
  // static const String notFoundRoute = "/No Search Result";
  // static const String timelineOneRoute = "/Feed";
  // static const String timelineTwoRoute = "/Tweets";
  // static const String settingsOneRoute = "/Device Settings";
  // static const String shoppingOneRoute = "/Shopping List";
  // static const String shoppingTwoRoute = "/Shopping Details";
  // static const String shoppingThreeRoute = "/Product Details";
  // static const String paymentOneRoute = "/Credit Card";
  // static const String paymentTwoRoute = "/Payment Success";
  // static const String loginOneRoute = "/Login With OTP";
  // static const String loginTwoRoute = "/Login 2";
  // static const String dashboardOneRoute = "/Dashboard 1";
  // static const String dashboardTwoRoute = "/Dashboard 2";

  //strings
  static const String appName = "JOLT";

  //fonts
  // static const String quickFont = "Quicksand";
  // static const String ralewayFont = "Raleway";
  // static const String quickBoldFont = "Quicksand_Bold.otf";
  // static const String quickNormalFont = "Quicksand_Book.otf";
  // static const String quickLightFont = "Quicksand_Light.otf";

  //images
  static const String imageDir = "assets/images";
  static const String careerImage = "$imageDir/career_img.jpg";
  static const String thinkingImage = "$imageDir/thinking.jpg";
  static const String teamImage = "$imageDir/productivity.jpg";
  static const String goalImage = "$imageDir/happy.jpg";
  static const String joltImage = "$imageDir/jolt_img.png";
  static const String basicTestImage = "$imageDir/career_planning.png";
  static const String careerPlanningImage = "$imageDir/development.png";
  static const String coachingImage = "$imageDir/collaboration.png";
  static const String assessmentTestImage = "$imageDir/assessment_test.png";
  static const String profileImage = "$imageDir/profile.png";
  static const String victorImage = "$imageDir/victor_shoaga.jpg";
  static const String pleaseWaitImage = "$imageDir/please_wait.jpg";
  static const String welcomeImage = "$imageDir/welcome.jpg";
  static const String logInImage = "$imageDir/login_logo.png";
  static const String holaImage = "$imageDir/welcome_hola.jpg";
  static const String discImage = "$imageDir/disc.jpeg";
  static const String newEmployee = "$imageDir/new_employee.jpg";
  static const String currentEmployee = "$imageDir/current_employee.jpg";
  static const String leader = "$imageDir/leader.jpg";
  static const String employTransition = "$imageDir/employ_transition.jpg";

  static const String DISC360 = "What if you had an enhanced 360° view of how you come across to others? In other words, imagine having the ability to process the collective perceptions of how others see you. Oftentimes, the way we perceive ourselves and the way others perceive us can be two very different things. Even experienced business leaders will persist in communication and organizational practices with the belief that everyone recognizes their obvious intentions, while overlooking costly misinterpretations that stem from different behavioral styles. These handicaps have always been unfortunate workplace inefficiencies that couldn't be resolved - until now.Our DISC 360º online assessment is an instrument for individuals who want to enhance the traditional DISC self-assessment, by adding the objective insights from dozens (even hundreds) of observer-based DISC assessments.Imagine comparing the rich data of our popular DISC self-assessment, and contrasting it with the observer data collected from colleagues, managers, direct reports… anyone.. Now you can!";
  static const String DISCVALUES = "This is the perfect companion report to pair with the DISC report. The value concept is based on the research into what drives and motivates an individual. The Values assessment measures the seven universal dimensions of motivation that exist within each of us. This report will explain why you are  driven to utilize the communication style and behavioral strengths presented within the DISC reports.The DISC reports predict HOW people will behave. The Values reports reveal WHY they do what they do. Being able to measure and understand HOW and WHY someone will behave is vital to predicting the performance of job applicants, optimizing employee performance, building top performing teams, uncovering the source(s) of communicative dysfunction and developing self-aware leaders.";
  static const String DISCSELF = "This assessment will accurately highlight how your behaviors define your interactions with others, how you approach problems, your preferred activity levels, and the structure of your daily lives. You will receive the added value of a tangible report that indicates the behavioral needs needed for optimum performance.";
  static const String DISCSALESIQ = "The Sales IQ Plus is an objective analysis of an individual's understanding of his/her strategies to sell successfully. It essentially answers the question, Can this person sell? Like any profession, selling has a body of knowledge related to its successful execution. It is this knowledge that the Sales IQ Plus measures.This is one assessment that can be referred to as a test as there are answers which are better than others. It tests sales competencies.This information can be used to compare the sales person with other sales professionals and provide the background for a personalized, high impact approach to increasing sales for coaching and for those scoring well, the report provides excellent content for showcasing in résumés, cover letters, leave-behinds and other marketing materials.";
  static const String DISCSALES = "For those clients in sales positions or having a goal to transition into a sales, this report is the most appropriate for understanding how to effectively present and position your client's personal style so that his or her sales strengths jump off the page of a resume and other marketing materials. Additionally, within the report, your client will receive valuable information on developing, mastering, and applying the steps necessary for success in the selling process. Frequently, clients realize a need for interview coaching so that they can use the report talking points more effectively.";
  static const String DISCCOVERLETTER = "You will gain confidence when you realize how you benefit from insight into communication styles and how critical it is for you, as a ghostwriter, to write resumes and cover letters in your voice. You will be able to confidently use appropriate key words and descriptive phrases directly from this report that are truly the inherent characteristics traits you bring to a job. While reading the report, you will be encouraged to use it  to develop a personal brand, to enhance presentation skills in job interviews, to create an interview portfolio, and to strategize job search strategies.";
  static const String DISCMINI = "This abbreviated report gives you quick insight into the talents and tendencies you bring to the work environment. The report makes all aspects of communicating with and writing for you easier. It will provide appropriate key words and specific interpersonal traits directly in the report. You will marvel at the accuracy of the information revealed in this short report that will enhance your effectiveness in communicating your core strengths and value to the workplace. ";
  static const String DISCLEADERSHIP = " Receives a tangible product providing useful insights into your job performance, team building qualities, and leadership characteristics. You will have reference material for your continued professional development in Social Intelligence, while becoming one of the many people who report that they no longer feel like just a boss. You will be armed with the behavioral and communication insights to help you feel, behave, and be treated like a trusted advisor, with an increased ability to help people find solutions to problems and more adeptness in helping others grow and succeed.";
  static const String DISCCAREERMGMT = "This report identifies job titles that require your greatest strengths with the least need for the your lower-scoring interpersonal strengths. The report also highlights a list of your interpersonal strengths and the value those strengths bring to an organization, for use as talking points in information gathering sessions and as key word choices for showcasing your inherent talents. Research indicates this information increases a person's self-understanding and job satisfaction, while opening his or her mind to appropriate job possibilities.";

  static const String DISC360_NGN = "NGN 30,000";
  static const String DISCVALUES_NGN = "NGN 24,000";
  static const String DISCSELF_NGN = "NGN 24,000";
  static const String DISCSALESIQ_NGN = "NGN 24,000";
  static const String DISCSALES_NGN = "NGN 24,000";
  static const String DISCCOVERLETTER_NGN = "NGN 20,000";
  static const String DISCMINI_NGN = "NGN 18,000";
  static const String DISCLEADERSHIP_NGN = "NGN 24,000";
  static const String DISCCAREERMGMT_NGN = "NGN 30,000";

  static const String DISC360_USD = "USD 150";
  static const String DISCVALUES_USD = "USD 120";
  static const String DISCSELF_USD = "USD 120";
  static const String DISCSALESIQ_USD = "USD 120";
  static const String DISCSALES_USD = "USD 120";
  static const String DISCCOVERLETTER_USD = "USD 100";
  static const String DISCMINI_USD = "USD 90";
  static const String DISCLEADERSHIP_USD = "USD 120";
  static const String DISCCAREERMGMT_USD = "USD 150";
  //login
  // static const String enter_code_label = "Phone Number";
  // static const String enter_code_hint = "10 Digit Phone Number";
  // static const String enter_otp_label = "OTP";
  // static const String enter_otp_hint = "4 Digit OTP";
  // static const String get_otp = "Get OTP";
  // static const String resend_otp = "Resend OTP";
  // static const String login = "Login";
  // static const String enter_valid_number = "Enter 10 digit phone number";
  // static const String enter_valid_otp = "Enter 4 digit otp";

  //gneric
  // static const String error = "Error";
  // static const String success = "Success";
  // static const String ok = "OK";
  // static const String forgot_password = "Forgot Password?";
  // static const String something_went_wrong = "Something went wrong";
  // static const String coming_soon = "Coming Soon";

  static const MaterialColor ui_kit_color = Colors.grey;

//colors
  static List<Color> kitGradients = [
    // new Color.fromRGBO(103, 218, 255, 1.0),
    // new Color.fromRGBO(3, 169, 244, 1.0),
    // new Color.fromRGBO(0, 122, 193, 1.0),
    UIData.joltBlue,
    Colors.black87,
  ];
  static List<Color> kitGradients2 = [
    Colors.cyan.shade600,
    UIData.joltBlue
  ];

  //randomcolor
  static final Random _random = new Random();

  /// Returns a random color.
  static Color next() {
    return new Color(0xFF000000 + _random.nextInt(0x00FFFFFF));
  }

}
