import 'package:shared_preferences/shared_preferences.dart';

class SessionManager {
  ///
  /// Instantiation of the SharedPreferences library
  ///
  final String _kAvatarPrefs = "avatar";
  final String _kFullnamePrefs = "fullname";
  final String _kEmailPrefs = "email";
  final String _kPhonePrefs = "phone";
  final String _kUserIDPrefs = "user_id";
  final String _kDescPrefs = "description";
  final String _kCityPrefs = "city";
  final String _kCountryPrefs = "country";
  final String _kAgePrefs = "age";
  final String _kBusinessConnectsPrefs = "business_connects";
  final String _kSessionPrefs = "session";
  final String _kIsLoggedInPrefs = "is_logged_in";
  final String _kHasUpdatedProfilePrefs = "has_updated_profile";

  SharedPreferences prefs;

  SessionManager(SharedPreferences prefs){
    this.prefs = prefs;
  }

  void createLoginSession(String avatar, String fullname, String email, String userId, 
  String phone, String description, String city, String country, String age, 
  String businessConnects, String session, bool isLoggedIn, bool hasUpdatedProfile){
    setAvatar(prefs, avatar);
    setAge(prefs, age);
    setFullname(prefs, fullname);
    setEmail(prefs, email);
    setUserID(prefs, userId);
    setPhone(prefs, phone);
    setDesc(prefs, description);
    setCity(prefs, city);
    setCountry(prefs, country);
    setAge(prefs, age);
    setBusinessConenects(prefs, businessConnects);
    setSession(prefs, session);
    setIsLoggedIn(prefs, isLoggedIn);
    setUpdatedProfile(prefs, hasUpdatedProfile);
  }

  void updateProfile(bool hasUpdatedProfile){
    setUpdatedProfile(prefs, hasUpdatedProfile);
  }

  void clearLoginSession(){
    setAvatar(prefs, null);
    setAge(prefs, null);
    setFullname(prefs, null);
    setEmail(prefs, null);
    setUserID(prefs, null);
    setPhone(prefs, null);
    setDesc(prefs, null);
    setCity(prefs, null);
    setCountry(prefs, null);
    setAge(prefs, null);
    setBusinessConenects(prefs, null);
    setSession(prefs, null);
    setIsLoggedIn(prefs, false);
    setUpdatedProfile(prefs, false);
  }

  Map getSessionDetails(){
    var map = new Map();
    map[_kAvatarPrefs] = prefs.getString(_kAvatarPrefs) ?? "";
    map[_kFullnamePrefs] = prefs.getString(_kFullnamePrefs) ?? "";
    map[_kEmailPrefs] = prefs.getString(_kEmailPrefs) ?? "";
    map[_kUserIDPrefs] = prefs.getString(_kUserIDPrefs) ?? "";
    map[_kPhonePrefs] = prefs.getString(_kPhonePrefs) ?? "";
    map[_kDescPrefs] = prefs.getString(_kDescPrefs) ?? "";
    map[_kCityPrefs] = prefs.getString(_kCityPrefs) ?? "";
    map[_kCountryPrefs] = prefs.getString(_kCountryPrefs) ?? "";
    map[_kAgePrefs] = prefs.getString(_kAgePrefs) ?? "";
    map[_kBusinessConnectsPrefs] = prefs.getString(_kBusinessConnectsPrefs) ?? "";
    map[_kSessionPrefs] = prefs.getString(_kSessionPrefs) ?? "";
    map[_kIsLoggedInPrefs] = prefs.getBool(_kIsLoggedInPrefs) ?? false;
    map[_kHasUpdatedProfilePrefs] = prefs.getBool(_kHasUpdatedProfilePrefs) ?? false;
    return map;
  }

  /// ----------------------------------------------------------
  /// Method that saves the user avatar
  /// ----------------------------------------------------------
  Future<bool> setAvatar(SharedPreferences prefs, String avatar) async {
    //final SharedPreferences prefs = await SharedPreferences.getInstance();
    return prefs.setString(_kAvatarPrefs, avatar);
  }
  Future<bool> setFullname(SharedPreferences prefs, String fullname) async {
    return prefs.setString(_kFullnamePrefs, fullname);
  }
  Future<bool> setEmail(SharedPreferences prefs, String email) async {
    return prefs.setString(_kEmailPrefs, email);
  }
  Future<bool> setUserID(SharedPreferences prefs, String userID) async {
    return prefs.setString(_kUserIDPrefs, userID);
  }
  Future<bool> setPhone(SharedPreferences prefs, String phone) async {
    return prefs.setString(_kPhonePrefs, phone);
  }
  Future<bool> setDesc(SharedPreferences prefs, String description) async {
    return prefs.setString(_kDescPrefs, description);
  }
  Future<bool> setCity(SharedPreferences prefs, String city) async {
    return prefs.setString(_kCityPrefs, city);
  }
  Future<bool> setCountry(SharedPreferences prefs, String country) async {
    return prefs.setString(_kCountryPrefs, country);
  }
  Future<bool> setAge(SharedPreferences prefs, String age) async {
    return prefs.setString(_kAgePrefs, age);
  }
  Future<bool> setBusinessConenects(SharedPreferences prefs, String bConnects) async {
    return prefs.setString(_kBusinessConnectsPrefs, bConnects);
  }
  Future<bool> setSession(SharedPreferences prefs, String session) async {
    return prefs.setString(_kSessionPrefs, session);
  }
  Future<bool> setIsLoggedIn(SharedPreferences prefs, bool isLoggedIn) async {
    return prefs.setBool(_kIsLoggedInPrefs, isLoggedIn);
  }
  Future<bool> setUpdatedProfile(SharedPreferences prefs, bool hasUpdatedProfile) async {
    return prefs.setBool(_kHasUpdatedProfilePrefs, hasUpdatedProfile);
  }
}