
import 'dart:math';
import 'package:flutter/material.dart';
import 'package:jolt/utils/uidata.dart';

class TabIndicationPainter extends CustomPainter {
  Paint painter;
  final double dxTarget;
  final double dxEntry;
  final double radius;
  final double dy;
  BuildContext context;

  final PageController pageController;

  TabIndicationPainter(
      {this.dxTarget = 125.0,
        this.dxEntry = 25.0,
        this.radius = 21.0,
        this.dy = 25.0, this.pageController, this.context}) : super(repaint: pageController) {
    painter = new Paint()
      ..color = Color(0xFFFFFFFF)
      ..style = PaintingStyle.fill;
  }

  @override
  void paint(Canvas canvas, Size size) {
    double _dxTarget = UIData.percentWidth(context, dxTarget);
    double _dxEntry = UIData.percentWidth(context, dxEntry);
    double _dy = UIData.percentHeight(context, dy);
    double _radius = UIData.percentHeight(context, radius);
    final pos = pageController.position;
    double fullExtent = (pos.maxScrollExtent - pos.minScrollExtent + pos.viewportDimension);

    double pageOffset = pos.extentBefore / fullExtent;

    bool left2right = _dxEntry < _dxTarget;
    Offset entry = new Offset(left2right ? _dxEntry: _dxTarget, _dy);
    Offset target = new Offset(left2right ? _dxTarget : _dxEntry, _dy);

    Path path = new Path();
    path.addArc(
        new Rect.fromCircle(center: entry, radius: _radius), 0.5 * pi, 1 * pi);
    path.addRect(
        new Rect.fromLTRB(entry.dx, _dy - _radius, target.dx, _dy + _radius));
    path.addArc(
        new Rect.fromCircle(center: target, radius: _radius), 1.5 * pi, 1 * pi);

    canvas.translate(size.width * pageOffset, 0.0);
    canvas.drawShadow(path, Color(0xFFfbab66), 3.0, true);
    canvas.drawPath(path, painter);
  }

  @override
  bool shouldRepaint(TabIndicationPainter oldDelegate) => true;
}