import 'dart:async';

//import 'package:flutter/painting.dart';

import 'package:flutter/services.dart' show rootBundle;
import 'package:jolt/utils/uidata.dart';
import 'package:pdf/pdf.dart';
import 'package:flutter/widgets.dart' as fw;
import 'package:pdf/widgets.dart';
import 'dart:io';
import 'package:path_provider/path_provider.dart';
import 'package:path/path.dart' as p;
import 'package:printing/printing.dart' as pri;
import 'package:flutter/material.dart' as im;


// class MyPage extends MultiPage {
//   MyPage(
//       {PdfPageFormat pageFormat = PdfPageFormat.a4,
//       BuildCallback build,
//       EdgeInsets margin})
//       : super(pageFormat: pageFormat, margin: margin, build: build);

//   @override
//   void paint(Widget child, Context context) {
//     context.canvas
//       ..setColor(lightGreen)
//       ..moveTo(0, pageFormat.height)
//       ..lineTo(0, pageFormat.height - 230)
//       ..lineTo(60, pageFormat.height)
//       ..fillPath()
//       ..setColor(green)
//       ..moveTo(0, pageFormat.height)
//       ..lineTo(0, pageFormat.height - 100)
//       ..lineTo(100, pageFormat.height)
//       ..fillPath()
//       ..setColor(lightGreen)
//       ..moveTo(30, pageFormat.height)
//       ..lineTo(110, pageFormat.height - 50)
//       ..lineTo(150, pageFormat.height)
//       ..fillPath()
//       ..moveTo(pageFormat.width, 0)
//       ..lineTo(pageFormat.width, 230)
//       ..lineTo(pageFormat.width - 60, 0)
//       ..fillPath()
//       ..setColor(green)
//       ..moveTo(pageFormat.width, 0)
//       ..lineTo(pageFormat.width, 100)
//       ..lineTo(pageFormat.width - 100, 0)
//       ..fillPath()
//       ..setColor(lightGreen)
//       ..moveTo(pageFormat.width - 30, 0)
//       ..lineTo(pageFormat.width - 110, 50)
//       ..lineTo(pageFormat.width - 150, 0)
//       ..fillPath();

//     super.paint(child, context);
//   }
// }

// class Block extends StatelessWidget {
//   Block({this.question, this.answer});

//   String question;
//   String answer;

//   @override
//   Widget build(Context context) {
//     return Column(
//         crossAxisAlignment: CrossAxisAlignment.start,
//         children: <Widget>[
//           Row(crossAxisAlignment: CrossAxisAlignment.start, children: <Widget>[
//             Container(
//               width: 15,
//               height: 15,
//               margin: const EdgeInsets.only(top: 10.5, left: 10, right: 13),
//               decoration:
//                   const BoxDecoration(color: green, shape: BoxShape.circle),
//             ),
//             Text(question,textScaleFactor: 2.5 ,style: Theme.of(context).defaultTextStyleBold),
//           ]),
//           Padding(padding: const EdgeInsets.only(top: 20)),
//           Container(
//             // decoration: const BoxDecoration(
//             //     border: BoxBorder(left: true, color: green, width: 2)),
//             padding: const EdgeInsets.only(left: 10, top: 5, bottom: 5),
//             margin: const EdgeInsets.only(left: 5),
//             child: Column(
//                 crossAxisAlignment: CrossAxisAlignment.start,
//                 children: <Widget>[
//                   //Lorem(length: 80, textScaleFactor: 2.0),
//                   Text(answer, textScaleFactor: 2.0),
//                 ]),
//           ),
//         ]);
//   }
// }

// class Category extends StatelessWidget {
//   Category({this.title});

//   final String title;

//   @override
//   Widget build(Context context) {
//     return Container(
//         decoration: const BoxDecoration(color: lightGreen, borderRadius: 6),
//         margin: const EdgeInsets.only(bottom: 10, top: 20),
//         padding: const EdgeInsets.fromLTRB(10, 7, 10, 4),
//         child: Text(title, textScaleFactor: 1.5));
//   }
// }

// Future<File> mkfile(String filename) async {
//   Directory dir = await getApplicationDocumentsDirectory();
//   String pathName = p.join(dir.path, filename);
//   return File(pathName);
// }

Future<String> generateDocument(im.BuildContext context, PdfPageFormat pageFormat, String full_name, String abt, String ideal_self, String ideal_attrs, 
String training_needs, String sctwo, String scfive, String scten, String ideal_strengths, String career_path, String dev_opp, String other_dev_needs) async {
  
  final Document pdf = Document(deflate: zlib.encode);
  
  // final img = image_lib.decodeImage(File(await rootBundle.loadString('assets/images/jolt_img.webp')).readAsBytesSync());
  // final image = PdfImage(
  //   pdf.document,
  //   image: img.data.buffer.asUint8List(),
  //   width: img.width,
  //   height: img.height,
  // );

  final PdfImage joltLogo = await pri.pdfImageFromImageProvider(
          pdf: pdf.document,
          image: im.Image.asset("assets/images/jolt_img_pdf.png").image,
          onError: (dynamic exception, StackTrace stackTrace) {
            //print('Unable to download image');
          });
  final fontBold = await rootBundle.load("assets/fonts/WorkSans-Bold.ttf");
  final fontMedium = await rootBundle.load("assets/fonts/WorkSans-Medium.ttf");
  final fontSemiBold = await rootBundle.load("assets/fonts/WorkSans-SemiBold.ttf");
  
  final ttfBold = Font.ttf(fontBold);
  final ttfMedium = Font.ttf(fontMedium);
  final ttfSemiBold = Font.ttf(fontSemiBold);
          
  pdf.addPage(MultiPage(
    pageFormat:
          PdfPageFormat.a4.copyWith(marginBottom: 1.5 * PdfPageFormat.cm),
      crossAxisAlignment: CrossAxisAlignment.start,
      header: (Context context) {
        context.canvas
                ..setColor(PdfColor.fromInt(0xff246f84))
                ..moveTo(0, pageFormat.height)
                ..lineTo(0, pageFormat.height - 200)
                ..lineTo(60, pageFormat.height)
                ..fillPath()
                ..setColor(PdfColor.fromInt(0xff214764))
                ..moveTo(0, pageFormat.height)
                ..lineTo(0, pageFormat.height - 70)
                ..lineTo(100, pageFormat.height)
                ..fillPath()
                ..setColor(PdfColor.fromInt(0xff246f84))
                ..moveTo(30, pageFormat.height)
                ..lineTo(110, pageFormat.height - 20)
                ..lineTo(150, pageFormat.height)
                ..fillPath()
                ..moveTo(pageFormat.width, 0)
                ..lineTo(pageFormat.width, 200)
                ..lineTo(pageFormat.width - 30, 0)
                ..fillPath()
                ..setColor(PdfColor.fromInt(0xff214764))
                ..moveTo(pageFormat.width, 0)
                ..lineTo(pageFormat.width, 70)
                ..lineTo(pageFormat.width - 70, 0)
                ..fillPath()
                ..setColor(PdfColor.fromInt(0xff246f84))
                ..moveTo(pageFormat.width - 30, 0)
                ..lineTo(pageFormat.width - 80, 50)
                ..lineTo(pageFormat.width - 120, 0)
                ..fillPath();
        // if (context.pageNumber == 1) {
        //   return null;
        // }
        // return Container(
        //     alignment: Alignment.centerRight,
        //     margin: const EdgeInsets.only(bottom: 3.0 * PdfPageFormat.mm),
        //     padding: const EdgeInsets.only(bottom: 3.0 * PdfPageFormat.mm),
        //     decoration: const BoxDecoration(
        //         border:
        //             BoxBorder(bottom: true, width: 0.5, color: PdfColor.grey)),
        //     child: Text(full_name,
        //         style: Theme.of(context)
        //             .defaultTextStyle
        //             .copyWith(color: PdfColor.grey)));
       },
      // footer: (Context context) {
      //   return Container(
      //       alignment: Alignment.centerRight,
      //       margin: const EdgeInsets.only(top: 1.0 * PdfPageFormat.cm),
      //       child: Text('Page ${context.pageNumber}',
      //           style: Theme.of(context)
      //               .defaultTextStyle
      //               .copyWith(color: PdfColor.grey)));
      // },
    build: (Context context) => <Widget>[
            Header(
                level: 2,
                child: Row(
                  mainAxisAlignment: MainAxisAlignment.spaceBetween,
                  children: <Widget> [
                    Column(
                      crossAxisAlignment: CrossAxisAlignment.start,
                      children: <Widget>[
                        Text(full_name, textScaleFactor: 3.0, style: TextStyle(font: ttfBold, color: PdfColor.fromInt(0xff214764))),
                        Padding(
                          padding: EdgeInsets.symmetric(vertical: 5, horizontal: 5)
                        ),
                        Text(abt, textScaleFactor: 2.0, style: TextStyle(font: ttfMedium, color: PdfColor.fromInt(0xff246f84))),
                      ]
                    ),
                    Image(joltLogo,)
                ])),
            Header(level: 2, child: Text("IDEAL SELF", textScaleFactor: 2.0, style: TextStyle(font: ttfBold, color: PdfColor.fromInt(0xff246f84)) )),
            Paragraph(
              text: ideal_self,
              style: TextStyle(
                //font: ttfMedium,
                fontSize: 22.0,
                color: PdfColor.fromInt(0xff246f84)
              )
            ),
            Padding(
              padding: EdgeInsets.all(10.0)
            ),
            Header(level: 2, child: Text("IDEAL ATTRIBUTES", textScaleFactor: 2.0, style: TextStyle(font: ttfBold, color: PdfColor.fromInt(0xff246f84))) ),
            Paragraph(
              text: ideal_attrs,
              style: TextStyle(
                //font: ttfMedium,
                fontSize: 22.0,
                color: PdfColor.fromInt(0xff246f84)
              )
            ),
            Padding(
              padding: EdgeInsets.all(10.0)
            ),
            Header(level: 2, child: Text("TRAINING NEEDS", textScaleFactor: 2.0, style: TextStyle(font: ttfBold, color: PdfColor.fromInt(0xff246f84))) ),
            Paragraph(
              text: training_needs,
              style: TextStyle(
                //font: ttfMedium,
                fontSize: 22.0,
                color: PdfColor.fromInt(0xff246f84)
              )
            ),
            Padding(
              padding: EdgeInsets.all(10.0)
            ),
            Header(level: 2, child: Text("SPECIFIC CAREER GOALS", textScaleFactor: 2.0, style: TextStyle(font: ttfBold, color: PdfColor.fromInt(0xff246f84))) ),
            Padding(
              padding: EdgeInsets.all(5.0)
            ),
            Header(level: 1, child: Text("2 years", textScaleFactor: 2.0, style: TextStyle(font: ttfSemiBold, color: PdfColor.fromInt(0xff246f84))) ),
            Padding(
              padding: EdgeInsets.all(5.0)
            ),
            Paragraph(
              text: sctwo,
              style: TextStyle(
                //font: ttfMedium,
                fontSize: 22.0,
                color: PdfColor.fromInt(0xff246f84)
              )
            ),
            Padding(
              padding: EdgeInsets.all(5.0)
            ),
            Header(level: 1, child: Text("5 years", textScaleFactor: 2.0, style: TextStyle(font: ttfSemiBold, color: PdfColor.fromInt(0xff246f84))) ),
            Padding(
              padding: EdgeInsets.all(5.0)
            ),
            Paragraph(
              text: scfive,
              style: TextStyle(
                //font: ttfMedium,
                fontSize: 22.0,
                color: PdfColor.fromInt(0xff246f84)
              )
            ),
            Padding(
              padding: EdgeInsets.all(5.0)
            ),
            Header(level: 1, child: Text("10 years", textScaleFactor: 2.0, style: TextStyle(font: ttfSemiBold, color: PdfColor.fromInt(0xff246f84))) ),
            Padding(
              padding: EdgeInsets.all(5.0)
            ),
            Paragraph(
              text: scten,
              style: TextStyle(
                //font: ttfMedium,
                fontSize: 22.0,
                color: PdfColor.fromInt(0xff246f84)
              )
            ),
            Padding(
              padding: EdgeInsets.all(10.0)
            ),
            Header(level: 2, child: Text("IDEAL STRENGTHS", textScaleFactor: 2.0, style: TextStyle(font: ttfBold, color: PdfColor.fromInt(0xff246f84))) ),
            Paragraph(
              text: ideal_strengths,
              style: TextStyle(
                //font: ttfMedium,
                fontSize: 22.0,
                color: PdfColor.fromInt(0xff246f84)
              )
            ),
            Padding(
              padding: EdgeInsets.all(10.0)
            ),
            Header(level: 2, child: Text("CAREER PATH/TRACK", textScaleFactor: 2.0, style: TextStyle(font: ttfBold, color: PdfColor.fromInt(0xff246f84))) ),
            Paragraph(
              text: career_path,
              style: TextStyle(
                //font: ttfMedium,
                fontSize: 22.0,
                color: PdfColor.fromInt(0xff246f84)
              )
            ),
            Padding(
              padding: EdgeInsets.all(10.0)
            ),
            Header(level: 2, child: Text("DEVELOPMENT OPPORTUNITIES", textScaleFactor: 2.0, style: TextStyle(font: ttfBold, color: PdfColor.fromInt(0xff246f84))) ),
            Paragraph(
              text: dev_opp,
              style: TextStyle(
                //font: ttfMedium,
                fontSize: 22.0,
                color: PdfColor.fromInt(0xff246f84)
              )
            ),
            Padding(
              padding: EdgeInsets.all(10.0)
            ),
            Header(level: 2, child: Text("OTHER DEVELOPMENT NEEDS", textScaleFactor: 2.0, style: TextStyle(font: ttfBold, color: PdfColor.fromInt(0xff246f84))) ),
            Paragraph(
              text: other_dev_needs,
              style: TextStyle(
                //font: ttfMedium,
                fontSize: 22.0,
                color: PdfColor.fromInt(0xff246f84)
              )
            ),
         ]
      )
  );

  Directory tempDir = await getTemporaryDirectory();
  String tempPath = tempDir.path;
  var genfile = File("$tempPath" + "/careerplanner_" + new DateTime.now().millisecondsSinceEpoch.toString() + ".pdf");
  await genfile.writeAsBytes(pdf.save());
  return genfile.path;
}