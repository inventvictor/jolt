import 'package:flutter/material.dart';
import 'package:jolt/utils/uidata.dart';

class ActivityTile extends StatelessWidget {
  final activityTitle;
  final activitySubtitle;
  final timestamp;
  final activityIcon;
  final activityTitleColor;
  final activitySubtitleColor;
  ActivityTile({this.activityTitle, this.activitySubtitle, this.timestamp, this.activityIcon, this.activityTitleColor = Colors.black, this.activitySubtitleColor = Colors.black});
  @override
  Widget build(BuildContext context) {
    return Material(
      elevation: UIData.percentHeight(context, 5),
      borderRadius: BorderRadius.circular(5.0),
      shadowColor: UIData.joltBlueAccent,
      type: MaterialType.card,
      color: Colors.white,
      child: Padding(
        padding: EdgeInsets.symmetric(vertical: UIData.percentHeight(context, 10), horizontal: UIData.percentWidth(context, 10)),
        child: ListTile(
          leading: //Icon(
            activityIcon,
            //size: UIData.percentHeight(context, 60),
          //),
          title: Text(activityTitle,
            style: TextStyle(
              color: activityTitleColor,
              fontSize: UIData.percentHeight(context, 22),
              fontFamily: "WorkSansMedium"
            ),
          ),
          isThreeLine: true,
          
          subtitle: Text(activitySubtitle + "\n\n" + "[$timestamp]",
            style: TextStyle(
              fontSize: UIData.percentHeight(context, 20),
              fontFamily: "WorkSansMedium",
              color: activitySubtitleColor
            ),
          ),
        ),
      )
    );
  }
}