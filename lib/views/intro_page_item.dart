import 'package:flutter/material.dart';
import 'package:jolt/ui/current_employee_page.dart';
import 'package:jolt/ui/emp_employee_page.dart';
import 'package:jolt/ui/leader_employee_page.dart';
import 'package:jolt/utils/uidata.dart';
import 'package:meta/meta.dart';
import 'package:jolt/models/data.dart';
import 'page_transformer.dart';
import 'package:jolt/ui/new_employee_page.dart';

class IntroPageItem extends StatelessWidget {
  IntroPageItem({
    @required this.item,
    @required this.pageVisibility,
  });

  final IntroItem item;
  final PageVisibility pageVisibility;

  Widget _applyTextEffects({
    @required double translationFactor,
    @required Widget child,
  }) {
    final double xTranslation = pageVisibility.pagePosition * translationFactor;

    return Opacity(
      opacity: pageVisibility.visibleFraction,
      child: Transform(
        alignment: FractionalOffset.topLeft,
        transform: Matrix4.translationValues(
          xTranslation,
          0.0,
          0.0,
        ),
        child: child,
      ),
    );
  }

  _buildTextContainer(BuildContext context, Image image) {
    var textTheme = Theme.of(context).textTheme;
    var categoryText = _applyTextEffects(
      translationFactor: 300.0,
      child: Text(
        item.category,
        style: textTheme.caption.copyWith(
          color: Colors.white70,
          fontWeight: FontWeight.bold,
          letterSpacing: 2.0,
          fontSize: 14.0,
        ),
        textAlign: TextAlign.center,
      ),
    );

    var titleText = _applyTextEffects(
      translationFactor: 200.0,
      child: Padding(
        padding: const EdgeInsets.only(top: 16.0),
        child: Text(
          item.title,
          style: textTheme.title
              .copyWith(color: Colors.white, fontWeight: FontWeight.bold),
          textAlign: TextAlign.center,
        ),
      ),
    );

    return Positioned(
      bottom: UIData.percentHeight(context, 56),
      left: UIData.percentWidth(context, 32),
      right: UIData.percentWidth(context, 32),
      child: Column(
        mainAxisSize: MainAxisSize.min,
        children: [
          categoryText,
          titleText,
          new SizedBox(
            height: UIData.percentHeight(context, 15),
          ),
          new RaisedButton(
            elevation: UIData.percentHeight(context, 10),
            child: Text('Continue',
            style: TextStyle(
              color: Colors.black,
              fontFamily: "WorkSansMedium",
              fontSize: UIData.percentHeight(context, 18)
            ),),
            color: Colors.white,
            onPressed: (){
              if(image.image.toString().contains("new_employee.jpg")){
                //print('New Employee');
                Future(() {
                  Navigator.push(context, MaterialPageRoute(builder: (context)=> NewEmployeePage(begin: true,)));
                });
              }
              if(image.image.toString().contains("current_employee.jpg")){
                //print('Current Employee');
                Future(() {
                  Navigator.push(context, MaterialPageRoute(builder: (context)=> CurrentEmployeePage(begin: true,)));
                });
              }
              if(image.image.toString().contains("leader.jpg")){
                //print('Leader');
                Future(() {
                  Navigator.push(context, MaterialPageRoute(builder: (context)=> LeaderEmployeePage(begin: true,)));
                });
              }
              if(image.image.toString().contains("employ_transition.jpg")){
                //print('Employment Transition');
                Future(() {
                  Navigator.push(context, MaterialPageRoute(builder: (context)=> EmpEmployeePage(begin: true,)));
                });
              }
            },
          )
        ],
      ),
    );
  }

  @override
  Widget build(BuildContext context) {
    
    var image = Image.asset(
      item.imageUrl,
      fit: BoxFit.cover,
      alignment: FractionalOffset(
        0.5 + (pageVisibility.pagePosition / 3),
        0.5,
      ),
    );
    

    var imageOverlayGradient = DecoratedBox(
      decoration: BoxDecoration(
        gradient: LinearGradient(
          begin: FractionalOffset.bottomCenter,
          end: FractionalOffset.topCenter,
          colors: [
            const Color(0xFF000000),
            const Color(0x00000000),
          ],
        ),
      ),
    );

    return Padding(
      padding: EdgeInsets.symmetric(
        vertical: UIData.percentHeight(context, 16),
        horizontal: UIData.percentWidth(context, 8),
      ),
      child: Material(
        elevation: UIData.percentHeight(context, 8),
        borderRadius: BorderRadius.circular(8.0),
        child: Stack(
          fit: StackFit.expand,
          children: [
            image,
            imageOverlayGradient,
            _buildTextContainer(context, image),

          ],
        ),
      ),
    );
  }
}
