import 'package:flutter/material.dart';
import 'package:intro_views_flutter/Models/page_view_model.dart';
import 'package:intro_views_flutter/intro_views_flutter.dart';
import 'package:jolt/utils/uidata.dart';
import 'package:jolt/ui/second_root_page.dart';

class JolTCareerIntros extends StatelessWidget{
  
  final pages = [
    new PageViewModel(
        pageColor: Color(0xFF533742),
        iconImageAssetPath: null,
        iconColor: UIData.joltBlueAccent,
        bubbleBackgroundColor: UIData.joltBlueAccent,
        body: Text(
          'Thinking of how to take your career to the next level or a career transition?',
          style: TextStyle(color: Colors.white),
        ),
        title: Text(
          '',
        ),
        textStyle: TextStyle(color: Colors.black, fontFamily: "WorkSansSemiBold", fontSize: 20),
        mainImage: Image.asset(
          UIData.thinkingImage,
          fit: BoxFit.scaleDown,
        )),
    new PageViewModel(
      pageColor: Color(0xFF6e7c8a),
      iconImageAssetPath: null,
      iconColor: Colors.cyanAccent,
      bubbleBackgroundColor: Colors.cyanAccent,
      body: Text(
        'Not to worry, we can help you get started well on that journey.',
        style: TextStyle(
          color: Colors.white
        ),
      ),
      title: Text(''),
      mainImage: Image.asset(
        UIData.teamImage,
        fit:BoxFit.scaleDown,
      ),
      textStyle: TextStyle(color: Colors.black, fontFamily: "WorkSansSemiBold", fontSize: 20.0),
    ),
    new PageViewModel(
      pageColor: Color(0xFF778ca3),
      iconImageAssetPath: null,
      iconColor: Colors.deepOrangeAccent,
      bubbleBackgroundColor: Colors.deepOrangeAccent,
      body: Text(
        'Our goal is to make your career a beautiful one',
        style: TextStyle(
          color: Colors.white
        ),
      ),
      title: Text(''),
      mainImage: Image.asset(
        UIData.goalImage,
        fit: BoxFit.scaleDown,
      ),
      textStyle: TextStyle(color: Colors.black, fontFamily: "WorkSansSemiBold", fontSize: 20.0),
    ),
  ];
  
  @override
  Widget build(BuildContext context){
    return new Builder(
        builder: (context) => new IntroViewsFlutter(
              pages,
              onTapDoneButton: () {
                Navigator.pop(context);
                Navigator.pushReplacement(
                  context,
                  new MaterialPageRoute(
                    builder: (context) => new SecondRootPage(),
                  ), //MaterialPageRoute
                );
              },
              showSkipButton:
                  true, //Whether you want to show the skip button or not.
              pageButtonTextStyles: TextStyle(
                color: Colors.white,
                fontSize: UIData.percentHeight(context, 25),
              ),
            ), //IntroViewsFlutter
      ); //Builder
  }
}
