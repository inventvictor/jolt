import 'package:flutter/material.dart';
import 'package:jolt/utils/uidata.dart';

class ProfileTile extends StatelessWidget {
  final title;
  final titleColor;
  final subtitle;
  final subtitleColor;
  ProfileTile({this.title, this.titleColor = UIData.joltBlueAccent, this.subtitle, this.subtitleColor = Colors.black});
  @override
  Widget build(BuildContext context) {
    return Column(
      // crossAxisAlignment: CrossAxisAlignment.start,
      mainAxisAlignment: MainAxisAlignment.center,
      children: <Widget>[
        Text(
          title,
          style: TextStyle(
            fontFamily: "WorkSansSemiBold",
              fontSize: UIData.percentHeight(context, 25), fontWeight: FontWeight.w700, color: titleColor),
        ),
        SizedBox(
          height: 5.0,
        ),
        Text(
          subtitle,
          style: TextStyle(
            fontFamily: "WorkSansSemiBold",
              fontSize: UIData.percentHeight(context, 20), fontWeight: FontWeight.bold, color: subtitleColor),
        ),
      ],
    );
  }
}
