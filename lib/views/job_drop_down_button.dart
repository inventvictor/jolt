import 'package:flutter/material.dart';
import 'package:jolt/utils/uidata.dart';
class JobDropDownButton extends StatefulWidget {
  JobDropDownButton({
    Key key,
    this.items
  }) : super(key: key);
  final List<DropdownMenuItem<int>> items;
  @override
  _JobDropDownButtonState createState() => _JobDropDownButtonState();
}

class _JobDropDownButtonState extends State<JobDropDownButton> {
  int _active = 0;
  @override
  Widget build(BuildContext context) {
    return DropdownButton(
      iconEnabledColor: UIData.joltBlueAccent,
      underline: Container(),
      onChanged: (f){
         setState(() {
           _active = f;
         });
      },
      value: _active,
      style: Theme.of(context).textTheme.headline,
      items: widget.items
      // [
      //   DropdownMenuItem(child: Text("Engineering", style: TextStyle(color: UIData.joltBlueAccent, fontSize: UIData.percentHeight(context, 25), fontFamily: "WorkSansSemiBold"),), value: 0),
      //   DropdownMenuItem(child: Text("Business", style: TextStyle(color: UIData.joltBlueAccent, fontSize: UIData.percentHeight(context, 25), fontFamily: "WorkSansSemiBold")), value: 1),
      //   DropdownMenuItem(child: Text("Fashion", style: TextStyle(color: UIData.joltBlueAccent, fontSize: UIData.percentHeight(context, 25), fontFamily: "WorkSansSemiBold")), value: 2),
      //   DropdownMenuItem(child: Text("Banking", style: TextStyle(color: UIData.joltBlueAccent, fontSize: UIData.percentHeight(context, 25), fontFamily: "WorkSansSemiBold")), value: 3),
      //   DropdownMenuItem(child: Text("Fintech", style: TextStyle(color: UIData.joltBlueAccent, fontSize: UIData.percentHeight(context, 25), fontFamily: "WorkSansSemiBold")), value: 4),
      // ],
    );
  }
}