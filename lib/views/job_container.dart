import 'package:flutter/material.dart';
import 'package:jolt/utils/uidata.dart';
class JobContainer extends StatelessWidget {
  const JobContainer({
    Key key,
    @required this.iconUrl,
    @required this.title,
    @required this.location,
    @required this.description,
    @required this.salary,
    @required this.onTap,
  }) : super(key: key);
  final String iconUrl, title, location, description, salary;
  final Function onTap;
  @override
  Widget build(BuildContext context) {
    return GestureDetector(
      onTap: onTap,
      child: Container(
        margin: EdgeInsets.symmetric(vertical: UIData.percentHeight(context, 15), horizontal: UIData.percentWidth(context, 15)),
        padding: EdgeInsets.symmetric(vertical: UIData.percentHeight(context, 15), horizontal: UIData.percentWidth(context, 15)),
        decoration: BoxDecoration(
          color: Colors.white,
          borderRadius: BorderRadius.circular(9.0),
          boxShadow: [
            BoxShadow(
                color: Colors.grey[350], blurRadius: 5.0, offset: Offset(0, 0))
          ],
        ),
        child: Column(
          crossAxisAlignment: CrossAxisAlignment.start,
          children: <Widget>[
            Row(
              children: <Widget>[
                ClipRRect(
                  borderRadius: BorderRadius.circular(30.0),
                  child: Image.network(
                    "$iconUrl",
                    height: UIData.percentHeight(context, 100),
                    width: UIData.percentWidth(context, 100),
                  ),
                ),
                SizedBox(width: UIData.percentWidth(context, 25)),
                Expanded(
                  child: Column(
                    crossAxisAlignment: CrossAxisAlignment.start,
                    children: <Widget>[
                      Text(
                        "$title",
                        style: Theme.of(context).textTheme.title,
                      ),
                      Text(
                        "$location",
                        style: Theme.of(context).textTheme.subtitle.apply(
                              color: Colors.grey,
                            ),
                      )
                    ],
                  ),
                )
              ],
            ),
            SizedBox(height: UIData.percentHeight(context, 10)),
            Text(
              "$description",
              style:
                  Theme.of(context).textTheme.body1.apply(color: Colors.grey),
              maxLines: 3,
              overflow: TextOverflow.ellipsis,
            ),
            SizedBox(height: UIData.percentHeight(context, 10)),
            Text(
              "$salary",
              style:
                  Theme.of(context).textTheme.subhead.apply(fontWeightDelta: 2),
            )
          ],
        ),
      ),
    );
  }
}