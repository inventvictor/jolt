import 'package:flutter/material.dart';
import 'package:jolt/views/scale_transition.dart';
import 'package:jolt/ui/sign_up.dart';

class Splash extends StatelessWidget {

  static final String _iconTitle = "res/j.png";

  @override
  Widget build(BuildContext context) {


    ///create book tile hero
    createSplashIcon() => Hero(
      tag: _iconTitle,
      child: Material(
        child: Image(
          image: AssetImage(_iconTitle),
          width: 100.00,
          height: 100.00,
          color: null,
          fit: BoxFit.scaleDown,
          alignment: Alignment.center,
        ),
      ),
    );

    final splashIcon = new GestureDetector(
      onTap: () {
        //do something here
        //print("onTap called.");
        //Navigator.of(context).pushNamed('/sign_up');
        Navigator.push(
          context,
          ScaleRoute(widget: SignUp()),
        );
      },
      child: createSplashIcon(),

    );

    return Scaffold(
      backgroundColor: Colors.white,
      body: new Center(
        child: splashIcon,
      ),
    );



  }
}