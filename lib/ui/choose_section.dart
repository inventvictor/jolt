import 'package:flutter/material.dart';
import 'package:jolt/buttons/simple_round_career_icon_button.dart';
import 'package:jolt/buttons/simple_round_business_icon_button.dart';
import 'package:jolt/utils/uidata.dart';

class ChooseSection extends StatelessWidget {
  
  @override
  Widget build(BuildContext context) {
    return new Container(
      padding: EdgeInsets.only(top: UIData.percentHeight(context, 30)),
      color: Colors.white,
      height: MediaQuery.of(context).size.height,
      child: new Column(
        children: <Widget>[
          Image.asset(
            UIData.careerImage,
            fit: BoxFit.cover,
          ),
          
          // new Container(
          //   child: new GestureDetector(
          //     onTap: () {
          //       //print("onTap called.");
          //     },
          //   ),
          // ),
          SimpleRoundCareerIconButton(
                backgroundColor: UIData.joltBlueAccent,
                buttonText: Text("CAREER", style: TextStyle(
                  color: Colors.white,
                  fontFamily: "WorkSansSemiBold"
                ),),
                textColor: Colors.white,
                icon: Icon(Icons.email),
              ),

          SimpleRoundBusinessIconButton(
            backgroundColor: Colors.deepPurpleAccent,
            buttonText: Text("BUSINESS", style: TextStyle(
                color: Colors.white,
                fontFamily: "WorkSansSemiBold"
            ),),
            textColor: Colors.white,
            icon: Icon(Icons.email),
            iconAlignment: Alignment.centerRight,
          ),

        ],
      )
    );
  }
}