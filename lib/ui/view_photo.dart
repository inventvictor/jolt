import 'package:crop/crop.dart';
import 'package:flutter/material.dart';
import 'package:jolt/ui/view_photo_crop.dart';
import 'package:jolt/utils/uidata.dart';
import 'package:jolt/views/common_scaffold.dart';
import 'dart:io';
import 'package:image_picker/image_picker.dart';
import 'package:font_awesome_flutter/font_awesome_flutter.dart';
import 'package:permission_handler/permission_handler.dart';
import 'package:shared_preferences/shared_preferences.dart';
import 'package:jolt/utils/career_session_manager.dart';
import 'package:firebase_storage/firebase_storage.dart';
import 'package:intl/intl.dart';
import 'package:firebase_database/firebase_database.dart';


class ViewPhotoPage extends StatefulWidget {
  String imageFilePath;
  bool fromProfileView;
  File imageFile;
  ViewPhotoPage({Key key, @required this.imageFilePath, this.fromProfileView = false}) : super(key: key);

  @override
  State<StatefulWidget> createState() => new _ViewPhotoPageState();
  
}

class _ViewPhotoPageState extends State<ViewPhotoPage> {
  Size deviceSize;
  

  Future<Null> _pickImage() async {
      widget.imageFile = await ImagePicker.pickImage(source: ImageSource.gallery);
      if (widget.imageFile != null) {
        // _cropImage();
        //send widget.imageFile to Cropping Page

        Navigator.push(
            context,
            new MaterialPageRoute(
              builder: (context) => new ViewPhotoCropPage(imageFile: widget.imageFile,),
            ), //MaterialPageRoute
          );


      }
  }

  // Future<Null> _cropImage() async {
  //     print('mercy');
  //     print(widget.imageFile.path);
  //     File croppedFile = await ImageCropper.cropImage(
  //       sourcePath: widget.imageFile.path,
  //       aspectRatioPresets: Platform.isAndroid
  //           ? [
  //               CropAspectRatioPreset.square,
  //               CropAspectRatioPreset.ratio3x2,
  //               CropAspectRatioPreset.original,
  //               CropAspectRatioPreset.ratio4x3,
  //               CropAspectRatioPreset.ratio16x9
  //             ]
  //           : [
  //               CropAspectRatioPreset.original,
  //               CropAspectRatioPreset.square,
  //               CropAspectRatioPreset.ratio3x2,
  //               CropAspectRatioPreset.ratio4x3,
  //               CropAspectRatioPreset.ratio5x3,
  //               CropAspectRatioPreset.ratio5x4,
  //               CropAspectRatioPreset.ratio7x5,
  //               CropAspectRatioPreset.ratio16x9
  //             ],
  //       androidUiSettings: AndroidUiSettings(
  //           toolbarTitle: 'Cropper',
  //           toolbarColor: UIData.joltBlueAccent,
  //           toolbarWidgetColor: Colors.white,
  //           initAspectRatio: CropAspectRatioPreset.original,
  //           lockAspectRatio: false
  //         ),
  //         // iosUiSettings: IOSUiSettings(
  //         //   title: 'Cropper'
  //         // )
  //     );
  //     print(croppedFile.uri);
  //     if (croppedFile != null) {
  //       print('victor');
  //       widget.imageFile = croppedFile;
  //       try {
  //         final result = await InternetAddress.lookup('google.com');
  //         if (result.isNotEmpty && result[0].rawAddress.isNotEmpty) {
  //             //upload to firebase storage here
  //             DateTime now = new DateTime.now();
  //             var datestamp = new DateFormat("yyyyMMdd'T'HHmmss");
  //             String formatted = datestamp.format(now);
  //             final StorageReference firebaseStorageRef = FirebaseStorage.instance.ref().child(_sessionManager.getSessionDetails()['user_id']+"_"+formatted);
  //             final StorageUploadTask task = firebaseStorageRef.putFile(widget.imageFile);
  //             //progress dialog should sit here
  //             if(task.isInProgress){
  //               showProgress(context);
  //             }
  //             var dowurl = await (await task.onComplete).ref.getDownloadURL();
  //             Navigator.pop(context);
  //             setState(() {
  //                     widget.imageFilePath = dowurl.toString();
  //               });
  //             await _sessionManager.setAvatar(_prefs, widget.imageFilePath);
  //             await FirebaseDatabase.instance.reference().child("users").child(_sessionManager.getSessionDetails()['user_id']).set({
  //               'name': _sessionManager.getSessionDetails()['fullname'],
  //               'email': _sessionManager.getSessionDetails()['email'],
  //               'avatar': widget.imageFilePath,
  //               'phone': _sessionManager.getSessionDetails()['phone'],
  //               'user_id': _sessionManager.getSessionDetails()['user_id'],
  //               'desc': _sessionManager.getSessionDetails()['description'],
  //               'city': _sessionManager.getSessionDetails()['city'],
  //               'country': _sessionManager.getSessionDetails()['country'],
  //               'age': _sessionManager.getSessionDetails()['age'],
  //               'business_connects': _sessionManager.getSessionDetails()['business_connects'],
  //               'session': _sessionManager.getSessionDetails()['session'],
  //               'has_updated_profile':_sessionManager.getSessionDetails()['has_updated_profile']
  //             });
  //             await FirebaseDatabase.instance.reference().child("activities").child(_sessionManager.getSessionDetails()['user_id']).push().child(
  //                     new DateTime.now().millisecondsSinceEpoch.toString()).set("profilephoto_update");
  //         }
  //       } on SocketException catch (_) {
  //           //print('not connected');
  //           showError(context, 'Something went wrong. Check network!');
  //       }
        
  //     }
  // }

  // Future<Null> showError(BuildContext context, String msg){
  //   return showDialog(
  //     context: context,
  //     barrierDismissible: true,
  //     builder: (BuildContext context){
  //       return SimpleDialog(
  //           contentPadding: EdgeInsets.all(0.0),
  //           children: <Widget>[
  //             new Container(
  //               height: UIData.percentHeight(context, 75),
  //               decoration: new BoxDecoration(
  //                 color: Colors.redAccent,
  //               ),
  //               child: new Center(
  //                 child: Icon(
  //                   FontAwesomeIcons.exclamationCircle,
  //                   color: Colors.white,
  //                 ),
  //               )),
  //             new SizedBox(
  //               height: UIData.percentHeight(context, 20),
  //             ),
  //             new Container(
  //               padding: EdgeInsets.symmetric(vertical: UIData.percentHeight(context, 12), horizontal: UIData.percentWidth(context, 12)),
  //               child: Column(
  //                 children: <Widget>[
  //                   new Center(
  //                       child: new Text(msg,
  //                         style: TextStyle(
  //                           fontSize: UIData.percentHeight(context, 20),
  //                           fontFamily: "WorkSansSemiBold"
  //                         ),
  //                         textAlign: TextAlign.center,
  //                       ),
  //                     ),
  //                   new SizedBox(
  //                     height: UIData.percentHeight(context, 20),
  //                   ),
  //                   new Padding(
  //                     padding: EdgeInsets.symmetric(vertical: UIData.percentHeight(context, 10), horizontal: UIData.percentWidth(context, 10)),
  //                   )
  //                     ],
  //                   ),
  //             ),
  //           ],
  //       ); 
  //     }
  //   ); 
  // }

  // Future<Null> showProgress(BuildContext context){
  //     return showDialog(
  //               context: context,
  //               barrierDismissible: false,
  //               builder: (BuildContext context){
  //                 return new WillPopScope(
  //                   onWillPop: (){
  //                     Future.value(false);
  //                   },
  //                   child: SimpleDialog(
  //                     contentPadding: EdgeInsets.all(0.0),
  //                     children: <Widget>[
  //                       new Container(
  //                         height: UIData.percentHeight(context, 75),
  //                         decoration: new BoxDecoration(
  //                           color: UIData.joltBlueAccent,
  //                         ),
  //                         child: new Center(
  //                           child: Icon(
  //                             FontAwesomeIcons.solidFileImage,
  //                             color: Colors.white,
  //                           ),
  //                         )),
  //                       new SizedBox(
  //                         height: UIData.percentHeight(context, 20),
  //                       ),
  //                       new Center(
  //                         child: CircularProgressIndicator(),
  //                       ),
  //                       new SizedBox(
  //                         height: UIData.percentHeight(context, 20),
  //                       ),
  //                       new Center(
  //                         child: new Text("Uploading, please wait...",
  //                           style: TextStyle(
  //                             fontSize: UIData.percentHeight(context, 20),
  //                             fontFamily: "WorkSansSemiBold"
  //                           ),
  //                         ),
  //                       ),
  //                       new SizedBox(
  //                         height: UIData.percentHeight(context, 20),
  //                       ),
  //                     ],
  //                   ),
  //                 ); 
  //               }
  //             );
  //     }

  SessionManager _sessionManager;
  SharedPreferences _prefs;

  Future<Object> initialisePrefs() async {
    _prefs = await SharedPreferences.getInstance();
    SessionManager sessionManager = new SessionManager(_prefs);
    return sessionManager;
  }

  @override
  void initState() {
    super.initState();
    initialisePrefs().then((sessionManager){
      setState(() {
            _sessionManager = sessionManager;
        });
    });
  }

  @override
  Widget build(BuildContext context) {
    deviceSize = MediaQuery.of(context).size;
    return CommonScaffold(
      appTitle: "My Photo",
      actionFirstIcon: widget.fromProfileView ? null:FontAwesomeIcons.upload,
      actionFirstIconFunc: (){
        _pickImage();
      },
      bColor: Colors.black,
      bodyData: new Center(
        child: Image.network(
          widget.imageFilePath,
          fit: BoxFit.scaleDown,
          alignment: Alignment.center,
        )
      ),
      backGroundColor: Colors.black,
      elevation: 0.0,
    );
  }
}
