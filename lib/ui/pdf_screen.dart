import 'package:flutter/material.dart';
import 'package:flutter_full_pdf_viewer/full_pdf_viewer_scaffold.dart';
import 'package:share/share.dart';
import 'package:intl/intl.dart';
import 'package:firebase_storage/firebase_storage.dart';
import 'package:fluttertoast/fluttertoast.dart';
import 'package:shared_preferences/shared_preferences.dart';
import 'package:jolt/utils/career_session_manager.dart';
import 'dart:io';
//import 'package:simple_share/simple_share.dart';

class PDFScreen extends StatefulWidget{

  String pathPDF = "";
  bool showShare = true;
  PDFScreen({this.pathPDF, this.showShare=true});

  State<StatefulWidget> createState(){
    return _PDFScreenState();
  }
}

class _PDFScreenState extends State<PDFScreen>{

  final GlobalKey<ScaffoldState> _scaffoldKey = new GlobalKey<ScaffoldState>();


  SessionManager _sessionManager;
  SharedPreferences _prefs;

  Future<Object> initialisePrefs() async {
    _prefs = await SharedPreferences.getInstance();
    SessionManager sessionManager = new SessionManager(_prefs);
    return sessionManager;
  }
  @override
  void initState() {
    super.initState();
    initialisePrefs().then((sessionManager){
      setState(() {
            _sessionManager = sessionManager;
        });
    });
  }
  
  
  @override
  Widget build(BuildContext context) {
    return PDFViewerScaffold(
        key: _scaffoldKey,
        appBar: AppBar(
          title: Text("Document"),
          actions: widget.showShare ? <Widget>[
            IconButton(
              icon: Icon(Icons.share),
              onPressed: () async{
                if (widget.pathPDF != null && widget.pathPDF.isNotEmpty) {
                  final uri = Uri.file(widget.pathPDF);
                  //print(uri);
                  // SimpleShare.share(
                  //     uri: uri.toString(),
                  //     title: "Share my file",
                  //     msg: "My message");
                  //Share.share("victor");
                  try {
                    final result = await InternetAddress.lookup('google.com');
                    if (result.isNotEmpty && result[0].rawAddress.isNotEmpty) {
                        DateTime now = new DateTime.now();
                        var datestamp = new DateFormat("yyyyMMdd'T'HHmmss");
                        String formatted = datestamp.format(now);
                        final StorageReference firebaseStorageRef = FirebaseStorage.instance.ref().child(_sessionManager.getSessionDetails()['user_id']+"_"+formatted);
                        final StorageUploadTask task = firebaseStorageRef.putFile(File(uri.toFilePath()));
                        //progress dialog should sit here
                        if(task.isInProgress){
                          Fluttertoast.showToast(
                                msg: "Please wait, some background tasks are running...",
                                toastLength: Toast.LENGTH_LONG,
                                gravity: ToastGravity.CENTER,
                                timeInSecForIos: 3,
                                backgroundColor: Colors.green,
                                textColor: Colors.white,
                              );
                        }
                        var dowurl = await (await task.onComplete).ref.getDownloadURL();
                        print ("Downloadable URL is " + dowurl.toString());
                        Share.share(dowurl);
                    }
                  } on SocketException catch (_) {
                      //print('not connected');
                      Fluttertoast.showToast(
                                msg: "Please check your internet connection",
                                toastLength: Toast.LENGTH_LONG,
                                gravity: ToastGravity.CENTER,
                                timeInSecForIos: 3,
                                backgroundColor: Colors.red,
                                textColor: Colors.white,
                              );
                  }
                  
                }
              },
            ),
          ] : null,
        ),
        path: widget.pathPDF);
  }
}