import 'package:flutter/material.dart';
import 'package:jolt/utils/uidata.dart';
import 'package:jolt/utils/career_session_manager.dart';
import 'package:shared_preferences/shared_preferences.dart';
import 'package:jolt/views/common_scaffold.dart';
import 'package:fluttertoast/fluttertoast.dart';
import 'package:firebase_database/firebase_database.dart';
import 'package:font_awesome_flutter/font_awesome_flutter.dart';

class WriteMore extends StatefulWidget {
  @override
  State<StatefulWidget> createState() {
    return _WriteMoreState();
  }
}

class _WriteMoreState extends State<WriteMore> {
  SessionManager _sessionManager;
  Size deviceSize;
  var writeMoreField = new TextEditingController();

  Future<Object> initialisePrefs() async {
    SharedPreferences prefs = await SharedPreferences.getInstance();
    SessionManager sessionManager = new SessionManager(prefs);
    return sessionManager;
  }

  @override
  void initState() {
    super.initState();
    initialisePrefs().then((sessionManager){
      setState(() {
            _sessionManager = sessionManager;
        });
    });
  }

  Widget bodyData(BuildContext context) => new Container(
    //height: 
    color: Colors.white,
    padding: EdgeInsets.symmetric(vertical: UIData.percentHeight(context, 20), horizontal: UIData.percentWidth(context, 20)),
    child: SingleChildScrollView(
      child: Column(
        children: <Widget>[
          new Container(
            child: new TextFormField(
              style: TextStyle(
                fontSize: UIData.percentHeight(context, 20),
                fontFamily: "WorkSansSemiBold"
              ),
              controller: writeMoreField,
              maxLines: null,
              keyboardType: TextInputType.multiline,
              decoration: new InputDecoration(
                hintText: 'Write More Here ....',
                border: const OutlineInputBorder(
                  borderRadius: const BorderRadius.all(Radius.circular(15.0)),
                  
                ),
              ),
            ),
          ),
          
          SizedBox(
                  height: UIData.percentHeight(context, 10),
                ),
          new Container(
            width: deviceSize.width,
            child: new RaisedButton(
              child: new Text(
                'Send'.toUpperCase(),
                style: new TextStyle(
                  fontSize: UIData.percentHeight(context, 20),
                  color: Colors.white,
                  fontWeight: FontWeight.w700,
                  letterSpacing: 2.0
                ),
              ),
              onPressed: (){
                try{
                    String t = new DateTime.now().millisecondsSinceEpoch.toString();
                    String s = writeMoreField.text.toString();
                    Future.delayed(new Duration(seconds: 1), (){
                      FirebaseDatabase.instance.reference().child("feedbacks").child(_sessionManager.getSessionDetails()['user_id']).push().child(
                        '$t').set('$s');
                      writeMoreField.clear();
                      showSuccess(context); //may revisit later on!
                    });
                    
                  }
                  catch(e){
                    Fluttertoast.showToast(
                                  msg: "Something went wrong!",
                                  toastLength: Toast.LENGTH_SHORT,
                                  gravity: ToastGravity.CENTER,
                                  timeInSecForIos: 1,
                                  backgroundColor: Colors.red,
                                  textColor: Colors.white,
                              );
                  }
              },
              color: UIData.joltBlue,
            ),
            margin: new EdgeInsets.only(
              top: UIData.percentHeight(context, 20)
            ),
          )
        ],
      ),
    ),
  );

  Future<Null> showSuccess(BuildContext context){
      return showDialog(
                context: context,
                barrierDismissible: true,
                builder: (BuildContext context){
                  return SimpleDialog(
                      contentPadding: EdgeInsets.all(0.0),
                      children: <Widget>[
                        new Container(
                          height: UIData.percentHeight(context, 75),
                          decoration: new BoxDecoration(
                            color: Colors.greenAccent,
                          ),
                          child: new Center(
                            child: Icon(
                              FontAwesomeIcons.check,
                              color: Colors.white,
                            ),
                          )),
                        new SizedBox(
                          height: UIData.percentHeight(context, 20),
                        ),
                        new Container(
                          padding: EdgeInsets.symmetric(vertical: UIData.percentHeight(context, 12), horizontal: UIData.percentWidth(context, 12)),
                          child: Column(
                            children: <Widget>[
                              new Center(
                                  child: new Text("Feedback submitted. Thank you.",
                                    style: TextStyle(
                                      fontSize: UIData.percentHeight(context, 20),
                                      fontFamily: "WorkSansSemiBold"
                                    ),
                                    textAlign: TextAlign.center,
                                  ),
                                ),
                              new SizedBox(
                                height: UIData.percentHeight(context, 20),
                              ),
                              new Padding(
                                padding: EdgeInsets.symmetric(vertical: UIData.percentHeight(context, 10), horizontal: UIData.percentWidth(context, 10)),
                              )
                                ],
                              ),
                        ),
                      ],
                  ); 
                }
              ); 
    }

  @override
  Widget build(BuildContext context) {
    deviceSize = MediaQuery.of(context).size;
    return CommonScaffold(
      appTitle: "Compose Feedback",
      backGroundColor: Colors.white,
      actionFirstIcon: null,
      bColor: UIData.joltBlueAccent,
      bodyData: bodyData(context),
    );
  }

}