import 'package:flutter/material.dart';
import 'package:font_awesome_flutter/font_awesome_flutter.dart';
import 'package:jolt/ui/assessment_page.dart';
import 'package:jolt/ui/career_advice_listview.dart';
import 'package:jolt/ui/career_path_listview.dart';
import 'package:jolt/ui/review_pay.dart';
import 'package:jolt/utils/uidata.dart';
import 'package:jolt/models/home_menu.dart';
import 'package:jolt/views/profile_tile.dart';
import 'edit_profile_page.dart';
import 'package:shared_preferences/shared_preferences.dart';
import 'package:jolt/utils/career_session_manager.dart';
import 'daily_todo.dart';
import 'show_intro_page_view.dart';
import 'my_profile_page.dart';
import 'package:jolt/auth/jolt_auth.dart';
import 'package:jolt/ui/career_planner.dart';

class HomePage extends StatelessWidget {

  var qtyField = new TextEditingController();

  Widget menuSetBackground(HomeMenu menu, BuildContext context) => Container(
    child: Card(
      color: Colors.white,
      elevation: UIData.percentHeight(context, 5),
      margin: EdgeInsets.fromLTRB(0.0, 0.0, 0.0, 10.0),
      shape: RoundedRectangleBorder(
        borderRadius: BorderRadius.circular(20.0)
      ),
    ),
    
  );

  Widget menuData(HomeMenu menu, BuildContext context) => Column(
    mainAxisAlignment: MainAxisAlignment.center,
    children: <Widget>[
      Image.asset(
        menu.image,
        width: UIData.percentWidth(context, 100),
        height: UIData.percentHeight(context, 100),
      ),
      SizedBox(
        height: UIData.percentHeight(context, 25),
      ),
      Text(
        menu.title,
        style: TextStyle(color: Colors.black, fontFamily: "WorkSansSemiBold", fontSize: UIData.percentHeight(context, 20)),
      )
    ],
  );

Widget header(HomeMenu menu, BuildContext context) => Ink(
        decoration: BoxDecoration(
            //gradient: LinearGradient(colors: UIData.kitGradients2),
            color: menu.color),
        child: Padding(
          padding: EdgeInsets.symmetric(vertical: UIData.percentHeight(context, 16), horizontal: UIData.percentWidth(context, 16)),
          child: Center(
            child: Row(
              mainAxisSize: MainAxisSize.min,
              children: <Widget>[
                Container(
                  decoration: BoxDecoration(
                        borderRadius: BorderRadius.circular(50.0),
                        border: Border.all(width: 2.0, color: Colors.white)
                      ),
                  child: CircleAvatar(
                        radius: 25.0,
                        backgroundImage: NetworkImage(new SessionManager(prefs).getSessionDetails()['avatar']),
                      ),
                ),
                SizedBox(
                  width: UIData.percentWidth(context, 20),
                ),
                Padding(
                  padding: EdgeInsets.symmetric(vertical: UIData.percentHeight(context, 8), horizontal: UIData.percentWidth(context, 8)),
                  child: ProfileTile(
                    title: new SessionManager(prefs).getSessionDetails()['fullname'],
                    subtitle: new SessionManager(prefs).getSessionDetails()['email'],
                    titleColor: Colors.white,
                    subtitleColor: Colors.white,
                  ),
                )
              ],
            ),
          )
        ),
      );

void _showListModalBottomSheet(BuildContext context, HomeMenu menu) {
    showModalBottomSheet(
        context: context,
        builder: (context) => Material(
            clipBehavior: Clip.antiAlias,
            color: Colors.white,
            shape: RoundedRectangleBorder(
                borderRadius: new BorderRadius.only(
                    topLeft: new Radius.circular(0.0),
                    topRight: new Radius.circular(0.0))),
            child: Column(
              crossAxisAlignment: CrossAxisAlignment.start,
              mainAxisSize: MainAxisSize.min,
              children: <Widget>[
                header(menu, context),
                Container(
                  height: UIData.percentHeight(context, 300),
                  child: ListView.builder(
                    itemCount: menu.menuItems.length,
                    itemBuilder: (context, i) => Padding(
                          padding: EdgeInsets.symmetric(horizontal: UIData.percentWidth(context, 10)),
                          child: ListTile(
                              title: Text(
                                menu.menuItems[i].menuName,
                                style: TextStyle(fontSize: UIData.percentHeight(context, 20), fontFamily: "WorkSansSemiBold"),
                              ),
                              leading: menu.menuItems[i].menuIcon,
                              onTap: () async {
                                //Navigator.pop(context);
                                //Navigator.pushNamed(context, "/${menu.items[i]}");
                                print(menu.menuItems[i]);
                                if(menu.menuItems[i].menuName.toLowerCase() == 'view profile'){
                                  Navigator.push(
                                    context,
                                    new MaterialPageRoute(
                                      builder: (context) => new MyProfilePage(auth: new Auth()),
                                    ), //MaterialPageRoute
                                  );
                                }
                                else if(menu.menuItems[i].menuName.toLowerCase() == 'edit profile'){
                                  Navigator.push(
                                    context,
                                    new MaterialPageRoute(
                                      builder: (context) => new EditProfilePage(),
                                    ), //MaterialPageRoute
                                  );
                                }
                                else if(menu.menuItems[i].menuName.toLowerCase() == 'career checklist'){
                                  Navigator.push(
                                    context,
                                    new MaterialPageRoute(
                                      builder: (context) => new ShowIntroPageView(),
                                    )
                                  );
                                }
                                else if(menu.menuItems[i].menuName.toLowerCase() == 'career planner'){
                                  Navigator.push(
                                    context, 
                                    new MaterialPageRoute(
                                      builder: (context) => new CareerPlannerPage(showDialog_: true,)
                                    )
                                  );
                                }
                                else if(menu.menuItems[i].menuName.toLowerCase() == 'daily todo'){
                                   Navigator.push(
                                     context,
                                     new MaterialPageRoute(
                                       builder: (context) => new TaskistApp(),
                                     )
                                   );
                                }
                                else if(menu.menuItems[i].menuName.toLowerCase() == 'career path') {
                                  // http.Response response = await http.get('https://www.themuse.com/advice/meeting-follow-up-email-template-example');
                                  // print(response.statusCode);
                                  // dom.Document document = parser.parse(response.body);
                                  // print (document.getElementsByTagName('article-row').length);
                                  Navigator.push(
                                    context,
                                    new MaterialPageRoute(
                                      builder: (context) => new CareerPathList(auth: new Auth()),
                                    ), //MaterialPageRoute
                                  );
                                }
                                else if(menu.menuItems[i].menuName.toLowerCase() == 'career advice') {
                                  // http.Response response = await http.get('https://www.themuse.com/advice/meeting-follow-up-email-template-example');
                                  // print(response.statusCode);
                                  // dom.Document document = parser.parse(response.body);
                                  // print (document.getElementsByTagName('article-row').length);
                                  Navigator.push(
                                    context,
                                    new MaterialPageRoute(
                                      builder: (context) => new CareerAdviceList(auth: new Auth()),
                                    ), //MaterialPageRoute
                                  );
                                }
                              }),
                        ),
                  ),
                ),
                //MyAboutTile()
              ],
            ))
    );
  }

  void _showBasicTestModalSheet(BuildContext context, HomeMenu menu) {
    showDialog(
      context: context,
      builder: (BuildContext context){
        return AlertDialog(
          title: Text(menu.title, style: TextStyle(fontSize: UIData.percentHeight(context, 25), fontFamily: "WorkSansBold"), ),
          content: Text(menu.instruction, style: TextStyle(fontSize: UIData.percentHeight(context, 20), fontFamily: "WorkSansMedium")),
          actions: <Widget>[
            new FlatButton(
              child: new Text('I UNDERSTAND', style: TextStyle(fontSize: UIData.percentHeight(context, 20), fontFamily: "WorkSansMedium"),),
              onPressed: (){
                Navigator.of(context).pop();
                Navigator.push(
                  context,
                  new MaterialPageRoute(
                    builder: (context) => new AssessmentPage(auth: new Auth()),
                  ), //MaterialPageRoute
                );
              },
            ),
          ],
        );
      }
    );
  }

  void _showQuantityModalSheet(BuildContext context, String amt, String title, String details) {
    print("Go to Review and Pay");
    Navigator.of(context).pop();
    Navigator.push(
      context,
      new MaterialPageRoute(
        builder: (context) => new ReviewPage(amt: amt, discTitle: title, discDetails: details,)
      ), //MaterialPageRoute
    );
    // showDialog(
    //   context: context,
    //   builder: (BuildContext context){
    //     return SimpleDialog(
    //       contentPadding: EdgeInsets.symmetric(vertical: UIData.percentHeight(context, 10), horizontal: UIData.percentWidth(context, 30)),
    //       title: Text("Please input how many?", style: TextStyle(fontSize: UIData.percentHeight(context, 25), fontFamily: "WorkSansBold"), ),
    //       children: <Widget>[
    //         SizedBox(
    //             height: UIData.percentHeight(context, 10),
    //           ),
    //         new TextFormField(
    //           controller: qtyField,
    //           keyboardType: TextInputType.numberWithOptions(decimal: true,signed: true), 
    //           decoration: new InputDecoration(
    //             hintText: 'e.g 1',
    //             labelText: 'Quantity',
    //           )
    //         ),
    //         SizedBox(
    //             height: UIData.percentHeight(context, 5),
    //           ),
    //         new FlatButton(
    //           child: new Text('DONE', style: TextStyle(fontSize: UIData.percentHeight(context, 20), fontFamily: "WorkSansMedium"),),
    //           onPressed: (){
    //             print("Go to Review and Pay");
    //             print(amt*int.parse(qtyField.text));
    //             Navigator.of(context).pop();
    //             Navigator.push(
    //               context,
    //               new MaterialPageRoute(
    //                 builder: (context) => new ReviewPage(amt: r"$100")
    //               ), //MaterialPageRoute
    //             );
    //           },
    //         ),
    //       ],
    //     );
    //   }
    // );
  }

  void _showDISCTestModalSheet(BuildContext context, HomeMenu menu) {
    showDialog(
      context: context,
      builder: (BuildContext context){
        return Center(
          child: ConstrainedBox(
          constraints: BoxConstraints(maxHeight: UIData.percentHeight(context, 700)),
          child: SimpleDialog(
              titlePadding: EdgeInsets.symmetric(vertical: UIData.percentHeight(context, 10), horizontal: UIData.percentWidth(context, 20)),
                semanticLabel: 'DISC Tests',
                title: Text(
                        'Buy DISC Tests',
                        style: TextStyle(
                          color: Colors.black,
                          fontFamily: "WorkSansBold",
                          fontSize: UIData.percentHeight(context, 22)
                        ),
                      ),
                  contentPadding: EdgeInsets.symmetric(vertical: UIData.percentHeight(context, 10), horizontal: UIData.percentWidth(context, 20)),
                  children: <Widget>[
                      GestureDetector(
                        child: Card(
                          elevation: UIData.percentHeight(context, 10),
                          child: Padding(
                            padding: EdgeInsets.symmetric(horizontal: UIData.percentWidth(context, 0), vertical: UIData.percentHeight(context, 0)),
                            child: ListTile(
                              leading: Icon(
                                FontAwesomeIcons.compactDisc,
                                color: UIData.joltBlueAccent,
                                size: UIData.percentHeight(context, 75),
                              ),
                              title: Row(
                                mainAxisAlignment: MainAxisAlignment.spaceBetween,
                                mainAxisSize: MainAxisSize.min,
                                children: <Widget>[
                                  Text(
                                    "DISC- 360°",
                                    style: TextStyle(
                                      color: Colors.black,
                                      fontSize: UIData.percentHeight(context, 18),
                                      fontFamily: "WorkSansBold"
                                    ),
                                  ),
                                ],
                              ),
                              subtitle: Container(
                                    child: Text(
                                      new SessionManager(prefs).getSessionDetails()['country'].toString().toLowerCase() == "nigeria" ? UIData.DISC360_NGN: UIData.DISC360_USD,
                                      style: TextStyle(
                                        fontSize: UIData.percentHeight(context, 18),
                                        fontFamily: "WorkSansMedium",
                                        color: UIData.joltBlueAccent
                                      ),
                                    ),
                                  ),
                            )
                          )
                        ),
                        onTap: (){
                          _showQuantityModalSheet(context, new SessionManager(prefs).getSessionDetails()['country'].toString().toLowerCase() == "nigeria" ? UIData.DISC360_NGN: UIData.DISC360_USD, "DISC- 360°", UIData.DISC360);
                        },
                      ),
                      GestureDetector(
                        child: Card(
                          elevation: UIData.percentHeight(context, 10),
                          child: Padding(
                            padding: EdgeInsets.symmetric(horizontal: UIData.percentWidth(context, 0), vertical: UIData.percentHeight(context, 0)),
                            child: ListTile(
                              leading: Icon(
                                FontAwesomeIcons.compactDisc,
                                color: UIData.joltBlueAccent,
                                size: UIData.percentHeight(context, 75),
                              ),
                              title: Row(
                                mainAxisAlignment: MainAxisAlignment.spaceBetween,
                                children: <Widget>[
                                  Text(
                                    "Leadership",
                                    style: TextStyle(
                                      color: Colors.black,
                                      fontSize: UIData.percentHeight(context, 18),
                                      fontFamily: "WorkSansBold"
                                    ),
                                  ),
                                ],
                              ),
                              subtitle: Container(
                                    child: Text(
                                      new SessionManager(prefs).getSessionDetails()['country'].toString().toLowerCase() == "nigeria" ? UIData.DISCLEADERSHIP_NGN: UIData.DISCLEADERSHIP_USD,
                                      style: TextStyle(
                                        fontSize: UIData.percentHeight(context, 18),
                                        fontFamily: "WorkSansMedium",
                                        color: UIData.joltBlueAccent
                                      ),
                                    ),
                                  ),
                            )
                          )
                        ),
                        onTap: (){
                          _showQuantityModalSheet(context, new SessionManager(prefs).getSessionDetails()['country'].toString().toLowerCase() == "nigeria" ? UIData.DISCLEADERSHIP_NGN: UIData.DISCLEADERSHIP_USD, "DISC- LEADERSHIP", UIData.DISCLEADERSHIP);
                        },
                      ),
                      GestureDetector(
                        child: Card(
                          elevation: UIData.percentHeight(context, 10),
                          child: Padding(
                            padding: EdgeInsets.symmetric(horizontal: UIData.percentWidth(context, 0), vertical: UIData.percentHeight(context, 0)),
                            child: ListTile(
                              leading: Icon(
                                FontAwesomeIcons.compactDisc,
                                color: UIData.joltBlueAccent,
                                size: UIData.percentHeight(context, 75),
                              ),
                              title: Row(
                                mainAxisAlignment: MainAxisAlignment.spaceBetween,
                                children: <Widget>[
                                  Text(
                                    "DISC- Values",
                                    style: TextStyle(
                                      color: Colors.black,
                                      fontSize: UIData.percentHeight(context, 18),
                                      fontFamily: "WorkSansBold"
                                    ),
                                  ),
                                ],
                              ),
                              subtitle: Container(
                                    child: Text(
                                      new SessionManager(prefs).getSessionDetails()['country'].toString().toLowerCase() == "nigeria" ? UIData.DISCVALUES_NGN: UIData.DISCVALUES_USD,
                                      style: TextStyle(
                                        fontSize: UIData.percentHeight(context, 18),
                                        fontFamily: "WorkSansMedium",
                                        color: UIData.joltBlueAccent
                                      ),
                                    ),
                                  ),
                            )
                          )
                        ),
                        onTap: (){
                          _showQuantityModalSheet(context, new SessionManager(prefs).getSessionDetails()['country'].toString().toLowerCase() == "nigeria" ? UIData.DISCVALUES_NGN: UIData.DISCVALUES_USD, "DISC- VALUES", UIData.DISCVALUES);
                        },
                      ),
                      GestureDetector(
                        child: Card(
                          elevation: UIData.percentHeight(context, 10),
                          child: Padding(
                            padding: EdgeInsets.symmetric(horizontal: UIData.percentWidth(context, 0), vertical: UIData.percentHeight(context, 0)),
                            child: ListTile(
                              leading: Icon(
                                FontAwesomeIcons.compactDisc,
                                color: UIData.joltBlueAccent,
                                size: UIData.percentHeight(context, 75),
                              ),
                              title: Row(
                                mainAxisAlignment: MainAxisAlignment.spaceBetween,
                                children: <Widget>[
                                  Text(
                                    "DISC- Self",
                                    style: TextStyle(
                                      color: Colors.black,
                                      fontSize: UIData.percentHeight(context, 18),
                                      fontFamily: "WorkSansBold"
                                    ),
                                  ),
                                ],
                              ),
                              subtitle: Container(
                                    child: Text(
                                      new SessionManager(prefs).getSessionDetails()['country'].toString().toLowerCase() == "nigeria" ? UIData.DISCSELF_NGN: UIData.DISCSELF_USD,
                                      style: TextStyle(
                                        fontSize: UIData.percentHeight(context, 18),
                                        fontFamily: "WorkSansMedium",
                                        color: UIData.joltBlueAccent
                                      ),
                                    ),
                                  ),
                            )
                          )
                        ),
                        onTap: (){
                          _showQuantityModalSheet(context, new SessionManager(prefs).getSessionDetails()['country'].toString().toLowerCase() == "nigeria" ? UIData.DISCSELF_NGN: UIData.DISCSELF_USD, "DISC- SELF", UIData.DISCSELF);
                        },
                      ),
                      GestureDetector(
                        child: Card(
                          elevation: UIData.percentHeight(context, 10),
                          child: Padding(
                            padding: EdgeInsets.symmetric(horizontal: UIData.percentWidth(context, 0), vertical: UIData.percentHeight(context, 0)),
                            child: ListTile(
                              leading: Icon(
                                FontAwesomeIcons.compactDisc,
                                color: UIData.joltBlueAccent,
                                size: UIData.percentHeight(context, 75),
                              ),
                              title: Row(
                                mainAxisAlignment: MainAxisAlignment.spaceBetween,
                                children: <Widget>[
                                  Text(
                                    "DISC- Sales IQ",
                                    style: TextStyle(
                                      color: Colors.black,
                                      fontSize: UIData.percentHeight(context, 18),
                                      fontFamily: "WorkSansBold"
                                    ),
                                  ),
                                ],
                              ),
                              subtitle: Container(
                                    child: Text(
                                      new SessionManager(prefs).getSessionDetails()['country'].toString().toLowerCase() == "nigeria" ? UIData.DISCSALESIQ_NGN: UIData.DISCSALESIQ_USD,
                                      style: TextStyle(
                                        fontSize: UIData.percentHeight(context, 18),
                                        fontFamily: "WorkSansMedium",
                                        color: UIData.joltBlueAccent
                                      ),
                                    ),
                                  ),
                            )
                          )
                        ),
                        onTap: (){
                          _showQuantityModalSheet(context, new SessionManager(prefs).getSessionDetails()['country'].toString().toLowerCase() == "nigeria" ? UIData.DISCSALESIQ_NGN: UIData.DISCSALESIQ_USD, "DISC- SALES IQ", UIData.DISCSALESIQ);
                        },
                      ),
                      GestureDetector(
                        child: Card(
                          elevation: UIData.percentHeight(context, 10),
                          child: Padding(
                            padding: EdgeInsets.symmetric(horizontal: UIData.percentWidth(context, 0), vertical: UIData.percentHeight(context, 0)),
                            child: ListTile(
                              leading: Icon(
                                FontAwesomeIcons.compactDisc,
                                color: UIData.joltBlueAccent,
                                size: UIData.percentHeight(context, 75),
                              ),
                              title: Row(
                                mainAxisAlignment: MainAxisAlignment.spaceBetween,
                                children: <Widget>[
                                  Text(
                                    "DISC- Sales",
                                    style: TextStyle(
                                      color: Colors.black,
                                      fontSize: UIData.percentHeight(context, 18),
                                      fontFamily: "WorkSansBold"
                                    ),
                                  ),
                                ],
                              ),
                              subtitle: Container(
                                    child: Text(
                                      new SessionManager(prefs).getSessionDetails()['country'].toString().toLowerCase() == "nigeria" ? UIData.DISCSALES_NGN: UIData.DISCSALES_USD,
                                      style: TextStyle(
                                        fontSize: UIData.percentHeight(context, 18),
                                        fontFamily: "WorkSansMedium",
                                        color: UIData.joltBlueAccent
                                      ),
                                    ),
                                  ),
                            )
                          )
                        ),
                        onTap: (){
                          _showQuantityModalSheet(context, new SessionManager(prefs).getSessionDetails()['country'].toString().toLowerCase() == "nigeria" ? UIData.DISCSALES_NGN: UIData.DISCSALES_USD, "DISC- SALES", UIData.DISCSALES);
                        },
                      ),
                      GestureDetector(
                        child: Card(
                          elevation: UIData.percentHeight(context, 10),
                          child: Padding(
                            padding: EdgeInsets.symmetric(horizontal: UIData.percentWidth(context, 0), vertical: UIData.percentHeight(context, 0)),
                            child: ListTile(
                              leading: Icon(
                                FontAwesomeIcons.compactDisc,
                                color: UIData.joltBlueAccent,
                                size: UIData.percentHeight(context, 75),
                              ),
                              title: Row(
                                mainAxisAlignment: MainAxisAlignment.spaceBetween,
                                children: <Widget>[
                                  Text(
                                    "DISC- Cover Letter",
                                    style: TextStyle(
                                      color: Colors.black,
                                      fontSize: UIData.percentHeight(context, 18),
                                      fontFamily: "WorkSansBold"
                                    ),
                                  ),
                                ],
                              ),
                              subtitle: Container(
                                    child: Text(
                                      new SessionManager(prefs).getSessionDetails()['country'].toString().toLowerCase() == "nigeria" ? UIData.DISCCOVERLETTER_NGN: UIData.DISCCOVERLETTER_USD,
                                      style: TextStyle(
                                        fontSize: UIData.percentHeight(context, 18),
                                        fontFamily: "WorkSansMedium",
                                        color: UIData.joltBlueAccent
                                      ),
                                    ),
                                  ),
                            )
                          )
                        ),
                        onTap: (){
                          _showQuantityModalSheet(context, new SessionManager(prefs).getSessionDetails()['country'].toString().toLowerCase() == "nigeria" ? UIData.DISCCOVERLETTER_NGN: UIData.DISCCOVERLETTER_USD, "DISC- COVER LETTER", UIData.DISCCOVERLETTER);
                        },
                      ),
                      GestureDetector(
                        child: Card(
                          elevation: UIData.percentHeight(context, 10),
                          child: Padding(
                            padding: EdgeInsets.symmetric(horizontal: UIData.percentWidth(context, 0), vertical: UIData.percentHeight(context, 0)),
                            child: ListTile(
                              leading: Icon(
                                FontAwesomeIcons.compactDisc,
                                color: UIData.joltBlueAccent,
                                size: UIData.percentHeight(context, 75),
                              ),
                              title: Row(
                                mainAxisAlignment: MainAxisAlignment.spaceBetween,
                                children: <Widget>[
                                  Text(
                                    "DISC- Mini",
                                    style: TextStyle(
                                      color: Colors.black,
                                      fontSize: UIData.percentHeight(context, 18),
                                      fontFamily: "WorkSansBold"
                                    ),
                                  ),
                                ],
                              ),
                              subtitle: Container(
                                    child: Text(
                                      new SessionManager(prefs).getSessionDetails()['country'].toString().toLowerCase() == "nigeria" ? UIData.DISCMINI_NGN: UIData.DISCMINI_USD,
                                      style: TextStyle(
                                        fontSize: UIData.percentHeight(context, 18),
                                        fontFamily: "WorkSansMedium",
                                        color: UIData.joltBlueAccent
                                      ),
                                    ),
                                  ),
                            )
                          )
                        ),
                        onTap: (){
                          _showQuantityModalSheet(context, new SessionManager(prefs).getSessionDetails()['country'].toString().toLowerCase() == "nigeria" ? UIData.DISCMINI_NGN: UIData.DISCMINI_USD, "DISC- MINI", UIData.DISCMINI);
                        },
                      ),
                      GestureDetector(
                        child: Card(
                          elevation: UIData.percentHeight(context, 10),
                          child: Padding(
                            padding: EdgeInsets.symmetric(horizontal: UIData.percentWidth(context, 0), vertical: UIData.percentHeight(context, 0)),
                            child: ListTile(
                              leading: Icon(
                                FontAwesomeIcons.compactDisc,
                                color: UIData.joltBlueAccent,
                                size: UIData.percentHeight(context, 75),
                              ),
                              title: Row(
                                mainAxisAlignment: MainAxisAlignment.spaceBetween,
                                children: <Widget>[
                                  Text(
                                    "DISC- Career Mgmt",
                                    style: TextStyle(
                                      color: Colors.black,
                                      fontSize: UIData.percentHeight(context, 18),
                                      fontFamily: "WorkSansBold"
                                    ),
                                  ),
                                ],
                              ),
                              subtitle: Container(
                                    child: Text(
                                      new SessionManager(prefs).getSessionDetails()['country'].toString().toLowerCase() == "nigeria" ? UIData.DISCCAREERMGMT_NGN: UIData.DISCCAREERMGMT_USD,
                                      style: TextStyle(
                                        fontSize: UIData.percentHeight(context, 18),
                                        fontFamily: "WorkSansMedium",
                                        color: UIData.joltBlueAccent
                                      ),
                                    ),
                                  ),
                            )
                          )
                        ),
                        onTap: (){
                          _showQuantityModalSheet(context, new SessionManager(prefs).getSessionDetails()['country'].toString().toLowerCase() == "nigeria" ? UIData.DISCCAREERMGMT_NGN: UIData.DISCCAREERMGMT_USD, "DISC- CAREER MANAGEMENT", UIData.DISCCAREERMGMT);
                        },
                      ),
                    ],
                  
            ),
        ),
        );
      }
    );
  }

  void _showInstructionModalSheet(BuildContext context, HomeMenu menu) {
    showDialog(
      context: context,
      builder: (BuildContext context){
        // return AlertDialog(
        //   title: Text(menu.title, style: TextStyle(fontSize: UIData.percentHeight(context, 25), fontFamily: "WorkSansBold"), ),
        //   content: Text(menu.instruction, style: TextStyle(fontSize: UIData.percentHeight(context, 20), fontFamily: "WorkSansMedium")),
        //   actions: <Widget>[
        //     new FlatButton(
        //       child: new Text('I UNDERSTAND', style: TextStyle(fontSize: UIData.percentHeight(context, 20), fontFamily: "WorkSansMedium"),),
        //       onPressed: (){
        //         Navigator.of(context).pop();
        //         Navigator.push(
        //           context,
        //           new MaterialPageRoute(
        //             builder: (context) => new AssessmentPage(auth: new Auth()),
        //           ), //MaterialPageRoute
        //         );
        //       },
        //     ),
        //   ],
        // );
        return SimpleDialog(
            semanticLabel: 'Assessment Tests',
              contentPadding: EdgeInsets.symmetric(vertical: UIData.percentHeight(context, 20), horizontal: UIData.percentWidth(context, 20)),
              children: <Widget>[
                  new Text(
                    'Take Assessment Tests',
                    style: TextStyle(
                      color: Colors.black,
                      fontFamily: "WorkSansBold",
                      fontSize: UIData.percentHeight(context, 22)
                    ),
                  ),
                  SizedBox(
                    height: UIData.percentHeight(context, 10),
                  ),
                  GestureDetector(
                    child: Card(
                      elevation: UIData.percentHeight(context, 10),
                      child: Padding(
                        padding: EdgeInsets.symmetric(horizontal: UIData.percentWidth(context, 15), vertical: UIData.percentHeight(context, 15)),
                        child: ListTile(
                          leading: new Image(
                                      image: AssetImage(UIData.basicTestImage),
                                    ),
                          title: Text("Basic Test",
                            style: TextStyle(
                              color: Colors.blue,
                              fontSize: UIData.percentHeight(context, 20),
                              fontFamily: "WorkSansMedium"
                            ),
                          ),
                          subtitle: Column(
                            crossAxisAlignment: CrossAxisAlignment.start,
                            children: <Widget>[
                              Container(
                                padding: EdgeInsets.all(UIData.percentHeight(context, 7)),
                                decoration: BoxDecoration(
                                  color: Colors.green,
                                  borderRadius: BorderRadius.all(Radius.circular(5.0)),
                                ),
                                child: Text(
                                  "Free",
                                  style: TextStyle(
                                    fontSize: UIData.percentHeight(context, 15),
                                    fontFamily: "WorkSansMedium",
                                    color: Colors.white
                                  ),
                                ),
                              )
                            ],
                          )
                        )
                      )
                    ),
                    onTap: (){
                       _showBasicTestModalSheet(context, menu);
                    },
                  ),
                  SizedBox(
                    height: UIData.percentHeight(context, 5),
                  ),
                  GestureDetector(
                    child: Card(
                      elevation: UIData.percentHeight(context, 10),
                      child: Padding(
                        padding: EdgeInsets.symmetric(horizontal: UIData.percentWidth(context, 15), vertical: UIData.percentHeight(context, 15)),
                        child: ListTile(
                          leading: new Image(
                                      image: AssetImage(UIData.discImage),
                                    ),
                          title: Text("DISC Test",
                            style: TextStyle(
                              color: Colors.blue,
                              fontSize: UIData.percentHeight(context, 20),
                              fontFamily: "WorkSansMedium"
                            ),
                          ),
                          subtitle: Column(
                            crossAxisAlignment: CrossAxisAlignment.start,
                            children: <Widget>[
                              Container(
                                padding: EdgeInsets.all(UIData.percentHeight(context, 7)),
                                decoration: BoxDecoration(
                                  color: Colors.blue,
                                  borderRadius: BorderRadius.all(Radius.circular(5.0)),
                                ),
                                child: Text(
                                  "In-App purchase",
                                  style: TextStyle(
                                    fontSize: UIData.percentHeight(context, 15),
                                    fontFamily: "WorkSansMedium",
                                    color: Colors.white
                                  ),
                                ),
                              )
                            ],
                          )
                        )
                      )
                    ),
                    onTap: (){
                      Navigator.pop(context);
                      _showDISCTestModalSheet(context, menu);
                    },
                  ),
                  //new Divider(
                    //color: Colors.black45,
                    //height: 30.0,
                  //),
                ],
              
        );
      }
    );
  }

  SharedPreferences prefs;
  Future<void> initialisePrefs() async {
    prefs = await SharedPreferences.getInstance();
    print(new SessionManager(prefs).getSessionDetails());
  }

  @override
  Widget build(BuildContext context){
    initialisePrefs();
    createTile(HomeMenu homeMenu) => new Container(
      child: new GestureDetector(
        onTap: (){
          print(homeMenu.title);
          if(homeMenu.title == 'Assessment Test'){
            _showInstructionModalSheet(context, homeMenu);
          }
          else{
            _showListModalBottomSheet(context, homeMenu);
          }
        },
        child: Material(
          elevation: UIData.percentHeight(context, 7),
          borderRadius: BorderRadius.circular(20.0),
          shadowColor: UIData.joltBlueAccent,
          type: MaterialType.card,
          color: homeMenu.color,
          child: Stack(
            fit: StackFit.expand,
            children: <Widget>[
              menuSetBackground(homeMenu, context),
              menuData(homeMenu, context),
            ],
          ),
        ),
      )
    );

  

  final grid = CustomScrollView(
      primary: false,
      slivers: <Widget>[
        SliverPadding(
          padding: EdgeInsets.symmetric(horizontal: UIData.percentWidth(context, 16), vertical: UIData.percentHeight(context, 16)),
          sliver: SliverGrid.count(
            childAspectRatio: 2 / 3,
            crossAxisCount: 2,
            mainAxisSpacing: 10.0,
            crossAxisSpacing: 10.0,
            children: menus.map((menu) => createTile(menu)).toList(),
          ),
        )
      ],
  );

  return Scaffold(
      body: grid,
    );
  }
  
}