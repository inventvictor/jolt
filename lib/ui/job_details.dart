import 'package:flutter/material.dart';
import 'package:font_awesome_flutter/font_awesome_flutter.dart';
import 'package:jolt/models/job_model.dart';
import 'package:jolt/utils/uidata.dart';
import 'package:jolt/views/activity_tile.dart';
import 'view_photo.dart';
import 'package:shared_preferences/shared_preferences.dart';
import 'package:jolt/utils/career_session_manager.dart';
import 'dart:async';
import 'package:url_launcher/url_launcher.dart';

class JobDetails extends StatefulWidget {
  JobDetails({this.jobModel});
  final JobModel jobModel;

  @override
  _JobDetailsState createState() => new _JobDetailsState();
}


class _JobDetailsState extends State<JobDetails> {
  Size deviceSize;
  List<ActivityTile> activityTiles = [];

  SessionManager _sessionManager;

  Future<Object> initialisePrefs() async {
    SharedPreferences prefs = await SharedPreferences.getInstance();
    SessionManager sessionManager = new SessionManager(prefs);
    return sessionManager;
  }
  
  
  @override
  void initState() {
    super.initState();
    initialisePrefs().then((sessionManager){
      if (sessionManager != null){
        setState(() {
            _sessionManager = sessionManager;
        });
      }
    });
  }

  @override
  Widget build(BuildContext context) {
    deviceSize = MediaQuery.of(context).size;
    try{

      if (_sessionManager.getSessionDetails() != null){
        return SafeArea(
          child: Scaffold(
            body: Stack(
              children: <Widget>[
                Positioned(
                  top: 0,
                  left: 0,
                  right: 0,
                  height: MediaQuery.of(context).size.height / 5,
                  child: Image.network(
                    "https://firebasestorage.googleapis.com/v0/b/jolt-f19ca.appspot.com/o/jolt_job.webp?alt=media&token=c35a90cb-ad2f-46a1-86a6-ca9e7e2500af",
                    fit: BoxFit.cover,
                    color: Colors.black38,
                    colorBlendMode: BlendMode.darken,
                  ),
                ),
                Positioned(
                  top: 0,
                  left: 0,
                  right: 0,
                  child: Row(
                    children: <Widget>[
                      IconButton(
                        icon: Icon(
                          Icons.chevron_left,
                          color: Colors.white,
                          size: UIData.percentHeight(context, 60),
                        ),
                        onPressed: () => Navigator.pop(context),
                      ),
                    ],
                  ),
                ),
                Positioned(
                  left: 0,
                  right: 0,
                  bottom: 0,
                  height: MediaQuery.of(context).size.height * 0.8,
                  child: Container(
                    padding: const EdgeInsets.all(15.0),
                    decoration: BoxDecoration(
                      color: Colors.white,
                      borderRadius: BorderRadius.only(
                        topLeft: Radius.circular(25),
                        topRight: Radius.circular(25),
                      ),
                    ),
                    child: SingleChildScrollView(
                      child: Column(
                        crossAxisAlignment: CrossAxisAlignment.start,
                        children: <Widget>[
                          Text(
                            "${widget.jobModel.title}",
                            style: Theme.of(context).textTheme.headline.apply(color: UIData.joltBlueAccent, fontFamily: "WorkSansBold"),
                          ),
                          Text(
                            "${widget.jobModel.location}",
                            style: Theme.of(context)
                                .textTheme
                                .body2
                                .apply(color: Colors.grey, fontFamily: "WorkSansBold"),
                          ),
                          SizedBox(
                            height: UIData.percentHeight(context, 20)
                          ),
                          Text(
                            "Company",
                            style: Theme.of(context).textTheme.subhead.apply(color: UIData.joltBlueAccent, fontFamily: "WorkSansSemiBold"),
                          ),
                          Text(
                            "${widget.jobModel.description}",
                            style: Theme.of(context)
                                .textTheme
                                .body2
                                .apply(color: Colors.grey, fontFamily: "WorkSansMedium"),
                          //maxLines: 3,
                          ),
                          SizedBox(
                            height: UIData.percentHeight(context, 20),
                          ),
                          Container(
                            width: MediaQuery.of(context).size.height * .7,
                            height: UIData.percentHeight(context, 70),
                            child: RaisedButton(
                              child: Text(
                                "Are you interested?, Apply",
                                style: Theme.of(context)
                                    .textTheme
                                    .button
                                    .apply(color: Colors.white, fontFamily: "WorkSansMedium"),
                              ),
                              color: UIData.joltBlueAccent,
                              onPressed: () async{
                                print (widget.jobModel.url);
                                //open up the default email of the user
                                //if (await canLaunch(widget.jobModel.url))
                                launch('https://'+widget.jobModel.url);
                                //else {}
                              },
                            ),
                          )
                        ],
                      ),
                    ),
                  ),
                )
              ],
            ),
          ),
        );
      }
      else{
        return Container(
            child: Padding(
                padding: EdgeInsets.symmetric(vertical: UIData.percentHeight(context, 5), horizontal: UIData.percentWidth(context, 5)),
                child: Center(
                  child: CircularProgressIndicator()
                ),
              )
            );
      }

    }
    catch(e){
      return Container(
            child: Padding(
                padding: EdgeInsets.symmetric(vertical: UIData.percentHeight(context, 5), horizontal: UIData.percentWidth(context, 5)),
                child: Center(
                  child: CircularProgressIndicator()
                ),
              )
            );
    }
    
  }
}
