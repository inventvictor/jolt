import 'package:flutter/material.dart';
import 'package:jolt/views/job_container.dart';
import 'package:jolt/ui/job_details.dart';
import 'package:jolt/views/job_drop_down_button.dart';
import 'package:jolt/models/job_model.dart';
import 'package:jolt/utils/uidata.dart';
import 'package:jolt/ui/view_photo.dart';
import 'package:shared_preferences/shared_preferences.dart';
import 'package:jolt/utils/career_session_manager.dart';
import 'dart:async';
import 'package:jolt/auth/jolt_auth.dart';

class JobPage extends StatefulWidget {
  JobPage({this.auth});
  final AuthImpl auth;
  @override
  _JobPageState createState() => new _JobPageState();
}


class _JobPageState extends State<JobPage> {
  Size deviceSize;
  SessionManager _sessionManager;
  bool isLoadingDropdownMenuItems = true;
  bool isLoadingListViewItems = false;
  List<String> menuItems = [];
  List<JobModel> jobModels = [];
  int _dropdownActive = 0;
  String _search = "";
  //List<JobModel> searchedItems = [];

  Future<Object> initialisePrefs() async {
    SharedPreferences prefs = await SharedPreferences.getInstance();
    SessionManager sessionManager = new SessionManager(prefs);
    return sessionManager;
  }
  
  Future<List<String>> getJobSections(AuthImpl auth) async{
    return auth.grabJobSections(context);
  }

  List<DropdownMenuItem<int>> getDropdownMenuItems(List<String> list){
    List<DropdownMenuItem<int>> dropdownMenuItems = [];
    for (int i=0; i<list.length; i++){
      dropdownMenuItems.add(DropdownMenuItem(child: Text(list[i], style: TextStyle(color: UIData.joltBlueAccent, fontSize: UIData.percentHeight(context, 25), fontFamily: "WorkSansSemiBold"),), value: i));
    }
    return dropdownMenuItems;
  }

  Future<List<JobModel>> getJobs(AuthImpl authImpl, String section) async{
    List<JobModel> jobModels = [];
    Map<dynamic, dynamic> val = await authImpl.grabJobsBySection(context, section);
    for(int i=0; i<val.length; i++){
      Map<dynamic, dynamic> eachVal = val.values.elementAt(i);
      JobModel jobModel = JobModel(
        description: eachVal['desc'], 
        iconUrl: eachVal['iconUrl'], 
        location: eachVal['location'], 
        salary: eachVal['salary'], 
        title: eachVal['title'],
        url: eachVal['url']);
      jobModels.add(jobModel);
    }
    return jobModels;
  }

  Future<List<JobModel>> getJobsSearch(AuthImpl authImpl, String section, String query) async{
    List<JobModel> jobModels = [];
    Map<dynamic, dynamic> val = await authImpl.getJobsSearchBySection(context, section, query);
    for(int i=0; i<val.length; i++){
      Map<dynamic, dynamic> eachVal = val.values.elementAt(i);
      JobModel jobModel = JobModel(
        description: eachVal['desc'], 
        iconUrl: eachVal['iconUrl'], 
        location: eachVal['location'], 
        salary: eachVal['salary'], 
        title: eachVal['title'],
        url: eachVal['url']);
      jobModels.add(jobModel);
    }
    return jobModels;
  }
  
  @override
  void initState() {
    super.initState();
    initialisePrefs().then((sessionManager){
      if(sessionManager != null){
        setState(() {
            _sessionManager = sessionManager;
        });
      }
    });
    getJobSections(widget.auth).then((jobSections){
      setState(() {
        isLoadingDropdownMenuItems = false;
        menuItems = jobSections;
        isLoadingListViewItems = true;
      });
      if(isLoadingListViewItems){
        getJobs(widget.auth, menuItems.elementAt(_dropdownActive)).then((retVal){
            setState(() {
              jobModels = retVal;
              //searchedItems = retVal;
              isLoadingListViewItems = false;
            });
          });
      }
    });
  }

  // void filterSearchResults(String query) {
  //   List<JobModel> jobModelList = List<JobModel>();
  //   jobModelList.addAll(jobModels);
  //   if(query.isNotEmpty) {
  //     List<JobModel> jobModelListData = List<JobModel>();
  //     jobModelList.forEach((item) {
  //       if(item.title.contains(query) || item.location.contains(query)) {
  //         jobModelListData.add(item);
  //       }
  //     });
  //     setState(() {
  //       searchedItems.clear();
  //       searchedItems.addAll(jobModelListData);
  //     });
  //     return;
  //   } else {
  //     setState(() {
  //       searchedItems.clear();
  //       searchedItems.addAll(jobModels);
  //     });
  //   }
  // }

  @override
  Widget build(BuildContext context) {
    deviceSize = MediaQuery.of(context).size;
    try{
        if (_sessionManager.getSessionDetails() != null){
          if (isLoadingDropdownMenuItems){
            return Container(
              child: Padding(
                  padding: EdgeInsets.symmetric(vertical: UIData.percentHeight(context, 5), horizontal: UIData.percentWidth(context, 5)),
                  child: Center(
                    child: CircularProgressIndicator()
                  ),
                )
              );
          }
          else{
            return Scaffold(
              backgroundColor: Colors.white,
              body: Stack(
                children: <Widget>[
                  Positioned(
                    top: 0,
                    right: 0,
                    left: 0,
                    bottom: 0,
                    child: Padding(
                      padding: EdgeInsets.symmetric(horizontal: UIData.percentWidth(context, 20)),
                      child: Column(
                        children: <Widget>[
                          SizedBox(
                            height: UIData.percentHeight(context, 10),
                          ),
                          Row(
                            mainAxisAlignment: MainAxisAlignment.spaceBetween,
                            children: <Widget>[
                              DropdownButton(
                                iconEnabledColor: UIData.joltBlueAccent,
                                underline: Container(),
                                onChanged: (f){
                                  setState(() {
                                    _dropdownActive = f;
                                    isLoadingListViewItems = true;
                                    getJobs(widget.auth, menuItems.elementAt(_dropdownActive)).then((retVal){
                                      setState(() {
                                        jobModels = retVal;
                                        isLoadingListViewItems = false;
                                      });
                                    });
                                  });
                                },
                                value: _dropdownActive,
                                style: Theme.of(context).textTheme.headline,
                                items: getDropdownMenuItems(menuItems)
                              ),
                              GestureDetector(
                                child: CircleAvatar(
                                  radius: UIData.percentHeight(context, 30),
                                  backgroundImage: NetworkImage(_sessionManager.getSessionDetails()['avatar'])
                                ),
                                onTap:(){
                                  Navigator.push(
                                    context,
                                    new MaterialPageRoute(
                                      builder: (context) => new ViewPhotoPage(imageFilePath: _sessionManager.getSessionDetails()['avatar'],fromProfileView: true,),
                                    ), //MaterialPageRoute
                                  );
                                }
                              )
                            ],
                          ),
                          SizedBox(
                            height: UIData.percentHeight(context, 20)
                          ),
                          Container(
                            height: UIData.percentHeight(context, 73),
                            child: Row(
                              mainAxisAlignment: MainAxisAlignment.spaceBetween,
                              children: <Widget>[
                                Expanded(
                                  flex: 4,
                                  child: TextField(
                                    onChanged: (value){
                                      //filterSearchResults(value);
                                      _search = value;
                                      if(value.length == 0){
                                        isLoadingListViewItems = true;
                                        getJobs(widget.auth, menuItems.elementAt(_dropdownActive)).then((retVal){
                                          setState(() {
                                            jobModels = retVal;
                                            isLoadingListViewItems = false;
                                          });
                                        });
                                      }
                                    },
                                    decoration: InputDecoration(
                                      prefixIcon: Icon(Icons.search),
                                      hintText: "Search",
                                      border: OutlineInputBorder(
                                          borderRadius: BorderRadius.circular(15.0),
                                          borderSide: BorderSide.none),
                                      fillColor: Color(0xffe6e6ec),
                                      filled: true,
                                    ),
                                  ),
                                ),
                                Spacer(
                                  flex: 1,
                                ),
                                Expanded(
                                  flex: 2,
                                  child: RaisedButton(
                                    shape: RoundedRectangleBorder(
                                      borderRadius: new BorderRadius.circular(15.0),
                                      side: BorderSide(color: UIData.joltBlueAccent)
                                    ),
                                    color: Colors.white,
                                    child: new Text(
                                      'Go',
                                      style: new TextStyle(
                                        fontSize: UIData.percentHeight(context, 20),
                                        color: UIData.joltBlueAccent
                                      ),
                                    ),
                                    onPressed: (){
                                      isLoadingListViewItems = true;
                                      getJobsSearch(widget.auth, menuItems.elementAt(_dropdownActive), _search).then((retVal){
                                        setState(() {
                                          jobModels = retVal;
                                          isLoadingListViewItems = false;
                                        });
                                      });
                                    },
                                  )
                                ),
                              ],
                            ),
                          ),
                          SizedBox(
                            height: UIData.percentHeight(context, 20),
                          ),
                          Expanded(
                            child: isLoadingListViewItems == false ? ListView.builder(
                              itemCount: jobModels.length,
                              itemBuilder: (ctx, i) {
                                return JobContainer(
                                  description: jobModels[i].description,
                                  iconUrl: jobModels[i].iconUrl,
                                  location: jobModels[i].location,
                                  salary: jobModels[i].salary,
                                  title: jobModels[i].title,
                                  onTap: () => Navigator.push(
                                    context,
                                    MaterialPageRoute(
                                      builder: (ctx) => JobDetails(jobModel: jobModels[i],),
                                    ),
                                  ),
                                );
                              },
                            ) : Container(
                                  child: Padding(
                                    padding: EdgeInsets.symmetric(vertical: UIData.percentHeight(context, 5), horizontal: UIData.percentWidth(context, 5)),
                                    child: Center(
                                      child: CircularProgressIndicator()
                                    ),
                              )
                            ),
                          )
                        ],
                      ),
                    ),
                  )
                ],
              ),
            );
          }
      }
      else{
        return Container(
            child: Padding(
                padding: EdgeInsets.symmetric(vertical: UIData.percentHeight(context, 5), horizontal: UIData.percentWidth(context, 5)),
                child: Center(
                  child: CircularProgressIndicator()
                ),
              )
            );
      }
    }
    catch(e){
      return Container(
            child: Padding(
                padding: EdgeInsets.symmetric(vertical: UIData.percentHeight(context, 5), horizontal: UIData.percentWidth(context, 5)),
                child: Center(
                  child: CircularProgressIndicator()
                ),
              )
            );
    }
    
  }
}