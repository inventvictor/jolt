import 'package:flutter/material.dart';
import 'package:shared_preferences/shared_preferences.dart';
import 'package:jolt/utils/career_session_manager.dart';
import 'login_page.dart';
import 'home.dart';
import 'package:jolt/auth/jolt_auth.dart';

class SecondRootPage extends StatefulWidget {
  SecondRootPage({Key key}) : super(key: key);

  @override
  State<StatefulWidget> createState() => new _SecondRootPageState();
}

// enum AuthStatus {
//   NOT_SIGNED_IN,
//   SIGNED_IN_CAREER,
//   SIGNED_IN_BUSINESS
// }

class _SecondRootPageState extends State<SecondRootPage> {
  //AuthStatus authStatus = AuthStatus.NOT_SIGNED_IN;
  SessionManager _sessionManager;

  Future<SessionManager> initialisePrefs() async {
    SharedPreferences prefs = await SharedPreferences.getInstance();
    SessionManager sessionManager = new SessionManager(prefs);
    return sessionManager;
  }

  @override
  void initState() {
    super.initState();
    initialisePrefs().then((sessionManager){
      setState(() {
          _sessionManager = sessionManager;
          // try{
          //   if(_sessionManager.getSessionDetails()['is_logged_in']){
          //     authStatus = AuthStatus.SIGNED_IN_CAREER;
          //   }
          //   else{
          //     authStatus = AuthStatus.NOT_SIGNED_IN;
          //   }
          // }
          // catch(e){
          //   //print(e.toString());
          //   authStatus = AuthStatus.NOT_SIGNED_IN;
          // }
          
      });
    });
  }

  @override
  Widget build(BuildContext context) {
    if(_sessionManager == null){
      return Scaffold(
        body: Container(
          
          alignment: Alignment.center,
          child: CircularProgressIndicator(),
        ),
      );
    }
    else{
      //print(_sessionManager.getSessionDetails()['is_logged_in']);
      if(_sessionManager.getSessionDetails()['is_logged_in']){
        return new Home(showWelcome: false, showUpdateProfile: _sessionManager.getSessionDetails()['has_updated_profile']);
      }
      else if(!_sessionManager.getSessionDetails()['is_logged_in']){
        return new LoginPage(auth: new Auth());
      }
      // switch (authStatus) {
      //   case AuthStatus.NOT_SIGNED_IN:
      //     return new LoginPage(auth: new Auth());

      //   case AuthStatus.SIGNED_IN_CAREER:
      //     return new Home(); //Essentially this is the career home!

      //   case AuthStatus.SIGNED_IN_BUSINESS:
      //     // return new HomePage(
      //     //   auth: widget.auth,
      //     //   onSignedOut: _signedOut,
      //     // );
      // }
    }
    
  }
}