import 'package:flutter/material.dart';
import 'package:jolt/utils/uidata.dart';
import 'package:shared_preferences/shared_preferences.dart';
import 'package:jolt/utils/career_session_manager.dart';
import 'package:firebase_database/firebase_database.dart';
import 'package:jolt/views/common_scaffold.dart';
import 'package:font_awesome_flutter/font_awesome_flutter.dart';
import 'package:fluttertoast/fluttertoast.dart';
import 'dart:convert';
import 'dart:async';

class CurrentEmployeePage extends StatefulWidget {
  CurrentEmployeePage({Key key, this.begin}) : super(key: key);
  bool begin;
  @override
  _CurrentEmployeePageState createState() => new _CurrentEmployeePageState();
}

class _CurrentEmployeePageState extends State<CurrentEmployeePage> {
    Size deviceSize;

    //List<String> subtitle_checklist = [];
    //List<String> title_checklist = [];
    Map<int, Map<String,String>> checklistMap = {};
    Map<String, bool> checklistedMap = {};
    bool _showDialog = true;
    bool showUpdateDialog = true;
    bool _value = false;

    SessionManager _sessionManager;

    Future<Object> initialisePrefs() async {
      SharedPreferences prefs = await SharedPreferences.getInstance();
      SessionManager sessionManager = new SessionManager(prefs);
      return sessionManager;
    }

    @override
    void initState() {
        super.initState();
        initialisePrefs().then((sessionManager){
          setState(() {
                _sessionManager = sessionManager;
            });
        });
        loadChecklist();
    }

    Future<Null> loadChecklist() async {
        //List<String> subtitle_currentCheckLists = [];
        //List<String> title_currentCheckLists = [];
        //print("showDialog - $showDialog");
        Map<int, Map<String,String>> currentChecklistMap = {};
        Map<String, bool> currentChecklistedMap = {};
        DatabaseReference databaseReference = FirebaseDatabase.instance.reference().child("checklist").child("employee_checklist");
        await databaseReference.once().then((DataSnapshot snapshot){
                  Map<dynamic, dynamic> values = snapshot.value;
                  values.forEach((key,values) {
                      //print("$key + "','" + $values");
                      int lCount = key.toString().trim().split(" ").length;
                      int index = int.parse(key.toString().trim().split(" ").removeAt(0));
                      String title = key.toString().trim().split(" ").sublist(1, lCount).join(" ").replaceAll('"', '');
                      String subTitle = values.toString();
                      currentChecklistMap[index] = {"$title":"$subTitle"};
                    }
                  );
                }
              );
        try{
          DatabaseReference databaseReference2 = FirebaseDatabase.instance.reference().child("checklisted").child("employee_checklist").child(_sessionManager.getSessionDetails()['user_id']);
          await databaseReference2.once().then((DataSnapshot snapshot){
            Map<dynamic, dynamic> values2 = snapshot.value;
            values2.forEach((key,val){
              //print("$key + "','" + $val");
              Map<String, dynamic> data = json.decode(val);
              
              //print(data);
              data.forEach((k,v){
                currentChecklistedMap['"'+k+'"'] = v as bool;
              });
            });
          });
        }
        catch(e){
          //print(e.toString());
        }
        
        setState(() {
            _showDialog = false;
            checklistMap = currentChecklistMap;
            checklistedMap = currentChecklistedMap;
        });
        //print("showDialog - $showDialog");
    }

    @override
    Widget build(BuildContext context) {
        // final list = CustomScrollView(
        //     primary: false,
        //     slivers: <Widget>[
        //       SliverPadding(
        //         padding: EdgeInsets.all(16.0),
        //         sliver: ``
        //       )
        //     ],
        // );
        deviceSize = MediaQuery.of(context).size;
        return CommonScaffold(
          appTitle: "Current Employee",
          actionFirstIconFunc: (){
            showProgress(context);
            try{
              Future.delayed(new Duration(seconds: 2), ()async{
                FirebaseDatabase.instance.reference().child("checklisted").child("employee_checklist").child(_sessionManager.getSessionDetails()['user_id']).set({
                    "checklisted":checklistedMap.toString()
                  });
                await FirebaseDatabase.instance.reference().child("activities").child(_sessionManager.getSessionDetails()['user_id']).push().child(new DateTime.now().millisecondsSinceEpoch.toString()).set("careerchecklist_update");
                                  
                Navigator.pop(context);
                showSuccess(context); //may revisit later on!
              });
              
            }
            catch(e){
              Navigator.pop(context);
              Fluttertoast.showToast(
                  msg: "Something went wrong!",
                  toastLength: Toast.LENGTH_SHORT,
                  gravity: ToastGravity.CENTER,
                  timeInSecForIos: 1,
                  backgroundColor: Colors.red,
                  textColor: Colors.white,
              );
            }
            
          },
          actionFirstIcon: FontAwesomeIcons.check,
          showImageNotIcon: true,
          bColor: UIData.joltBlueAccent,
          bodyData: new Container(
            child: _showDialog? new Center(
              child: CircularProgressIndicator(),
            ):getPage()
          ),
        );
    }

    Widget getPage() {
        //if(!showDialog) {
            return new ListView.builder(
                itemCount: checklistMap.length,
                itemBuilder: (BuildContext context, int i) {
                    int io = i+1;
                    String ios = io.toString();
                    return new Container(
                        child: Material(
                              elevation: UIData.percentHeight(context, 8),
                              borderRadius: BorderRadius.circular(5.0),
                              shadowColor: UIData.joltBlueAccent,
                              type: MaterialType.card,
                              color: Colors.white,
                              child: CheckboxListTile(
                                value: checklistedMap['"'+ios+'"']==null?false:checklistedMap['"'+ios+'"'],
                                onChanged: (bool newValue){
                                  ////print(newValue);
                                  ////print(checklistedMap[i+1]);
                                  setState(() {
                                    checklistedMap['"'+ios+'"'] = newValue;
                                  });
                                },
                                activeColor: UIData.joltBlueAccent,
                                title: new Text(
                                      checklistMap[i+1].keys.toList()[0],
                                      style: new TextStyle(
                                        color: Colors.black,
                                        fontFamily: "WorkSansBold",
                                        fontSize: UIData.percentHeight(context, 22)
                                      ),
                                    ),
                                    isThreeLine: true,
                                subtitle: new Text(
                                      checklistMap[i+1].values.toList()[0],
                                      style: new TextStyle(
                                        color: Colors.black,
                                        fontFamily: "WorkSansMedium",
                                        fontSize: UIData.percentHeight(context, 20)
                                      ),
                                    ),
                              )
                            ),
                        padding: EdgeInsets.only(left: UIData.percentWidth(context, 20), right: UIData.percentWidth(context, 20), top: UIData.percentHeight(context, 20), bottom: 0.0)
                    );
                }
            );
        // } else {
        //     return new Center(
        //         child: new Container(
        //             child: new CircularProgressIndicator()
        //         )
        //     );
        // }
    }

    Future<Null> showProgress(BuildContext context){
      return showDialog(
              context: context,
              barrierDismissible: false,
              builder: (BuildContext context){
                return new WillPopScope(
                  onWillPop: (){
                    Future.value(false);
                  },
                  child: SimpleDialog(
                    contentPadding: EdgeInsets.all(0.0),
                    children: <Widget>[
                      new Container(
                        height: UIData.percentHeight(context, 80),
                        decoration: new BoxDecoration(
                          color: UIData.joltBlueAccent,
                        ),
                        child: new Center(
                          child: Icon(
                            FontAwesomeIcons.user,
                            color: Colors.white,
                          ),
                        )),
                      new SizedBox(
                        height: UIData.percentHeight(context, 20),
                      ),
                      new Center(
                        child: CircularProgressIndicator(),
                      ),
                      new SizedBox(
                        height: UIData.percentHeight(context, 20),
                      ),
                      new Center(
                        child: new Text("Updating please wait...",
                          style: TextStyle(
                            fontSize: UIData.percentHeight(context, 18),
                            fontFamily: "WorkSansSemiBold"
                          ),
                        ),
                      ),
                      new SizedBox(
                        height: UIData.percentHeight(context, 20),
                      ),
                    ],
                  ),
                ); 
              }
            );
    }

    Future<Null> showSuccess(BuildContext context){
      return showDialog(
                context: context,
                barrierDismissible: true,
                builder: (BuildContext context){
                  return SimpleDialog(
                      contentPadding: EdgeInsets.all(0.0),
                      children: <Widget>[
                        new Container(
                          height: UIData.percentHeight(context, 80),
                          decoration: new BoxDecoration(
                            color: Colors.greenAccent,
                          ),
                          child: new Center(
                            child: Icon(
                              FontAwesomeIcons.check,
                              color: Colors.white,
                            ),
                          )),
                        new SizedBox(
                          height: UIData.percentHeight(context, 20),
                        ),
                        new Container(
                          padding: EdgeInsets.symmetric(vertical: UIData.percentHeight(context, 12), horizontal: UIData.percentWidth(context, 12)),
                          child: Column(
                            children: <Widget>[
                              new Center(
                                  child: new Text("Successfully Updated",
                                    style: TextStyle(
                                      fontSize: UIData.percentHeight(context, 20),
                                      fontFamily: "WorkSansSemiBold"
                                    ),
                                    textAlign: TextAlign.center,
                                  ),
                                ),
                              new SizedBox(
                                height: UIData.percentHeight(context, 20),
                              ),
                              new Padding(
                                padding: EdgeInsets.symmetric(vertical: UIData.percentHeight(context, 10), horizontal: UIData.percentWidth(context, 10)),
                              )
                                ],
                              ),
                        ),
                      ],
                  ); 
                }
              ); 
    }

}

