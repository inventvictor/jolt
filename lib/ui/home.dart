import 'package:flutter/material.dart';
import 'package:jolt/ui/job_page.dart';
import 'package:jolt/auth/jolt_auth.dart';
import 'package:jolt/ui/home_page.dart';
import 'package:jolt/utils/uidata.dart';
import 'package:shared_preferences/shared_preferences.dart';
import 'package:jolt/utils/career_session_manager.dart';
import 'second_root_page.dart';
import 'package:firebase_auth/firebase_auth.dart';
import 'edit_profile_page.dart';
import 'package:firebase_database/firebase_database.dart';
import 'package:fluttertoast/fluttertoast.dart';
import 'package:font_awesome_flutter/font_awesome_flutter.dart';
import 'dart:core';
import 'dart:async';
import 'write_more.dart';

class Home extends StatefulWidget {
  Home({this.showWelcome, this.showUpdateProfile, this.firstname});
  bool showWelcome;
  bool showUpdateProfile;
  String firstname;
  @override
  State<StatefulWidget> createState() {
    return _HomeState();
  }
}

class _HomeState extends State<Home> {
  var feedbackTextField = new TextEditingController();
  int _currentIndex = 0;
  final List<Widget> _children = [
    HomePage(),
    JobPage(auth: new Auth(),)
  ];

  SessionManager _sessionManager;

  Future<Object> initialisePrefs() async {
    SharedPreferences prefs = await SharedPreferences.getInstance();
    SessionManager sessionManager = new SessionManager(prefs);
    return sessionManager;
  }

  Future<Null> showWelcomeDialog(String msg){
    return showDialog(
      context: context,
      barrierDismissible: true,
      builder: (BuildContext context){
        return SimpleDialog(
            contentPadding: EdgeInsets.all(0.0),
            children: <Widget>[
              new Image(
                image: AssetImage(UIData.pleaseWaitImage),
              ),
              new SizedBox(
                height: UIData.percentHeight(context, 20),
              ),
              new Container(
                padding: EdgeInsets.symmetric(vertical: UIData.percentHeight(context, 12), horizontal: UIData.percentWidth(context, 12)),
                child: Column(
                  children: <Widget>[
                    new Center(
                        child: new Text(msg,
                          style: TextStyle(
                            fontSize: UIData.percentHeight(context, 20),
                            fontFamily: "WorkSansSemiBold"
                          ),
                          textAlign: TextAlign.center,
                        ),
                      ),
                    new SizedBox(
                      height: UIData.percentHeight(context, 20),
                    ),
                    new Padding(
                      padding: EdgeInsets.symmetric(vertical: UIData.percentHeight(context, 10), horizontal: UIData.percentWidth(context, 10)),
                    )
                      ],
                    ),
              ),
            ],
        ); 
      }
    ); 
  }
  @override
  void initState() {
    super.initState();
    initialisePrefs().then((sessionManager){
      setState(() {
            _sessionManager = sessionManager;
        });
    });

    // if(!widget.showUpdateProfile){
    //   //print("I am here oooooooooo");
    //   new Future.delayed(new Duration(seconds: 0), (){
    //     return showUpdatedProfileDialog(context);
    //   });
    // }
    
    if(widget.showWelcome && !widget.showUpdateProfile){
      //print("showWelcome by Victor");
      new Future.delayed(new Duration(seconds:0), () {
        
        showDialog(
          context: context,
          barrierDismissible: false,
          builder: (BuildContext context){
            return SimpleDialog(
                contentPadding: EdgeInsets.all(0.0),
                children: <Widget>[
                  new Image(
                    image: AssetImage(UIData.holaImage),
                  ),
                  new SizedBox(
                    height: UIData.percentHeight(context, 5),
                  ),
                  new Container(
                    padding: EdgeInsets.symmetric(vertical: UIData.percentHeight(context, 7), horizontal: UIData.percentWidth(context, 7)),
                    child: Column(
                      children: <Widget>[
                            new Center(
                                child: new Text("Welcome "+widget.firstname,
                                  style: TextStyle(
                                    fontSize: UIData.percentHeight(context, 20),
                                    fontWeight: FontWeight.w700,
                                    fontFamily: "WorkSansSemiBold"
                                  ),
                                  textAlign: TextAlign.center,
                                ),
                              ),
                            new SizedBox(
                              height: UIData.percentHeight(context, 5),
                            ),
                            new Center(
                                child: new Text("Lets grow your Career together",
                                  style: TextStyle(
                                    fontSize: UIData.percentHeight(context, 20),
                                    fontFamily: "WorkSansSemiBold"
                                  ),
                                  textAlign: TextAlign.center,
                                ),
                              ),
                            new SizedBox(
                              height: UIData.percentHeight(context, 15),
                            ),
                            RaisedButton(
                              elevation: UIData.percentHeight(context, 10),
                              child: Text('Continue',
                              style: TextStyle(
                                color: UIData.joltBlueAccent,
                                fontFamily: "WorkSansMedium",
                                fontSize: UIData.percentHeight(context, 20)
                              ),),
                              color: Colors.white,
                              onPressed: (){
                                Navigator.pop(context);
                                if(widget.showUpdateProfile){
                                  return showUpdatedProfileDialog(context);
                                }
                              },
                            ),
                            new SizedBox(
                              height: UIData.percentHeight(context, 15),
                            ),
                          ],
                        ),
                  ),
                ],
            ); 
          }
        ); 
      });
    }
  }

  Future<Null> logout() async{
    FirebaseAuth _firebaseAuth = FirebaseAuth.instance;
    await _firebaseAuth.signOut();
    _sessionManager.clearLoginSession();
  }

  Future<Null> showUpdatedProfileDialog(BuildContext context) async{
    return showDialog(
          barrierDismissible: false,
          context: context,
          builder: (BuildContext context){
            return SimpleDialog(
              contentPadding: EdgeInsets.symmetric(vertical: UIData.percentHeight(context, 10), horizontal: UIData.percentWidth(context, 10)),
              children: <Widget>[
                  new Text(
                    'Update Profile',
                    style: TextStyle(
                      color: Colors.black,
                      fontFamily: "WorkSansSemiBold",
                      fontSize: UIData.percentHeight(context, 20)
                    ),
                  ),
                  SizedBox(
                    height: UIData.percentHeight(context, 15),
                  ),
                  new Text(
                    'Please update your profile. An Updated profile makes you more attractive to your business contacts',
                    style: TextStyle(
                      color: Colors.black,
                      fontFamily: "WorkSansMedium",
                      fontSize: UIData.percentHeight(context, 20)
                    ),
                  ),
                  SizedBox(
                    height: UIData.percentHeight(context, 10),
                  ),
                  new Row(
                    mainAxisAlignment: MainAxisAlignment.center,
                    children: <Widget>[
                      RaisedButton(
                        elevation: UIData.percentHeight(context, 10),
                        child: Text('Continue',
                        style: TextStyle(
                          color: UIData.joltBlueAccent,
                          fontFamily: "WorkSansMedium",
                          fontSize: UIData.percentHeight(context, 20)
                        ),),
                        color: Colors.white,
                        onPressed: (){
                          Navigator.pop(context);
                          Navigator.push(
                                    context,
                                    new MaterialPageRoute(
                                      builder: (context) => new EditProfilePage(),
                                    ),
                                  );
                        },
                      ),
                    ],
                  ),
                ],
              
            );
          }
        );
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text('JOLT', style: TextStyle(fontSize: UIData.percentHeight(context, 25),fontFamily: "WorkSansBold"),),
        backgroundColor: UIData.joltBlueAccent,
        leading: new Container(),
        actions: [
          new PopupMenuButton<OverflowItem>(
              onSelected: _overflow,
              itemBuilder: (BuildContext context) {
                return [
                  new PopupMenuItem(value: OverflowItem.GiveFeedback,
                      child: new Row(
                        mainAxisAlignment: MainAxisAlignment.start,
                        children: <Widget>[
                          Icon(Icons.feedback,
                          color: UIData.joltBlueAccent,),
                          Padding(padding: EdgeInsets.symmetric(vertical: UIData.percentHeight(context, 5), horizontal: UIData.percentWidth(context, 5)),),
                          Text('Give Feedback')
                        ],
                      )),
                  new PopupMenuItem(value: OverflowItem.MyProfile,
                      child: new Row(
                        mainAxisAlignment: MainAxisAlignment.start,
                        children: <Widget>[
                          
                          Icon(Icons.account_circle,
                          color: UIData.joltBlueAccent,),
                          Padding(padding: EdgeInsets.symmetric(vertical: UIData.percentHeight(context, 5), horizontal: UIData.percentWidth(context, 5)),),
                          Text('My Profile')
                        ],
                      )),
                  // new PopupMenuItem(value: OverflowItem.Settings,
                  //     child: new Row(
                  //       mainAxisAlignment: MainAxisAlignment.start,
                  //       children: <Widget>[
                  //         Icon(Icons.settings,
                  //         color: UIData.joltBlueAccent,),
                  //         Padding(padding: EdgeInsets.symmetric(vertical: UIData.percentHeight(context, 5), horizontal: UIData.percentWidth(context, 5)),),
                  //         Text('Settings')
                  //       ],
                  //     )),
                  new PopupMenuItem<OverflowItem>(
                      value: OverflowItem.LogOut, 
                      child: new Row(
                        mainAxisAlignment: MainAxisAlignment.start,
                        children: <Widget>[
                          Icon(Icons.cancel,
                          color: Colors.redAccent,),
                          Padding(padding: EdgeInsets.symmetric(vertical: UIData.percentHeight(context, 5), horizontal: UIData.percentWidth(context, 5)),),
                          Text('Logout')
                        ],
                      ))
                ];
              })
          ],
      ),
      body: _children[_currentIndex],
      bottomNavigationBar: BottomNavigationBar(
        fixedColor: UIData.joltBlueAccent,
        onTap: onTabTapped,
        type: BottomNavigationBarType.fixed,
        currentIndex: _currentIndex,
        items: [
          BottomNavigationBarItem(
            icon: Icon(Icons.home),
            backgroundColor: UIData.joltBlueAccent,
            title: Text('Home'),
          ),
          // BottomNavigationBarItem(
          //   icon: Icon(Icons.store),
          //   backgroundColor: UIData.joltBlueAccent,
          //   title: Text('Store'),
          // ),
          BottomNavigationBarItem(
            icon: Icon(Icons.developer_board),
            backgroundColor: UIData.joltBlueAccent,
            title: Text('Job Board')
          ),
          // BottomNavigationBarItem(
          //   icon: Icon(Icons.chat),
          //   backgroundColor: UIData.joltBlueAccent,
          //   title: Text('Chats')
          // )
        ],
      ),
    );
  }

  void onTabTapped(int index) {
    setState(() {
      _currentIndex = index;
    }); 
  }

  Future<Null> showSuccess(BuildContext context){
      return showDialog(
                context: context,
                barrierDismissible: true,
                builder: (BuildContext context){
                  return SimpleDialog(
                      contentPadding: EdgeInsets.all(0.0),
                      children: <Widget>[
                        new Container(
                          height: UIData.percentHeight(context, 75),
                          decoration: new BoxDecoration(
                            color: Colors.greenAccent,
                          ),
                          child: new Center(
                            child: Icon(
                              FontAwesomeIcons.check,
                              color: Colors.white,
                            ),
                          )),
                        new SizedBox(
                          height: UIData.percentHeight(context, 12),
                        ),
                        new Container(
                          padding: EdgeInsets.symmetric(vertical: UIData.percentHeight(context, 12), horizontal: UIData.percentWidth(context, 12)),
                          child: Column(
                            children: <Widget>[
                              new Center(
                                  child: new Text("Feedback submitted. Thank you.",
                                    style: TextStyle(
                                      fontSize: UIData.percentHeight(context, 20),
                                      fontFamily: "WorkSansSemiBold"
                                    ),
                                    textAlign: TextAlign.center,
                                  ),
                                ),
                              new SizedBox(
                                height: UIData.percentHeight(context, 20),
                              ),
                              new Padding(
                                padding: EdgeInsets.symmetric(vertical: UIData.percentHeight(context, 10), horizontal: UIData.percentWidth(context, 10)),
                              )
                                ],
                              ),
                        ),
                      ],
                  ); 
                }
              ); 
    }

  void _overflow(OverflowItem selected) {
    switch (selected) {
      // case OverflowItem.Settings:
      //   //print("Setings page");
      //   break;
      case OverflowItem.MyProfile:
        //print("I just pressed to view profile");
        Navigator.of(context).pushNamed('/my_profile');
        break;
      case OverflowItem.GiveFeedback:
        showDialog(
          context: context,
          builder: (BuildContext context){
            return SimpleDialog(
              semanticLabel: 'Compose Feedback',
              contentPadding: EdgeInsets.symmetric(vertical: UIData.percentHeight(context, 10), horizontal: UIData.percentWidth(context, 10)),
              children: <Widget>[
                  new Text(
                    'Compose Feedback',
                    style: TextStyle(
                      color: Colors.black,
                      fontFamily: "WorkSansSemiBold",
                      fontSize: UIData.percentHeight(context, 20)
                    ),
                  ),
                  SizedBox(
                    height: UIData.percentHeight(context, 5),
                  ),
                  new TextFormField(
                    controller: feedbackTextField,
                    keyboardType: TextInputType.text, // Use email input type for emails.
                    decoration: new InputDecoration(
                      hintText: 'e.g Thanks for this amazing App',
                    ),
                    style: TextStyle(
                      color: Colors.black,
                      fontFamily: "WorkSansMedium",
                      fontSize: UIData.percentHeight(context, 20)
                    ),
                  ),
                  SizedBox(
                    height: UIData.percentHeight(context, 10),
                  ),
                  new Text(
                    'We will respond via email to feedback and questions. You may also send feedback to: feedback@transervproserv.com',
                    style: TextStyle(
                      color: Colors.black,
                      fontFamily: "WorkSansMedium",
                      fontSize: UIData.percentHeight(context, 18)
                    ),
                  ),
                  SizedBox(
                    height: UIData.percentHeight(context, 10),
                  ),
                  new Row(
                    mainAxisAlignment: MainAxisAlignment.end,
                    children: <Widget>[
                      RaisedButton(
                        elevation: UIData.percentHeight(context, 10),
                        child: Text('Cancel',
                        style: TextStyle(
                          color: UIData.joltBlueAccent,
                          fontFamily: "WorkSansMedium",
                          fontSize: UIData.percentHeight(context, 20)
                        ),),
                        color: Colors.white,
                        onPressed: (){
                          //print('Cancel');
                          Navigator.pop(context);
                        },
                      ),
                      SizedBox(
                        width: UIData.percentWidth(context, 10),
                      ),
                      RaisedButton(
                        elevation: UIData.percentHeight(context, 10),
                        child: Text('Send',
                        style: TextStyle(
                          color: UIData.joltBlueAccent,
                          fontFamily: "WorkSansMedium",
                          fontSize: UIData.percentHeight(context, 20)
                        ),),
                        color: Colors.white,
                        onPressed: (){
                          try{
                            String t = new DateTime.now().millisecondsSinceEpoch.toString();
                            String s =feedbackTextField.text.toString();
                            Future.delayed(new Duration(seconds: 1), (){
                              FirebaseDatabase.instance.reference().child("feedbacks").child(_sessionManager.getSessionDetails()['user_id']).push().child(
                                '$t').set('$s');
                              feedbackTextField.clear();
                              Navigator.pop(context);
                              showSuccess(context); //may revisit later on!
                            });
                            
                          }
                          catch(e){
                            Navigator.pop(context);
                            Fluttertoast.showToast(
                                          msg: "Something went wrong!",
                                          toastLength: Toast.LENGTH_SHORT,
                                          gravity: ToastGravity.CENTER,
                                          timeInSecForIos: 1,
                                          backgroundColor: Colors.red,
                                          textColor: Colors.white,
                                      );
                          }
                        },
                      ),
                    ],
                  ),
                  //new Divider(
                    //color: Colors.black45,
                    //height: 30.0,
                  //),
                  RaisedButton(
                    elevation: UIData.percentHeight(context, 10),
                    child: Text('Write More',
                    style: TextStyle(
                      color: UIData.joltBlueAccent,
                      fontFamily: "WorkSansMedium",
                      fontSize: UIData.percentHeight(context, 20)
                    ),),
                    color: Colors.white,
                    onPressed: (){
                      Navigator.push(
                        context,
                        new MaterialPageRoute(
                          builder: (context) => new WriteMore(),
                        ), //MaterialPageRoute
                      );
                    },
                  ),
                ],
              
            );
          }
        );
        
        break;
      case OverflowItem.LogOut:
        showDialog(
          context: context,
          builder: (BuildContext context){
            return SimpleDialog(
              semanticLabel: 'Log out',
              contentPadding: EdgeInsets.symmetric(vertical: UIData.percentHeight(context, 10), horizontal: UIData.percentWidth(context, 10)),
              children: <Widget>[
                  new Text(
                    'Log out',
                    style: TextStyle(
                      color: Colors.redAccent,
                      fontFamily: "WorkSansSemiBold",
                      fontSize: UIData.percentHeight(context, 20)
                    ),
                  ),
                  SizedBox(
                    height: UIData.percentHeight(context, 15),
                  ),
                  new Text(
                    'Are you sure you want to log out? If you do, you will lose contact with your business contacts.',
                    style: TextStyle(
                      color: Colors.black,
                      fontFamily: "WorkSansMedium",
                      fontSize: UIData.percentHeight(context, 20)
                    ),
                  ),
                  SizedBox(
                    height: UIData.percentHeight(context, 10),
                  ),
                  new Row(
                    mainAxisAlignment: MainAxisAlignment.end,
                    children: <Widget>[
                      RaisedButton(
                        elevation: UIData.percentHeight(context, 10),
                        child: Text('No',
                        style: TextStyle(
                          color: Colors.black,
                          fontFamily: "WorkSansMedium",
                          fontSize: UIData.percentHeight(context, 20)
                        ),),
                        color: Colors.white,
                        onPressed: (){
                          //print('Cancel');
                          Navigator.pop(context);
                        },
                      ),
                      SizedBox(
                        width: UIData.percentWidth(context, 10),
                      ),
                      RaisedButton(
                        elevation: UIData.percentHeight(context, 10),
                        child: Text('Yes',
                        style: TextStyle(
                          color: Colors.redAccent,
                          fontFamily: "WorkSansMedium",
                          fontSize: UIData.percentHeight(context, 20)
                        ),),
                        color: Colors.white,
                        onPressed: (){
                          logout();
                          Navigator.pop(context);
                          Navigator.pushReplacement(
                                    context,
                                    new MaterialPageRoute(
                                      builder: (context) => new SecondRootPage(),
                                    ),
                                  );
                        },
                      ),
                    ],
                  ),
                ],
              
            );
          }
        );
        
        break;
    }
  }
}

enum OverflowItem {
    GiveFeedback,
    MyProfile,
    //Settings,
    LogOut
  }