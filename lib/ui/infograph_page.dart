import 'package:flutter/material.dart';
import 'package:flutter_radar_chart/flutter_radar_chart.dart';
import 'package:font_awesome_flutter/font_awesome_flutter.dart';
import 'package:jolt/utils/uidata.dart';
import 'package:jolt/views/common_scaffold.dart';
import 'package:jolt/utils/career_session_manager.dart';
import 'package:shared_preferences/shared_preferences.dart';

List<Personality> personalityList_ = [
  Personality("LR", "Low Reactive"),
  Personality("LE", "Leader"),
  Personality("HD", "High Decisive"),
  Personality("VI", "Visionary"),
  Personality("HR", "High Reactive"),
  Personality("ST", "Stable"),
  Personality("LD", "Low Decisive"),
  Personality("DI", "Diligent")
];

class Infography extends StatefulWidget{

  final String personality;
  Infography({this.personality});

  @override
  _InfographyState createState() => _InfographyState();
}

class _InfographyState extends State<Infography>{
  Size deviceSize;

  SessionManager _sessionManager;
  bool _showDialog = true;

  Future<Object> initialisePrefs() async {
    SharedPreferences prefs = await SharedPreferences.getInstance();
    SessionManager sessionManager = new SessionManager(prefs);
    return sessionManager;
  }

  @override
  void initState() {
    super.initState();
    initialisePrefs().then((sessionManager){
      if(sessionManager != null){
        setState(() {
            _sessionManager = sessionManager;
        });
      }
    });

    Future.delayed(new Duration(seconds: 2), (){
      setState(() {
        _showDialog = false;
      });
    });
  }

  Widget bodyData (BuildContext context){
    const ticks = [1, 2, 3, 4, 5, 6, 7, 8];
    var features = ["LR", "LE", "HD", "VI", "HR", "ST", "LD", "DI"];
    int personalityIndex = 0;
    List<int> infographData = [];
    for (int i = 0; i < features.length; i++){
      if (features.elementAt(i).toLowerCase().substring(0,2) == widget.personality.toLowerCase().substring(0,2)){
        personalityIndex = i;
        break;
      }
    }
    for (int i = 0; i < features.length; i++){
      if(i == personalityIndex){
        infographData.add(7);
      }
      else{
        infographData.add(5);
      }
    }
    var data = [
      infographData,
    ];
    print (data);

    return Container(
      child: Column(
        children: <Widget>[
          Padding(
            padding: EdgeInsets.symmetric(horizontal: UIData.percentWidth(context, 15), vertical: UIData.percentHeight(context, 15)),
            child: Center(
              child: Text("Your personality test reveals", style: TextStyle(fontFamily: "WorkSansSemiBold", fontSize: UIData.percentHeight(context, 25), color: Colors.grey.shade500),),
            ),
          ),
          Padding(
            padding: EdgeInsets.symmetric(horizontal: UIData.percentWidth(context, 15), vertical: UIData.percentHeight(context, 15)),
            child: Center(
              child: Text("${widget.personality.toUpperCase()}", style: TextStyle(fontFamily: "WorkSansBold", fontSize: UIData.percentHeight(context, 45), color: UIData.joltBlueAccent),),
            )
          ),
          new Container(
            padding: EdgeInsets.symmetric(vertical: UIData.percentHeight(context, 10), horizontal: UIData.percentWidth(context, 10)),
            margin: EdgeInsets.symmetric(vertical: UIData.percentHeight(context, 10), horizontal: UIData.percentWidth(context, 10)),
            height: UIData.percentHeight(context, 100),
            decoration: BoxDecoration(
              color: UIData.joltBlueAccent,
              borderRadius: BorderRadius.circular(5)
            ),
            child: new Row(
              mainAxisAlignment: MainAxisAlignment.spaceEvenly,
              children: <Widget>[
                new Column(
                  crossAxisAlignment: CrossAxisAlignment.start,
                  children: <Widget>[
                    PersonalityContainer(
                      eachPersonality: personalityList_[0],
                    ),
                    SizedBox(height: 10,),
                    PersonalityContainer(
                      eachPersonality: personalityList_[4],
                    ),
                  ],
                ), 

                new Column(
                  crossAxisAlignment: CrossAxisAlignment.start,
                  children: <Widget>[
                    PersonalityContainer(
                      eachPersonality: personalityList_[1],
                    ),
                    SizedBox(height: 10,),
                    PersonalityContainer(
                      eachPersonality: personalityList_[5],
                    ),
                  ],
                ),

                new Column(
                  crossAxisAlignment: CrossAxisAlignment.start,
                  children: <Widget>[
                    PersonalityContainer(
                      eachPersonality: personalityList_[2],
                    ),
                    SizedBox(height: 10,),
                    PersonalityContainer(
                      eachPersonality: personalityList_[6],
                    ),
                  ],
                ),

                new Column(
                  crossAxisAlignment: CrossAxisAlignment.start,
                  children: <Widget>[
                    PersonalityContainer(
                      eachPersonality: personalityList_[3],
                    ),
                    SizedBox(height: 10,),
                    PersonalityContainer(
                      eachPersonality: personalityList_[7],
                    ),
                  ],
                ),
              ],
            ),
          ),
          new RaisedButton(
            shape: new RoundedRectangleBorder(borderRadius: new BorderRadius.circular(5.0)),
            child: Text(
              "View More",
              style: TextStyle(color: Colors.white, fontFamily: "WorkSansSemiBold", fontSize: UIData.percentHeight(context, 18)),
            ),
            color: UIData.joltBlueAccent,
            elevation: UIData.percentHeight(context, 8),
            splashColor: UIData.joltBlueAccent,
            onPressed: (){
              if(widget.personality.toLowerCase() == "leader"){
                showLeaderDetails();
              }
              else if(widget.personality.toLowerCase() == "diligent"){
                showDiligentDetails();
              }
              else if(widget.personality.toLowerCase() == "stable"){
                showStableDetails();
              }
              else if(widget.personality.toLowerCase() == "visionary"){
                showVisionaryDetails();
              }
            },
          ),
          Expanded(
            child: Padding(
              padding: EdgeInsets.symmetric(horizontal: UIData.percentWidth(context, 16), vertical: UIData.percentHeight(context, 16)),
              child: Theme(
                data: Theme.of(context).copyWith(
                  accentColor: UIData.joltBlueAccent,
                  primaryColor: UIData.joltBlue,
                ),
                child: new Builder(
                  builder: (context) => new RadarChart(
                    ticks: ticks,
                    features: features,
                    data: data,
                    reverseAxis: false,
                    axisColor: Colors.grey.shade300,
                    ticksTextStyle: TextStyle(color: Colors.grey.shade100, fontSize: UIData.percentHeight(context, 14), fontFamily: "WorkSansMedium"),
                    featuresTextStyle: TextStyle(color: Colors.black, fontSize: UIData.percentHeight(context, 20), fontFamily: "WorkSansBold"),
                    graphColors: [UIData.joltBlueAccent],
                  )
                )
              )
            ),
          )
        ],   
      ),
    );
    
  }

  Future<Null> showLeaderDetails(){
    return new Future.delayed(new Duration(seconds: 0), (){
      return showDialog(
          barrierDismissible: true,
          context: context,
          builder: (BuildContext context){
            return SimpleDialog(
              backgroundColor: Colors.white,
              contentPadding: EdgeInsets.symmetric(vertical: UIData.percentHeight(context, 10), horizontal: UIData.percentWidth(context, 10)),
              children: <Widget>[
                  Center(
                    child: Icon(
                      FontAwesomeIcons.certificate,
                      color: UIData.joltBlueAccent,
                      size: UIData.percentHeight(context, 60)
                    ),
                  ),
                  SizedBox(
                    height: UIData.percentHeight(context, 5),
                  ),
                  Center(
                    child: new Text(
                      "LEADER",
                      style: TextStyle(
                        color: UIData.joltBlueAccent,
                        fontFamily: "WorkSansBold",
                        fontSize: UIData.percentHeight(context, 30)
                      ),
                    ),
                  ),
                  SizedBox(
                    height: UIData.percentHeight(context, 15),
                  ),
                  Card(
                    elevation: UIData.percentHeight(context, 10),
                    child: Padding(
                      padding: EdgeInsets.symmetric(horizontal: UIData.percentWidth(context, 15), vertical: UIData.percentHeight(context, 15)),
                      child: Column(
                        crossAxisAlignment: CrossAxisAlignment.start,
                        children: <Widget>[
                          new Text(
                            "Personality",
                            style: TextStyle(
                              color: Colors.black,
                              fontFamily: "WorkSansBold",
                              fontSize: UIData.percentHeight(context, 18)
                            ),
                          ),
                          SizedBox(
                            height: UIData.percentHeight(context, 20),
                          ),
                          new Text(
                            'People with your rating are determined, competitive and like to be in control',
                            style: TextStyle(
                              color: Colors.black,
                              fontFamily: "WorkSansMedium",
                              fontSize: UIData.percentHeight(context, 18)
                            ),
                          ),
                        ],
                      ),
                    )
                  ),
                  SizedBox(
                    height: UIData.percentHeight(context, 15),
                  ),
                  Card(
                    elevation: UIData.percentHeight(context, 10),
                    child: Padding(
                      padding: EdgeInsets.symmetric(horizontal: UIData.percentWidth(context, 15), vertical: UIData.percentHeight(context, 15)),
                      child: Column(
                        crossAxisAlignment: CrossAxisAlignment.start,
                        children: <Widget>[
                          new Text(
                            "Social",
                            style: TextStyle(
                              color: Colors.black,
                              fontFamily: "WorkSansBold",
                              fontSize: UIData.percentHeight(context, 18)
                            ),
                          ),
                          SizedBox(
                            height: UIData.percentHeight(context, 20),
                          ),
                          new Text(
                            'You fear being seen as vulnerable and being taken advantage of',
                            style: TextStyle(
                              color: Colors.black,
                              fontFamily: "WorkSansMedium",
                              fontSize: UIData.percentHeight(context, 18)
                            ),
                          ),
                        ],
                      ),
                    )
                  ),
                  SizedBox(
                    height: UIData.percentHeight(context, 15),
                  ),
                  Card(
                    elevation: UIData.percentHeight(context, 10),
                    child: Padding(
                      padding: EdgeInsets.symmetric(horizontal: UIData.percentWidth(context, 15), vertical: UIData.percentHeight(context, 15)),
                      child: Column(
                        crossAxisAlignment: CrossAxisAlignment.start,
                        children: <Widget>[
                          new Text(
                            "Work Environment",
                            style: TextStyle(
                              color: Colors.black,
                              fontFamily: "WorkSansBold",
                              fontSize: UIData.percentHeight(context, 18)
                            ),
                          ),
                          SizedBox(
                            height: UIData.percentHeight(context, 20),
                          ),
                          new Text(
                            'You may appear busy and your desks will usually be covered with paperwork, projects and materials separated into piles',
                            style: TextStyle(
                              color: Colors.black,
                              fontFamily: "WorkSansMedium",
                              fontSize: UIData.percentHeight(context, 18)
                            ),
                          ),
                        ],
                      ),
                    )
                  ),
                  SizedBox(
                    height: UIData.percentHeight(context, 15),
                  ),
                  RaisedButton(
                    elevation: UIData.percentHeight(context, 20),
                    child: Text('Okay, Thanks',
                    style: TextStyle(
                      color: UIData.joltBlueAccent,
                      fontFamily: "WorkSansMedium",
                      fontSize: UIData.percentHeight(context, 18)
                    ),),
                    color: Colors.white,
                    onPressed: (){
                      Navigator.of(context).pop();
                    },
                  )
                ],
            );
          }
        );
    });
  }

  Future<Null> showDiligentDetails(){
    return new Future.delayed(new Duration(seconds: 0), (){
      return showDialog(
          barrierDismissible: true,
          context: context,
          builder: (BuildContext context){
            return SimpleDialog(
              backgroundColor: Colors.white,
              contentPadding: EdgeInsets.symmetric(vertical: UIData.percentHeight(context, 10), horizontal: UIData.percentWidth(context, 10)),
              children: <Widget>[
                  Center(
                    child: Icon(
                      FontAwesomeIcons.certificate,
                      color: UIData.joltBlueAccent,
                      size: UIData.percentHeight(context, 60)
                    ),
                  ),
                  SizedBox(
                    height: UIData.percentHeight(context, 5),
                  ),
                  Center(
                    child: new Text(
                      "DILIGENT",
                      style: TextStyle(
                        color: UIData.joltBlueAccent,
                        fontFamily: "WorkSansBold",
                        fontSize: UIData.percentHeight(context, 30)
                      ),
                    ),
                  ),
                  SizedBox(
                    height: UIData.percentHeight(context, 15),
                  ),
                  Card(
                    elevation: UIData.percentHeight(context, 10),
                    child: Padding(
                      padding: EdgeInsets.symmetric(horizontal: UIData.percentWidth(context, 15), vertical: UIData.percentHeight(context, 15)),
                      child: Column(
                        crossAxisAlignment: CrossAxisAlignment.start,
                        children: <Widget>[
                          new Text(
                            "Personality",
                            style: TextStyle(
                              color: Colors.black,
                              fontFamily: "WorkSansBold",
                              fontSize: UIData.percentHeight(context, 18)
                            ),
                          ),
                          SizedBox(
                            height: UIData.percentHeight(context, 20),
                          ),
                          new Text(
                            'People with your rating prefer task-oriented intellectual work and often like to work alone',
                            style: TextStyle(
                              color: Colors.black,
                              fontFamily: "WorkSansMedium",
                              fontSize: UIData.percentHeight(context, 18)
                            ),
                          ),
                        ],
                      ),
                    )
                  ),
                  SizedBox(
                    height: UIData.percentHeight(context, 15),
                  ),
                  Card(
                    elevation: UIData.percentHeight(context, 10),
                    child: Padding(
                      padding: EdgeInsets.symmetric(horizontal: UIData.percentWidth(context, 15), vertical: UIData.percentHeight(context, 15)),
                      child: Column(
                        crossAxisAlignment: CrossAxisAlignment.start,
                        children: <Widget>[
                          new Text(
                            "Social",
                            style: TextStyle(
                              color: Colors.black,
                              fontFamily: "WorkSansBold",
                              fontSize: UIData.percentHeight(context, 18)
                            ),
                          ),
                          SizedBox(
                            height: UIData.percentHeight(context, 20),
                          ),
                          new Text(
                            'You fear criticism of your work efforts',
                            style: TextStyle(
                              color: Colors.black,
                              fontFamily: "WorkSansMedium",
                              fontSize: UIData.percentHeight(context, 18)
                            ),
                          ),
                        ],
                      ),
                    )
                  ),
                  SizedBox(
                    height: UIData.percentHeight(context, 15),
                  ),
                  Card(
                    elevation: UIData.percentHeight(context, 10),
                    child: Padding(
                      padding: EdgeInsets.symmetric(horizontal: UIData.percentWidth(context, 15), vertical: UIData.percentHeight(context, 15)),
                      child: Column(
                        crossAxisAlignment: CrossAxisAlignment.start,
                        children: <Widget>[
                          new Text(
                            "Work Environment",
                            style: TextStyle(
                              color: Colors.black,
                              fontFamily: "WorkSansBold",
                              fontSize: UIData.percentHeight(context, 18)
                            ),
                          ),
                          SizedBox(
                            height: UIData.percentHeight(context, 20),
                          ),
                          new Text(
                            'Your office will be organized and functional',
                            style: TextStyle(
                              color: Colors.black,
                              fontFamily: "WorkSansMedium",
                              fontSize: UIData.percentHeight(context, 18)
                            ),
                          ),
                        ],
                      ),
                    )
                  ),
                  SizedBox(
                    height: UIData.percentHeight(context, 15),
                  ),
                  RaisedButton(
                    elevation: UIData.percentHeight(context, 20),
                    child: Text('Okay, Thanks',
                    style: TextStyle(
                      color: UIData.joltBlueAccent,
                      fontFamily: "WorkSansMedium",
                      fontSize: UIData.percentHeight(context, 18)
                    ),),
                    color: Colors.white,
                    onPressed: (){
                      Navigator.of(context).pop();
                    },
                  )
                ],
            );
          }
        );
    });
  }

  Future<Null> showStableDetails(){
    return new Future.delayed(new Duration(seconds: 0), (){
      return showDialog(
          barrierDismissible: true,
          context: context,
          builder: (BuildContext context){
            return SimpleDialog(
              backgroundColor: Colors.white,
              contentPadding: EdgeInsets.symmetric(vertical: UIData.percentHeight(context, 10), horizontal: UIData.percentWidth(context, 10)),
              children: <Widget>[
                  Center(
                    child: Icon(
                      FontAwesomeIcons.certificate,
                      color: UIData.joltBlueAccent,
                      size: UIData.percentHeight(context, 60)
                    ),
                  ),
                  SizedBox(
                    height: UIData.percentHeight(context, 5),
                  ),
                  Center(
                    child: new Text(
                      "STABLE",
                      style: TextStyle(
                        color: UIData.joltBlueAccent,
                        fontFamily: "WorkSansBold",
                        fontSize: UIData.percentHeight(context, 30)
                      ),
                    ),
                  ),
                  SizedBox(
                    height: UIData.percentHeight(context, 15),
                  ),
                  Card(
                    elevation: UIData.percentHeight(context, 10),
                    child: Padding(
                      padding: EdgeInsets.symmetric(horizontal: UIData.percentWidth(context, 15), vertical: UIData.percentHeight(context, 15)),
                      child: Column(
                        crossAxisAlignment: CrossAxisAlignment.start,
                        children: <Widget>[
                          new Text(
                            "Personality",
                            style: TextStyle(
                              color: Colors.black,
                              fontFamily: "WorkSansBold",
                              fontSize: UIData.percentHeight(context, 18)
                            ),
                          ),
                          SizedBox(
                            height: UIData.percentHeight(context, 20),
                          ),
                          new Text(
                            'People with your rating are good listeners',
                            style: TextStyle(
                              color: Colors.black,
                              fontFamily: "WorkSansMedium",
                              fontSize: UIData.percentHeight(context, 18)
                            ),
                          ),
                        ],
                      ),
                    )
                  ),
                  SizedBox(
                    height: UIData.percentHeight(context, 15),
                  ),
                  Card(
                    elevation: UIData.percentHeight(context, 10),
                    child: Padding(
                      padding: EdgeInsets.symmetric(horizontal: UIData.percentWidth(context, 15), vertical: UIData.percentHeight(context, 15)),
                      child: Column(
                        crossAxisAlignment: CrossAxisAlignment.start,
                        children: <Widget>[
                          new Text(
                            "Social",
                            style: TextStyle(
                              color: Colors.black,
                              fontFamily: "WorkSansBold",
                              fontSize: UIData.percentHeight(context, 18)
                            ),
                          ),
                          SizedBox(
                            height: UIData.percentHeight(context, 20),
                          ),
                          new Text(
                            'You fear sudden changes and instability',
                            style: TextStyle(
                              color: Colors.black,
                              fontFamily: "WorkSansMedium",
                              fontSize: UIData.percentHeight(context, 18)
                            ),
                          ),
                        ],
                      ),
                    )
                  ),
                  SizedBox(
                    height: UIData.percentHeight(context, 15),
                  ),
                  Card(
                    elevation: UIData.percentHeight(context, 10),
                    child: Padding(
                      padding: EdgeInsets.symmetric(horizontal: UIData.percentWidth(context, 15), vertical: UIData.percentHeight(context, 15)),
                      child: Column(
                        crossAxisAlignment: CrossAxisAlignment.start,
                        children: <Widget>[
                          new Text(
                            "Work Environment",
                            style: TextStyle(
                              color: Colors.black,
                              fontFamily: "WorkSansBold",
                              fontSize: UIData.percentHeight(context, 18)
                            ),
                          ),
                          SizedBox(
                            height: UIData.percentHeight(context, 20),
                          ),
                          new Text(
                            'Your office will be warm and friendly',
                            style: TextStyle(
                              color: Colors.black,
                              fontFamily: "WorkSansMedium",
                              fontSize: UIData.percentHeight(context, 18)
                            ),
                          ),
                        ],
                      ),
                    )
                  ),
                  SizedBox(
                    height: UIData.percentHeight(context, 15),
                  ),
                  RaisedButton(
                    elevation: UIData.percentHeight(context, 20),
                    child: Text('Okay, Thanks',
                    style: TextStyle(
                      color: UIData.joltBlueAccent,
                      fontFamily: "WorkSansMedium",
                      fontSize: UIData.percentHeight(context, 18)
                    ),),
                    color: Colors.white,
                    onPressed: (){
                      Navigator.of(context).pop();
                    },
                  )
                ],
            );
          }
        );
    });
  }

  Future<Null> showVisionaryDetails(){
    return new Future.delayed(new Duration(seconds: 0), (){
      return showDialog(
          barrierDismissible: true,
          context: context,
          builder: (BuildContext context){
            return SimpleDialog(
              backgroundColor: Colors.white,
              contentPadding: EdgeInsets.symmetric(vertical: UIData.percentHeight(context, 10), horizontal: UIData.percentWidth(context, 10)),
              children: <Widget>[
                  Center(
                    child: Icon(
                      FontAwesomeIcons.certificate,
                      color: UIData.joltBlueAccent,
                      size: UIData.percentHeight(context, 60)
                    ),
                  ),
                  SizedBox(
                    height: UIData.percentHeight(context, 5),
                  ),
                  Center(
                    child: new Text(
                      "VISIONARY",
                      style: TextStyle(
                        color: UIData.joltBlueAccent,
                        fontFamily: "WorkSansBold",
                        fontSize: UIData.percentHeight(context, 30)
                      ),
                    ),
                  ),
                  SizedBox(
                    height: UIData.percentHeight(context, 15),
                  ),
                  Card(
                    elevation: UIData.percentHeight(context, 10),
                    child: Padding(
                      padding: EdgeInsets.symmetric(horizontal: UIData.percentWidth(context, 15), vertical: UIData.percentHeight(context, 15)),
                      child: Column(
                        crossAxisAlignment: CrossAxisAlignment.start,
                        children: <Widget>[
                          new Text(
                            "Personality",
                            style: TextStyle(
                              color: Colors.black,
                              fontFamily: "WorkSansBold",
                              fontSize: UIData.percentHeight(context, 18)
                            ),
                          ),
                          SizedBox(
                            height: UIData.percentHeight(context, 20),
                          ),
                          new Text(
                            'People with your rating are idea people. They have strong persuasive skills and able to convince to share their pursuits and mobilize others to come together to accomplish the task at hand',
                            style: TextStyle(
                              color: Colors.black,
                              fontFamily: "WorkSansMedium",
                              fontSize: UIData.percentHeight(context, 18)
                            ),
                          ),
                        ],
                      ),
                    )
                  ),
                  SizedBox(
                    height: UIData.percentHeight(context, 15),
                  ),
                  Card(
                    elevation: UIData.percentHeight(context, 10),
                    child: Padding(
                      padding: EdgeInsets.symmetric(horizontal: UIData.percentWidth(context, 15), vertical: UIData.percentHeight(context, 15)),
                      child: Column(
                        crossAxisAlignment: CrossAxisAlignment.start,
                        children: <Widget>[
                          new Text(
                            "Social",
                            style: TextStyle(
                              color: Colors.black,
                              fontFamily: "WorkSansBold",
                              fontSize: UIData.percentHeight(context, 18)
                            ),
                          ),
                          SizedBox(
                            height: UIData.percentHeight(context, 20),
                          ),
                          new Text(
                            'You fear losing social recognition',
                            style: TextStyle(
                              color: Colors.black,
                              fontFamily: "WorkSansMedium",
                              fontSize: UIData.percentHeight(context, 18)
                            ),
                          ),
                        ],
                      ),
                    )
                  ),
                  SizedBox(
                    height: UIData.percentHeight(context, 15),
                  ),
                  Card(
                    elevation: UIData.percentHeight(context, 10),
                    child: Padding(
                      padding: EdgeInsets.symmetric(horizontal: UIData.percentWidth(context, 15), vertical: UIData.percentHeight(context, 15)),
                      child: Column(
                        crossAxisAlignment: CrossAxisAlignment.start,
                        children: <Widget>[
                          new Text(
                            "Work Environment",
                            style: TextStyle(
                              color: Colors.black,
                              fontFamily: "WorkSansBold",
                              fontSize: UIData.percentHeight(context, 18)
                            ),
                          ),
                          SizedBox(
                            height: UIData.percentHeight(context, 20),
                          ),
                          new Text(
                            'Your office may appear disorganized and cluttered. You however know when/if something is missing or out of place',
                            style: TextStyle(
                              color: Colors.black,
                              fontFamily: "WorkSansMedium",
                              fontSize: UIData.percentHeight(context, 18)
                            ),
                          ),
                        ],
                      ),
                    )
                  ),
                  SizedBox(
                    height: UIData.percentHeight(context, 15),
                  ),
                  RaisedButton(
                    elevation: UIData.percentHeight(context, 20),
                    child: Text('Okay, Thanks',
                    style: TextStyle(
                      color: UIData.joltBlueAccent,
                      fontFamily: "WorkSansMedium",
                      fontSize: UIData.percentHeight(context, 18)
                    ),),
                    color: Colors.white,
                    onPressed: (){
                      Navigator.of(context).pop();
                    },
                  )
                ],
            );
          }
        );
    });
  }

  @override
  Widget build(BuildContext context){
    deviceSize = MediaQuery.of(context).size;

    return CommonScaffold(
        appTitle: "Infography",
        backGroundColor: Colors.white,
        actionFirstIcon: null,
        bColor: UIData.joltBlueAccent,
        bodyData: _showDialog ? new Center(
          child: CircularProgressIndicator(),
        ) : bodyData(context),
      );

    // if(_sessionManager.getSessionDetails() != null){
    //   return CommonScaffold(
    //     appTitle: "Infography",
    //     backGroundColor: Colors.white,
    //     actionFirstIcon: null,
    //     bColor: UIData.joltBlueAccent,
    //     bodyData: _showDialog ? new Center(
    //       child: CircularProgressIndicator(),
    //     ) : bodyData(context),
    //   );
    // }
    // else{
    //   return Container(
    //       child: Padding(
    //           padding: EdgeInsets.symmetric(vertical: UIData.percentHeight(context, 5), horizontal: UIData.percentWidth(context, 5)),
    //           child: Center(
    //             child: CircularProgressIndicator()
    //           ),
    //         )
    //       );
    // }
    
  }

}

class Personality{
  String abbv;
  String name;

  Personality(
    this.abbv,
    this.name
  );

}

class PersonalityContainer extends StatelessWidget{
  final Personality eachPersonality;

  PersonalityContainer({
    Key key,
    this.eachPersonality,
  }) : super(key: key);
  
  @override
  Widget build(BuildContext context) {
    return new Container(
      child: Row(
        mainAxisAlignment: MainAxisAlignment.start,
        children: <Widget>[
          new Container(
            margin: EdgeInsets.only(left: UIData.percentWidth(context, 10), right: UIData.percentWidth(context, 10)),
            child: new Column(
              mainAxisAlignment: MainAxisAlignment.start,
              crossAxisAlignment: CrossAxisAlignment.start,
              children: <Widget>[
                Text(
                  this.eachPersonality.abbv,
                  style: TextStyle(
                    fontSize: UIData.percentHeight(context, 12),
                    fontFamily: "WorkSansMedium",
                    color: Colors.white
                  ),
                ),
                Text(
                  "${eachPersonality.name.toUpperCase()}",
                  style: TextStyle(
                    fontSize: UIData.percentHeight(context, 12),
                    fontFamily: "WorkSansBold",
                    color: Colors.white
                  )
                )
              ],
            ),
          ),
        ],
      ),
    );
  }
  
}