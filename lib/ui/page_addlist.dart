import 'dart:async';
import 'package:flutter/services.dart';

import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:firebase_auth/firebase_auth.dart';
import 'package:flutter/material.dart';
import 'package:flutter_colorpicker/flutter_colorpicker.dart';
import 'package:jolt/utils/uidata.dart';
import 'package:modal_progress_hud/modal_progress_hud.dart';

import 'package:connectivity/connectivity.dart';

class NewTaskPage extends StatefulWidget {
  final FirebaseUser user;

  NewTaskPage({Key key, this.user}) : super(key: key);

  @override
  State<StatefulWidget> createState() => _NewTaskPageState();
}

class _NewTaskPageState extends State<NewTaskPage> {
  TextEditingController listNameController = new TextEditingController();
  final GlobalKey<ScaffoldState> _scaffoldKey = new GlobalKey<ScaffoldState>();

  Color pickerColor = Color(0xff6633ff);
  Color currentColor = Color(0xff6633ff);

  ValueChanged<Color> onColorChanged;

  bool _saving = false;

  String _connectionStatus = 'Unknown';
  final Connectivity _connectivity = new Connectivity();
  StreamSubscription<ConnectivityResult> _connectivitySubscription;

  Future<Null> initConnectivity() async {
    String connectionStatus;
    // Platform messages may fail, so we use a try/catch PlatformException.
    try {
      connectionStatus = (await _connectivity.checkConnectivity()).toString();
    } on PlatformException catch (e) {
      //print(e.toString());
      connectionStatus = 'Failed to get connectivity.';
    }

    // If the widget was removed from the tree while the asynchronous platform
    // message was in flight, we want to discard the reply rather than calling
    // setState to update our non-existent appearance.
    if (!mounted) {
      return;
    }

    setState(() {
      _connectionStatus = connectionStatus;
    });
  }

  void addToFirebase() async {
    setState(() {
      _saving = true;
    });

    //print(_connectionStatus);

    if(_connectionStatus == "ConnectivityResult.none"){
      showInSnackBar("No internet connection currently available");
      setState(() {
        _saving = false;
      });
    } else {

      bool isExist = false;

      QuerySnapshot query =
      await Firestore.instance.collection(widget.user.uid).getDocuments();

      query.documents.forEach((doc) {
        if (listNameController.text.toString() == doc.documentID) {
          isExist = true;
        }
      });

      if (isExist == false && listNameController.text.isNotEmpty) {
        await Firestore.instance
            .collection(widget.user.uid)
            .document(listNameController.text.toString().trim())
            .setData({
          "color": currentColor.value.toString(),
          "date": DateTime.now().millisecondsSinceEpoch
        });

        listNameController.clear();

        pickerColor = Color(0xff6633ff);
        currentColor = Color(0xff6633ff);

        Navigator.of(context).pop();
      }
      if (isExist == true) {
        showInSnackBar("This list already exists");
        setState(() {
          _saving = false;
        });
      }
      if (listNameController.text.isEmpty) {
        showInSnackBar("Please enter a name");
        setState(() {
          _saving = false;
        });
      }
    }
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      key: _scaffoldKey,
      body: ModalProgressHUD(
          child: SingleChildScrollView(
            child: new Column(
              children: <Widget>[
                Padding(
                      padding: EdgeInsets.only(top: UIData.percentHeight(context, 50)),
                      child: Row(
                        mainAxisAlignment: MainAxisAlignment.spaceBetween,
                        crossAxisAlignment: CrossAxisAlignment.center,
                        children: <Widget>[
                          Expanded(
                            flex: 1,
                            child: Container(
                              color: UIData.joltBlueAccent,
                              height: UIData.percentHeight(context, 2),
                            ),
                          ),
                          Expanded(
                              flex: 2,
                              child: new Row(
                                mainAxisAlignment: MainAxisAlignment.center,
                                children: <Widget>[
                                  Text(
                                    'New ',
                                    style: new TextStyle(
                                        fontSize: UIData.percentHeight(context, 30),
                                        fontWeight: FontWeight.bold,
                                        fontFamily: "WorkSansBold",
                                        color: UIData.joltBlueAccent),
                                  ),
                                  Text(
                                    'List',
                                    style: new TextStyle(
                                        fontSize: UIData.percentHeight(context, 28), color: UIData.joltBlueAccent,
                                        fontFamily: "WorkSansBold",),
                                  )
                                ],
                              )),
                          Expanded(
                            flex: 1,
                            child: Container(
                              color: UIData.joltBlueAccent,
                              height: UIData.percentHeight(context, 2),
                            ),
                          ),
                        ],
                      ),
                    ),
                    Padding(
                      padding:
                          EdgeInsets.only(top: UIData.percentHeight(context, 50), left: UIData.percentWidth(context, 20), right: UIData.percentWidth(context, 20)),
                      child: new Column(
                        children: <Widget>[
                          new TextFormField(
                            decoration: InputDecoration(
                                border: new OutlineInputBorder(
                                    borderSide:
                                        new BorderSide(color: Colors.teal)),
                                labelText: "List name",
                                contentPadding: EdgeInsets.only(
                                    left: UIData.percentWidth(context, 32),
                                    top: UIData.percentHeight(context, 40),
                                    right: UIData.percentWidth(context, 32),
                                    bottom: UIData.percentHeight(context, 10))),
                            controller: listNameController,
                            autofocus: true,
                            style: TextStyle(
                              fontSize: UIData.percentHeight(context, 22),
                              color: Colors.black,
                              fontWeight: FontWeight.w500,
                            ),
                            keyboardType: TextInputType.text,
                            textCapitalization: TextCapitalization.sentences,
                            maxLength: 20,
                          ),
                          new Padding(
                            padding: EdgeInsets.only(bottom: UIData.percentHeight(context, 10)),
                          ),
                          ButtonTheme(
                            minWidth: double.infinity,
                            child: RaisedButton(
                              shape: new RoundedRectangleBorder(borderRadius: new BorderRadius.circular(50.0)),
                              elevation: UIData.percentHeight(context, 3),
                              onPressed: () {
                                pickerColor = currentColor;
                                showDialog(
                                  context: context,
                                  builder: (BuildContext context) {
                                    return AlertDialog(
                                      title: const Text('Pick a color!'),
                                      content: SingleChildScrollView(
                                        child: ColorPicker(
                                          pickerColor: pickerColor,
                                          onColorChanged: changeColor,
                                          enableLabel: false,
                                          colorPickerWidth: UIData.percentWidth(context, 300),
                                          pickerAreaHeightPercent: 1.0, 
                                        ),
                                      ),
                                      actions: <Widget>[
                                        FlatButton(
                                          child: Text('Choose'),
                                          onPressed: () {
                                            setState(() =>
                                                currentColor = pickerColor);
                                            Navigator.of(context).pop();
                                          },
                                        ),
                                      ],
                                    );
                                  },
                                );
                              },
                              child: Text('Choose Color', style:new TextStyle(fontFamily: "WorkSansSemiBold", fontSize: UIData.percentHeight(context, 20))),
                              color: currentColor,
                              textColor: const Color(0xffffffff),
                              padding: EdgeInsets.symmetric(vertical: UIData.percentHeight(context, 12), horizontal: UIData.percentWidth(context, 12)),
                            ),
                          ),
                        ],
                      ),
                    ),
                    Padding(
                      padding: EdgeInsets.only(top: UIData.percentHeight(context, 20)),
                      child: new Column(
                        children: <Widget>[
                          new RaisedButton(
                            shape: new RoundedRectangleBorder(borderRadius: new BorderRadius.circular(30.0)),
                            child: Text(
                              '    Add List    ',
                              style: TextStyle(color: Colors.white, fontFamily: "WorkSansSemiBold", fontSize: UIData.percentHeight(context, 20)),
                            ),
                            color: UIData.joltBlue,
                            elevation: UIData.percentHeight(context, 8),
                            splashColor: Colors.deepPurple,
                            onPressed: addToFirebase,
                          ),
                        ],
                      ),
                    ),
                  ],
            ),
          ),
          inAsyncCall: _saving),
    );
  }

  changeColor(Color color) {
    setState(() => pickerColor = color);
  }

  @override
  void dispose() {
    _scaffoldKey.currentState?.dispose();
    _connectivitySubscription?.cancel();
    super.dispose();
  }

  @override
  void initState() {
    super.initState();
    initConnectivity();
    _connectivitySubscription =
        _connectivity.onConnectivityChanged.listen((ConnectivityResult result) {
          setState(() {
            _connectionStatus = result.toString();
          });
        });
  }

  void showInSnackBar(String value) {
    _scaffoldKey.currentState?.removeCurrentSnackBar();

    _scaffoldKey.currentState?.showSnackBar(new SnackBar(
      content: new Text(value, textAlign: TextAlign.center),
      backgroundColor: currentColor,
      duration: Duration(seconds: 3),
    ));
  }

  // Container _getToolbar(BuildContext context) {
  //   return new Container(
  //     margin: new EdgeInsets.only(left: 10.0, top: 40.0),
  //     child: new BackButton(color: Colors.black),
  //   );
  // }
}
