import 'dart:async';

import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:firebase_auth/firebase_auth.dart';
import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:font_awesome_flutter/font_awesome_flutter.dart';
import 'package:jolt/utils/uidata.dart';
import 'package:jolt/models/element.dart';
import 'page_detail.dart';

import 'page_addlist.dart';
import 'package:jolt/utils/career_session_manager.dart';
import 'package:shared_preferences/shared_preferences.dart';
import 'view_photo.dart';

class TaskPage extends StatefulWidget {
  final FirebaseUser user;
  final SessionManager sessionManager;

  TaskPage({Key key, this.user, this.sessionManager}) : super(key: key);

  @override
  State<StatefulWidget> createState() => _TaskPageState();
}

class _TaskPageState extends State<TaskPage>
    with SingleTickerProviderStateMixin {
  Size deviceSize;
  int index = 1;
  List<bool> notCompletedList = new List<bool>();
  List<bool> completedList = new List<bool>();
  int todoPercent = 0;
  int x = 0;

  // void _onAfterBuild(BuildContext context){
  //   // I can now safely get the dimensions based on the context
  //   setState(() {
      
  //   });
  // }

  @override
  Widget build(BuildContext context) {
    //WidgetsBinding.instance.addPostFrameCallback((_) => _onAfterBuild(context));
    deviceSize = MediaQuery.of(context).size;
    return Scaffold(
      body: ListView(
        children: <Widget>[
          new Column(
            children: <Widget>[
              profileHeader(context),
              new SizedBox(
                height: 10.0,
              ),
              new RaisedButton(
                shape: new RoundedRectangleBorder(borderRadius: new BorderRadius.circular(7.0)),
                child: Text(
                  '    Add Todo    ',
                  style: TextStyle(color: Colors.white, fontFamily: "WorkSansSemiBold", fontSize: UIData.percentHeight(context, 20)),
                ),
                color: UIData.joltBlue,
                elevation: UIData.percentHeight(context, 8),
                splashColor: UIData.joltBlueAccent,
                onPressed: _addTaskPressed,
              ),
              // Padding(
              //   padding: EdgeInsets.only(top: 50.0),
              //   child: Row(
              //     mainAxisAlignment: MainAxisAlignment.spaceBetween,
              //     crossAxisAlignment: CrossAxisAlignment.center,
              //     children: <Widget>[
              //       Expanded(
              //         flex: 1,
              //         child: Container(
              //           color: Colors.grey,
              //           height: 1.5,
              //         ),
              //       ),
              //       Expanded(
              //           flex: 2,
              //           child: new Row(
              //             mainAxisAlignment: MainAxisAlignment.center,
              //             children: <Widget>[
              //               Text(
              //                 'Task',
              //                 style: new TextStyle(
              //                     fontSize: 30.0, fontWeight: FontWeight.bold),
              //               ),
              //               Text(
              //                 'Lists',
              //                 style: new TextStyle(
              //                     fontSize: 28.0, color: Colors.grey),
              //               )
              //             ],
              //           )),
              //       Expanded(
              //         flex: 1,
              //         child: Container(
              //           color: Colors.grey,
              //           height: 1.5,
              //         ),
              //       ),
              //     ],
              //   ),
              // ),
              // Padding(
              //   padding: EdgeInsets.only(top: 0.0),
              //   child: new Column(
              //     children: <Widget>[
              //       new Container(
              //         width: 50.0,
              //         height: 50.0,
              //         decoration: new BoxDecoration(
              //             border: new Border.all(color: Colors.black38),
              //             borderRadius: BorderRadius.all(Radius.circular(7.0))),
              //         child: new IconButton(
              //           icon: new Icon(Icons.add),
              //           onPressed: _addTaskPressed,
              //           iconSize: 30.0,
              //         ),
              //       ),
              //       Padding(
              //         padding: EdgeInsets.only(top: 10.0),
              //         child: Text('Add List',
              //             style: TextStyle(color: Colors.black45)),
              //       ),
              //     ],
              //   ),
              // ),
            ],
          ),
          Padding(
            padding: EdgeInsets.only(top: UIData.percentHeight(context, 25)),
            child: Container(
              height: UIData.percentHeight(context, 500),
              padding: EdgeInsets.only(bottom: UIData.percentHeight(context, 25)),
              child: NotificationListener<OverscrollIndicatorNotification>(
                onNotification: (overscroll) {
                  overscroll.disallowGlow();
                },
                child: new StreamBuilder<QuerySnapshot>(
                    stream: Firestore.instance
                        .collection(widget.user.uid)
                        .orderBy("date", descending: true)
                        .snapshots(),
                    builder: (BuildContext context,
                        AsyncSnapshot<QuerySnapshot> snapshot) {
                      if (!snapshot.hasData)
                        return new Center(
                            child: CircularProgressIndicator(
                          backgroundColor: UIData.joltBlue,
                        ));
                      return new ListView(
                        physics: const BouncingScrollPhysics(),
                        padding: EdgeInsets.only(left: UIData.percentWidth(context, 40), right: UIData.percentWidth(context, 40)),
                        scrollDirection: Axis.horizontal,
                        children: getExpenseItems(snapshot),
                      );
                    }),
              ),
            ),
          ),
        ],
      ),
    );
  }

  @override
  void dispose() {
    super.dispose();
  }

  getExpenseItems(AsyncSnapshot<QuerySnapshot> snapshot) {
    List<ElementTask> listElement = new List(), listElement2;
    Map<String, List<ElementTask>> userMap = new Map();

    List<String> cardColor = new List();

    if (widget.user.uid.isNotEmpty) {
      cardColor.clear();
      notCompletedList.clear();
      completedList.clear();

      snapshot.data.documents.map<List>((f) {
        String color;
        f.data.forEach((a, b) {
          if (b.runtimeType == bool) {
            listElement.add(new ElementTask(a, b));
          }
          if (b.runtimeType == String && a == "color") {
            color = b;
          }
        });
        listElement2 = new List<ElementTask>.from(listElement);
        for (int i = 0; i < listElement2.length; i++) {
          if (listElement2.elementAt(i).isDone == false) {
            userMap[f.documentID] = listElement2;
            cardColor.add(color);
            notCompletedList.add(listElement2.elementAt(i).isDone);
            break;
          }
          else{
            completedList.add(listElement2.elementAt(i).isDone);
          }
        }
        if (listElement2.length == 0) {
          userMap[f.documentID] = listElement2;
          cardColor.add(color);
        }
        listElement.clear();
      }).toList();
       try{
         todoPercent = (completedList.length/(completedList.length + notCompletedList.length) * 100).round();
       }
       catch(Exception){
         print (Exception.toString());
       }
      return new List.generate(userMap.length, (int index) {
        return new GestureDetector(
          onTap: () {
            Navigator.of(context).push(
              new PageRouteBuilder(
                pageBuilder: (_, __, ___) => new DetailPage(
                      user: widget.user,
                      i: index,
                      currentList: userMap,
                      color: cardColor.elementAt(index),
                    ),
                transitionsBuilder:
                    (context, animation, secondaryAnimation, child) =>
                        new ScaleTransition(
                          scale: new Tween<double>(
                            begin: 1.5,
                            end: 1.0,
                          ).animate(
                            CurvedAnimation(
                              parent: animation,
                              curve: Interval(
                                0.50,
                                1.00,
                                curve: Curves.linear,
                              ),
                            ),
                          ),
                          child: ScaleTransition(
                            scale: Tween<double>(
                              begin: 0.0,
                              end: 1.0,
                            ).animate(
                              CurvedAnimation(
                                parent: animation,
                                curve: Interval(
                                  0.00,
                                  0.50,
                                  curve: Curves.linear,
                                ),
                              ),
                            ),
                            child: child,
                          ),
                        ),
              ),
            );
          },
          child: Card(
            shape: RoundedRectangleBorder(
              borderRadius: BorderRadius.all(Radius.circular(8.0)),
            ),
            color: Color(int.parse(cardColor.elementAt(index))),
            child: new Container(
              width: UIData.percentWidth(context, 300),
              //height: 100.0,
              child: Container(
                child: Column(
                  children: <Widget>[
                    Padding(
                      padding: EdgeInsets.only(top: UIData.percentHeight(context, 20), bottom: UIData.percentHeight(context, 15)),
                      child: Container(
                        child: Text(
                          userMap.keys.elementAt(index),
                          style: TextStyle(
                            color: Colors.white,
                            fontSize: UIData.percentHeight(context, 25),
                          ),
                        ),
                      ),
                    ),
                    Padding(
                      padding: EdgeInsets.only(top: UIData.percentHeight(context, 10)),
                      child: Row(
                        children: <Widget>[
                          Expanded(
                            flex: 2,
                            child: Container(
                              margin: EdgeInsets.only(left: UIData.percentWidth(context, 50)),
                              color: Colors.white,
                              height: UIData.percentHeight(context, 2)
                            ),
                          ),
                        ],
                      ),
                    ),
                    Padding(
                      padding:
                          EdgeInsets.only(top: UIData.percentHeight(context, 30), left: UIData.percentWidth(context, 15), right: UIData.percentWidth(context, 5)),
                      child: Column(
                        children: <Widget>[
                          SizedBox(
                            height: UIData.percentHeight(context, 250),
                            child: ListView.builder(
                                //physics: const NeverScrollableScrollPhysics(),
                                itemCount:
                                    userMap.values.elementAt(index).length,
                                itemBuilder: (BuildContext ctxt, int i) {
                                  return Row(
                                    mainAxisAlignment: MainAxisAlignment.start,
                                    children: <Widget>[
                                      Icon(
                                        userMap.values
                                                .elementAt(index)
                                                .elementAt(i)
                                                .isDone
                                            ? FontAwesomeIcons.checkCircle
                                            : FontAwesomeIcons.circle,
                                        color: userMap.values
                                                .elementAt(index)
                                                .elementAt(i)
                                                .isDone
                                            ? Colors.white70
                                            : Colors.white,
                                        size: UIData.percentHeight(context, 18),
                                      ),
                                      Padding(
                                        padding: EdgeInsets.only(left: UIData.percentWidth(context, 10)),
                                      ),
                                      Flexible(
                                        child: Text(
                                          userMap.values
                                              .elementAt(index)
                                              .elementAt(i)
                                              .name,
                                          style: userMap.values
                                                  .elementAt(index)
                                                  .elementAt(i)
                                                  .isDone
                                              ? TextStyle(
                                                  decoration: TextDecoration
                                                      .lineThrough,
                                                  color: Colors.white70,
                                                  fontSize: UIData.percentHeight(context, 22),
                                                )
                                              : TextStyle(
                                                  color: Colors.white,
                                                  fontSize: UIData.percentHeight(context, 22),
                                                ),
                                        ),
                                      ),
                                    ],
                                  );
                                }),
                          ),
                        ],
                      ),
                    ),
                  ],
                ),
              ),
            ),
          ),
        );
      });
    }
  }

  SessionManager _sessionManager;
  Stream<int> stream;

  Future<Object> initialisePrefs() async {
    SharedPreferences prefs = await SharedPreferences.getInstance();
    SessionManager sessionManager = new SessionManager(prefs);
    return sessionManager;
  }

  @override
  void initState() {
    super.initState();

    // SystemChrome.setPreferredOrientations([
    //   DeviceOrientation.portraitUp,
    //   DeviceOrientation.portraitDown,
    // ]);

    initialisePrefs().then((sessionManager){
      setState(() {
            _sessionManager = sessionManager;
        });
    });

     //print("Creating a sample stream...");
     stream = new Stream.fromFuture(getData());
     //print("Created the stream");

     stream.listen((data) {
       //print("DataReceived: "+ data.toString());
     }, onDone: () {
       setState(() {
         
       });
     }, onError: (error) {
       //print("Some Error");
     });
  }

  Future<int> getData() async {
    await Future.delayed(Duration(seconds: 1)); //Mock delay 
    //print("Fetched Data");
    return todoPercent;
  }

  void _addTaskPressed() async {
    Navigator.push(
      context,
      new MaterialPageRoute(
            builder: (context) => new NewTaskPage(
               user: widget.user,
             ),
          )
      // new PageRouteBuilder(
      //   pageBuilder: (_, __, ___) => new NewTaskPage(
      //         user: widget.user,
      //       ),
      //   transitionsBuilder: (context, animation, secondaryAnimation, child) =>
      //       new ScaleTransition(
      //         scale: new Tween<double>(
      //           begin: 1.5,
      //           end: 1.0,
      //         ).animate(
      //           CurvedAnimation(
      //             parent: animation,
      //             curve: Interval(
      //               0.50,
      //               1.00,
      //               curve: Curves.linear,
      //             ),
      //           ),
      //         ),
      //         child: ScaleTransition(
      //           scale: Tween<double>(
      //             begin: 0.0,
      //             end: 1.0,
      //           ).animate(
      //             CurvedAnimation(
      //               parent: animation,
      //               curve: Interval(
      //                 0.00,
      //                 0.50,
      //                 curve: Curves.linear,
      //               ),
      //             ),
      //           ),
      //           child: child,
      //         ),
      //       ),
      // ),
    );
    //Navigator.of(context).pushNamed('/new');
  }

  Padding _getToolbar(BuildContext context) {
    return new Padding(
      padding: EdgeInsets.only(top: UIData.percentHeight(context, 50), left: UIData.percentWidth(context, 20), right: UIData.percentWidth(context, 20)),
      child: new Row(mainAxisAlignment: MainAxisAlignment.center, children: [
        new Image(
            width: UIData.percentWidth(context, 40),
            height: UIData.percentHeight(context, 40),
            fit: BoxFit.cover,
            image: new AssetImage('assets/images/list.png')
        ),
      ]),
    );
  }

  Widget profileHeader(BuildContext context) => Container(
        height: UIData.percentHeight(context, 300),
        width: double.infinity,
        color: UIData.joltBlueAccent,
        child: Padding(
          padding: EdgeInsets.symmetric(vertical: UIData.percentHeight(context, 10), horizontal: UIData.percentWidth(context, 10)),
          child: new Container(
            color: UIData.joltBlueAccent,
            child: FittedBox(
              child: new Row(
                children: <Widget>[
                  Column(
                    mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                    children: <Widget>[
                      new Row(
                            mainAxisAlignment: MainAxisAlignment.center,
                            children: <Widget>[
                              new GestureDetector(
                            child: Container(
                              child: CircleAvatar(
                                radius: UIData.percentHeight(context, 30),
                                backgroundImage: widget.sessionManager.getSessionDetails()['avatar'] == null ? null : NetworkImage(widget.sessionManager.getSessionDetails()['avatar'])
                                )
                            ),
                            onTap: (){
                              Navigator.of(context).pushNamed('/my_profile');
                            } 
                          ),
                          new Container(
                            height: UIData.percentHeight(context, 100),
                            width: UIData.percentWidth(context, 1),
                            color: Colors.transparent,
                            margin: EdgeInsets.only(left: UIData.percentWidth(context, 10), right: UIData.percentWidth(context, 10)),
                          ),
                          new Column(
                            crossAxisAlignment: CrossAxisAlignment.start,
                            mainAxisSize: MainAxisSize.min,
                            children: <Widget>[
                              new Row(
                                children: <Widget>[
                                    new Text(todoPercent.toString(), style: TextStyle(fontSize: UIData.percentHeight(context, 45), color: Colors.white)),
                                    new Text("%", style: TextStyle(fontSize: UIData.percentHeight(context, 15), color: Colors.white)),
                                ],
                              ),
                              
                              Text(
                                "Tasks Completed",
                                style: TextStyle(
                                  color: Colors.white,
                                  fontSize: UIData.percentHeight(context, 10),
                                
                                ),
                                textAlign: TextAlign.center,
                              )
                              // RaisedButton(
                              //   shape: new RoundedRectangleBorder(borderRadius: new BorderRadius.circular(20.0)),
                              //   elevation: UIData.percentHeight(context, 1),
                              //   child: Text('View Stats',
                              //   style: TextStyle(
                              //     color: UIData.joltBlueAccent,
                              //     fontFamily: "WorkSansSemiBold",
                              //     fontSize: UIData.percentHeight(context, 15)
                              //   ),),
                              //   color: Colors.white,
                              //   onPressed: (){
                              //     //print('View Stats');
                              //   },
                              // )

                            ],
                          )
                          
                        ],
                      ),
                      Text(
                        widget.sessionManager.getSessionDetails()['fullname'] == null ? "" : widget.sessionManager.getSessionDetails()['fullname'],
                        style: TextStyle(color: Colors.white, fontSize: UIData.percentHeight(context, 18), fontFamily: "WorkSansSemiBold"),
                      ),
                      SizedBox(
                        height: UIData.percentHeight(context, 5),
                      ),
                      Text(
                        widget.sessionManager.getSessionDetails()['description'] == null ? "" : widget.sessionManager.getSessionDetails()['description'],
                        style: TextStyle(color: Colors.white, fontSize: UIData.percentHeight(context, 10), fontFamily: "WorkSansMedium"),
                      )
                    ],
                  ),
                ],
              )
            ),
          )
        ),
      );

}
