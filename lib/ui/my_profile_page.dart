import 'package:flutter/material.dart';
import 'package:font_awesome_flutter/font_awesome_flutter.dart';
import 'package:jolt/utils/uidata.dart';
import 'package:jolt/views/common_scaffold.dart';
import 'package:jolt/views/profile_tile.dart';
import 'package:jolt/views/activity_tile.dart';
import 'edit_profile_page.dart';
import 'view_photo.dart';
import 'package:shared_preferences/shared_preferences.dart';
import 'package:jolt/utils/career_session_manager.dart';
import 'dart:async';
import 'package:jolt/auth/jolt_auth.dart';

class MyProfilePage extends StatefulWidget {
  MyProfilePage({this.auth});
  final AuthImpl auth;

  @override
  _MyProfilePageState createState() => new _MyProfilePageState();
}


class _MyProfilePageState extends State<MyProfilePage> {
  Size deviceSize;
  bool _showDialog = true;
  List<ActivityTile> activityTiles = [];

  SessionManager _sessionManager;

  Future<Object> initialisePrefs() async {
    SharedPreferences prefs = await SharedPreferences.getInstance();
    SessionManager sessionManager = new SessionManager(prefs);
    return sessionManager;
  }

  Widget profileHeader(BuildContext context) {
    try{
      return Container(
        height: UIData.percentHeight(context, 300),
        width: double.infinity,
        color: UIData.joltBlueAccent,
        child: Padding(
          padding: EdgeInsets.symmetric(vertical: UIData.percentHeight(context, 10), horizontal: UIData.percentWidth(context, 10)),
          child: new Container(
            color: UIData.joltBlueAccent,
            child: FittedBox(
              child: Column(
                mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                children: <Widget>[
                  new GestureDetector(
                    child: Container(
                      padding: EdgeInsets.symmetric(vertical: UIData.percentHeight(context, 20), horizontal: UIData.percentWidth(context, 20)),
                      child: Container(
                          decoration: BoxDecoration(
                                borderRadius: BorderRadius.circular(50.0),
                                border: Border.all(width: 2.0, color: Colors.white)
                              ),
                          child: CircleAvatar(
                                radius: UIData.percentHeight(context, 50),
                                backgroundImage: NetworkImage(_sessionManager.getSessionDetails()['avatar'])
                              )
                        ),
                    ),
                    onTap: (){
                      Navigator.push(
                        context,
                        new MaterialPageRoute(
                          builder: (context) => new ViewPhotoPage(imageFilePath: _sessionManager.getSessionDetails()['avatar'],fromProfileView: true,),
                        ), //MaterialPageRoute
                      );
                    } 
                  ),
                  Text(
                    _sessionManager.getSessionDetails()['fullname'],
                    style: TextStyle(color: Colors.white, fontSize: UIData.percentHeight(context, 20), fontFamily: "WorkSansSemiBold"),
                  ),
                  SizedBox(
                    height: UIData.percentHeight(context, 10),
                  ),
                  Text(
                    _sessionManager.getSessionDetails()['description'],
                    style: TextStyle(color: Colors.white, fontSize: UIData.percentHeight(context, 10), fontFamily: "WorkSansMedium"),
                  )
                ],
              ),
            ),
          )
        ),
      );
    }
    catch(e){
    }
    
  } 

  

  Widget eachActivityRow(List<ActivityTile> activityTiles) => Padding(
        padding: EdgeInsets.symmetric(vertical: UIData.percentHeight(context, 8), horizontal: UIData.percentWidth(context, 8)),
        child: Row(
          mainAxisAlignment: MainAxisAlignment.start,
          mainAxisSize: MainAxisSize.max,
          children: <Widget>[
            Expanded(
              child: ListView.builder(
                  shrinkWrap: true,
                  physics: const BouncingScrollPhysics(),
                  itemCount: activityTiles.length,
                  itemBuilder: (context, i) => Padding(
                        padding: EdgeInsets.symmetric(vertical: UIData.percentHeight(context, 10), horizontal: UIData.percentWidth(context, 10)),
                        child: activityTiles[i]
                      ),
                ),
            )
          ],
        ),
      );
  
  
  
  Widget activitiesCard(BuildContext context, String user_id) {
    Widget wdgt;
    wdgt = new Container(
          child: Padding(
              padding: EdgeInsets.symmetric(vertical: UIData.percentHeight(context, 5), horizontal: UIData.percentWidth(context, 5)),
              child: Center(
                child: CircularProgressIndicator()
              ),
            )
          );
    widget.auth.grabActivities(context, user_id).then((retval){
      if(retval != null){
        setState(() {
          _showDialog = false;
          activityTiles = retval;
        });
      }
      else{
        setState(() {
          _showDialog = false;
        });
        
      }
    });
    return wdgt;
  }

  // Widget followColumn(Size deviceSize){
  //   try{
  //     return Container(
  //               height: deviceSize.height * 0.13,
  //               child: Row(
  //                 mainAxisAlignment: MainAxisAlignment.spaceAround,
  //                 children: <Widget>[
  //                   ProfileTile(
  //                     title: _sessionManager.getSessionDetails()['business_connects'] == "" ? "0": _sessionManager.getSessionDetails()['business_connects'],
  //                     subtitle: "Business Connects",
  //                     titleColor: UIData.joltBlueAccent,
  //                     subtitleColor: UIData.joltBlueAccent,
  //                   ),
  //                 ],
  //               ), 
  //             );
  //   }
  //   catch(e){
  //   }
  // }

  Widget bodyData(BuildContext context) => SingleChildScrollView(
        child: Column(
          children: <Widget>[
            profileHeader(context),
            //followColumn(deviceSize),
            SizedBox(
              height: UIData.percentHeight(context, 10),
            ),
            _showDialog ? activitiesCard(context, _sessionManager.getSessionDetails()['user_id']) : activityTiles.length != 0 ? new Container(
                                                                                child: Padding(
                                                                                    padding: EdgeInsets.symmetric(vertical: UIData.percentHeight(context, 2), horizontal: UIData.percentWidth(context, 2)),
                                                                                    child: Center(
                                                                                      child: eachActivityRow(activityTiles),
                                                                                    ),
                                                                                  )
                                                                                ) : Center(
                                                                                            child: Column(
                                                                                              mainAxisSize: MainAxisSize.min,
                                                                                              children: <Widget>[
                                                                                                Icon(FontAwesomeIcons.solidFolderOpen, color: UIData.joltBlueAccent, size: UIData.percentHeight(context, 120),),
                                                                                                Text("You have not done anything on Jolt yet", 
                                                                                                  style: TextStyle(
                                                                                                    color: UIData.joltBlueAccent,
                                                                                                    fontSize: UIData.percentHeight(context, 25),
                                                                                                    fontFamily: "SourceSansPro-Light",
                                                                                                  ),
                                                                                                  textAlign: TextAlign.center,
                                                                                                )
                                                                                              ],
                                                                                            )
    )
          ],
        ),
      );

  
  
  @override
  void initState() {
    super.initState();
    initialisePrefs().then((sessionManager){
      if(sessionManager != null){
        setState(() {
            _sessionManager = sessionManager;
        });
      }
    });
  }

  @override
  Widget build(BuildContext context) {
    deviceSize = MediaQuery.of(context).size;
    try{

      if (_sessionManager.getSessionDetails() != null){
        return CommonScaffold(
          appTitle: "My Profile",
          actionFirstIcon: Icons.edit,
          actionFirstIconFunc: (){
            Navigator.push(
                context,
                new MaterialPageRoute(
                  builder: (context) => new EditProfilePage(),
                ), //MaterialPageRoute
              );
          },
          bColor: UIData.joltBlueAccent,
          bodyData: bodyData(context),
        );
      }
      else{
        return Container(
            child: Padding(
                padding: EdgeInsets.symmetric(vertical: UIData.percentHeight(context, 5), horizontal: UIData.percentWidth(context, 5)),
                child: Center(
                  child: CircularProgressIndicator()
                ),
              )
            );
      }

    }
    catch(e){
      return Container(
            child: Padding(
                padding: EdgeInsets.symmetric(vertical: UIData.percentHeight(context, 5), horizontal: UIData.percentWidth(context, 5)),
                child: Center(
                  child: CircularProgressIndicator()
                ),
              )
            );
    }
    
  }
}
