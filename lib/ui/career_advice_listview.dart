import 'package:flutter/material.dart';
import 'package:font_awesome_flutter/font_awesome_flutter.dart';
import 'package:jolt/models/career_advice.dart';
import 'package:jolt/ui/career_advice_view.dart';
import 'package:jolt/auth/jolt_auth.dart';
import 'package:jolt/utils/uidata.dart';
import 'package:jolt/views/common_scaffold.dart';
import 'package:jolt/utils/career_session_manager.dart';
import 'package:shared_preferences/shared_preferences.dart';

class CareerAdviceList extends StatefulWidget{
  final AuthImpl auth;
  CareerAdviceList({this.auth});

  State<StatefulWidget> createState(){
    return _CareerAdviceListState();
  }
}

class _CareerAdviceListState extends State<CareerAdviceList>{
  Size deviceSize;

  SessionManager _sessionManager;
  bool _showDialog = true;
  bool _showEmpty = false;
  bool _killPage = false;
  List<CareerAdvice> _list = [];

  Future<Object> initialisePrefs() async {
    SharedPreferences prefs = await SharedPreferences.getInstance();
    SessionManager sessionManager = new SessionManager(prefs);
    return sessionManager;
  }

  @override
  void initState() {
      super.initState();
      initialisePrefs().then((sessionManager){
        setState(() {
              _sessionManager = sessionManager;
          });
      });
      getCareerAdviceLists(widget.auth).then((retVal){
        if(retVal.length == 0){
          setState(() {
            _showDialog = false;
            _showEmpty = true;
          });
        }
        else{
          setState(() {
            _list = retVal;
            _showDialog = false;
            _showEmpty = false;
          });
        }
      });
      
  }

  Widget bodyData (BuildContext context){
    print ("List_.length is " + _list.length.toString());
    if(_list.length > 0){
        return new ListView.builder(
                itemCount: _list.length,
                itemBuilder: (BuildContext context, int i) {
                    return new Container(
                        child: Material(
                              elevation: UIData.percentHeight(context, 15),
                              borderRadius: BorderRadius.circular(5.0),
                              shadowColor: UIData.joltBlueAccent,
                              type: MaterialType.card,
                              color: Colors.white,
                              child: ListTile(
                                leading: CircleAvatar(
                                  radius: 30.0,
                                  backgroundImage: NetworkImage(_list[i].imgUrl)
                                ),
                                title: Padding(
                                  padding: EdgeInsets.symmetric( vertical: UIData.percentHeight(context, 5)),
                                  child: Text(_list[i].title,
                                    style: TextStyle(
                                      fontSize: UIData.percentHeight(context, 18),
                                      fontFamily: "WorkSansBold"
                                    ),
                                  ),
                                ),
                                isThreeLine: true,
                                
                                subtitle: Padding(
                                  padding: EdgeInsets.symmetric( vertical: UIData.percentHeight(context, 10)),
                                  child: Column(
                                    crossAxisAlignment: CrossAxisAlignment.start,
                                    children: <Widget>[
                                      Text(_list[i].excerpt.length > 100 ? _list[i].excerpt.substring(0,100) + "..." : _list[i].excerpt + "...",
                                        style: TextStyle(
                                          fontSize: UIData.percentHeight(context, 16),
                                          fontFamily: "WorkSansMedium",
                                          color: UIData.joltBlue
                                        ),
                                      ),
                                      Container(
                                        margin: EdgeInsets.symmetric(vertical: UIData.percentHeight(context, 10)),
                                        height: UIData.percentHeight(context, 0),
                                        color: Colors.grey[500],
                                      ),
                                      Row(
                                        children: <Widget>[
                                          Icon(FontAwesomeIcons.solidHeart, color:Colors.red, size: UIData.percentHeight(context, 16),),
                                          SizedBox(width: UIData.percentWidth(context, 10),),
                                          Text("${_list[i].likes} likes",
                                            style: TextStyle(
                                              fontSize: UIData.percentHeight(context, 16),
                                              fontFamily: "WorkSansMedium",
                                              color: Colors.grey[800]
                                            ),
                                          )
                                        ],
                                      )
                                    ],
                                  ),
                                ),
                                onTap: (){
                                  print (_list[i].content);
                                  Navigator.push(
                                    context,
                                    new MaterialPageRoute(
                                      builder: (context) => new CareerAdviceViewPage(careerAdvice: new CareerAdvice(_list[i].id, _list[i].title, _list[i].content, _list[i].imgUrl, _list[i].excerpt, _list[i].likes), auth: new Auth(),),
                                    ), //MaterialPageRoute
                                  );
                                },
                              )
                            ),
                        padding: EdgeInsets.only(left: UIData.percentWidth(context, 20), right: UIData.percentWidth(context, 20), top: UIData.percentHeight(context, 20), bottom: 0.0)
                    );
                }
            );
    }
    else{
      if(_showEmpty){
        new Future.delayed(new Duration(seconds: 0), (){
          return showDialog(
            barrierDismissible: false,
            context: context,
            builder: (BuildContext context){
              return new WillPopScope(
                onWillPop: (){
                  Future.value(false);
                },
                child: SimpleDialog(
                  semanticLabel: 'Information',
                  contentPadding: EdgeInsets.symmetric(vertical: UIData.percentHeight(context, 30), horizontal: UIData.percentWidth(context, 30)),
                  children: <Widget>[
                      new Text(
                        'Information',
                        style: TextStyle(
                          color: UIData.joltBlueAccent,
                          fontFamily: "WorkSansMedium",
                          fontSize: UIData.percentHeight(context, 20)
                        ),
                      ),
                      SizedBox(
                        height: UIData.percentHeight(context, 15),
                      ),
                      new Text(
                        'There is nothing here yet. Please check back.',
                        style: TextStyle(
                          color: Colors.black,
                          fontFamily: "WorkSansMedium",
                          fontSize: UIData.percentHeight(context, 16)
                        ),
                        textAlign: TextAlign.center,
                      ),
                      SizedBox(
                        height: UIData.percentHeight(context, 10),
                      ),
                      RaisedButton(
                            elevation: UIData.percentHeight(context, 20),
                            child: Text('Okay, Thanks',
                            style: TextStyle(
                              color: UIData.joltBlueAccent,
                              fontFamily: "WorkSansMedium",
                              fontSize: UIData.percentHeight(context, 16)
                            ),),
                            color: Colors.white,
                            onPressed: (){
                              Navigator.of(context)..pop()..pop();
                            },
                          )
                    ],
                )
              );
            }
          );
      });
      }
    }
  }

  Future<List<CareerAdvice>> getCareerAdviceLists(AuthImpl authImpl) async{
    return authImpl.grabCareerAdvice();
  }

  @override
  Widget build(BuildContext context){
    deviceSize = MediaQuery.of(context).size;

    try{
      if(_sessionManager.getSessionDetails() != null){
        return CommonScaffold(
          appTitle: "Career Advice",
          backGroundColor: Colors.white,
          actionFirstIcon: null,
          bColor: UIData.joltBlueAccent,
          bodyData: _showDialog ? new Center(
            child: CircularProgressIndicator(),
          ) : bodyData(context),
        );
      }
      else{
        return Container(
            child: Padding(
                padding: EdgeInsets.symmetric(vertical: UIData.percentHeight(context, 5), horizontal: UIData.percentWidth(context, 5)),
                child: Center(
                  child: CircularProgressIndicator()
                ),
              )
            );
      }
    }
    catch(e){
      return Container(
            child: Padding(
                padding: EdgeInsets.symmetric(vertical: UIData.percentHeight(context, 5), horizontal: UIData.percentWidth(context, 5)),
                child: Center(
                  child: CircularProgressIndicator()
                ),
              )
            );
    }
    
  }

}