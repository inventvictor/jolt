import 'package:flutter/material.dart';
import 'package:jolt/utils/uidata.dart';
import 'package:jolt/views/common_scaffold.dart';
import 'package:jolt/utils/career_session_manager.dart';
import 'package:shared_preferences/shared_preferences.dart';
import 'package:font_awesome_flutter/font_awesome_flutter.dart';
import 'package:path_provider/path_provider.dart';
import 'dart:io';
import 'package:intl/intl.dart';
import 'package:jolt/ui/pdf_screen.dart';
import 'career_planner.dart';

class CareerPlannerList extends StatefulWidget{

  CareerPlannerList();

  State<StatefulWidget> createState(){
    return _CareerPlannerListState();
  }
}

class _CareerPlannerListState extends State<CareerPlannerList>{
  Size deviceSize;

  SessionManager _sessionManager;
  bool _showDialog = true;
  bool _showEmpty = false;
  bool _killPage = false;

  Future<Object> initialisePrefs() async {
    SharedPreferences prefs = await SharedPreferences.getInstance();
    SessionManager sessionManager = new SessionManager(prefs);
    return sessionManager;
  }

  @override
  void initState() {
      super.initState();
      initialisePrefs().then((sessionManager){
        setState(() {
              _sessionManager = sessionManager;
          });
      });
      getFileLists(); //.then((retval){
      //   if(retval == 0){
      //     _showDialog = false;
      //   }
      // });
      
  }

  Widget bodyData (BuildContext context){
    print ("List_.length is " + _list.length.toString());
    if(_list.length > 0){
      _list.sort((b, a) => a.split('_').removeAt(1).split('.').removeAt(0).compareTo(b.split('_').removeAt(1).split('.').removeAt(0)));
    final f = new DateFormat('yyyy/MM/dd hh:mm');
    return new ListView.builder(
                itemCount: _list.length,
                itemBuilder: (BuildContext context, int i) {
                    return new Container(
                        child: Material(
                              elevation: UIData.percentHeight(context, 15),
                              borderRadius: BorderRadius.circular(5.0),
                              shadowColor: UIData.joltBlueAccent,
                              type: MaterialType.card,
                              color: Colors.white,
                              child: ListTile(
                                leading: Icon(
                                  FontAwesomeIcons.solidFilePdf,
                                  color: Colors.red,
                                  size: UIData.percentHeight(context, 60),
                                ),
                                title: Text("Career Plan by " + _sessionManager.getSessionDetails()['fullname'],
                                  style: TextStyle(
                                    fontSize: UIData.percentHeight(context, 22),
                                    fontFamily: "WorkSansMedium"
                                  ),
                                ),
                                isThreeLine: true,
                                
                                subtitle: Text("you created this on " + f.format(DateTime.fromMillisecondsSinceEpoch(int.parse(_list.elementAt(i).split('_').removeAt(1).split('.').removeAt(0)))),
                                  style: TextStyle(
                                    fontSize: UIData.percentHeight(context, 20),
                                    fontFamily: "WorkSansMedium",
                                    color: UIData.joltBlue
                                  ),
                                ),
                                onTap: (){
                                  Navigator.push(
                                    context,
                                    MaterialPageRoute(builder: (context) => PDFScreen(pathPDF: _list.elementAt(i), showShare: false)
                                    ),
                                  );  
                                },
                              )
                            ),
                        padding: EdgeInsets.only(left: UIData.percentWidth(context, 25), right: UIData.percentWidth(context, 25), top: UIData.percentHeight(context, 25), bottom: 0.0)
                    );
                }
            );
    }
    else{
      setState(() {
        _showEmpty = true;
      });
      if(_showEmpty){
        new Future.delayed(new Duration(seconds: 0), (){
          return showDialog(
            barrierDismissible: false,
            context: context,
            builder: (BuildContext context){
              return new WillPopScope(
                onWillPop: (){
                  Future.value(false);
                },
                child: SimpleDialog(
                  semanticLabel: 'Information',
                  contentPadding: EdgeInsets.symmetric(vertical: UIData.percentHeight(context, 30), horizontal: UIData.percentWidth(context, 30)),
                  children: <Widget>[
                      new Text(
                        'Information',
                        style: TextStyle(
                          color: UIData.joltBlueAccent,
                          fontFamily: "WorkSansMedium",
                          fontSize: UIData.percentHeight(context, 20)
                        ),
                      ),
                      SizedBox(
                        height: UIData.percentHeight(context, 15),
                      ),
                      new Text(
                        'There is nothing here because you have not created any plan. Please go ahead and create a new plan',
                        style: TextStyle(
                          color: Colors.black,
                          fontFamily: "WorkSansMedium",
                          fontSize: UIData.percentHeight(context, 16)
                        ),
                      ),
                      SizedBox(
                        height: UIData.percentHeight(context, 10),
                      ),
                      RaisedButton(
                            elevation: UIData.percentHeight(context, 20),
                            child: Text('Okay, Thanks',
                            style: TextStyle(
                              color: UIData.joltBlueAccent,
                              fontFamily: "WorkSansMedium",
                              fontSize: UIData.percentHeight(context, 16)
                            ),),
                            color: Colors.white,
                            onPressed: (){
                              Navigator.of(context)..pop()..pop();
                            },
                          )
                    ],
                )
              );
            }
          );
      });
      }
    }
  }

  List<String> _list = [];

  Future<int> getFileLists() async{
    Directory tempDir = await getTemporaryDirectory();
    await tempDir.list(recursive: true, followLinks: false)
    .listen((FileSystemEntity entity) {
      if(entity.path.contains("careerplanner_")){
        //print(entity.path);
        _list.add(entity.path);
      }
      setState(() {
        _showDialog = false;
      });
    }
    );
    if(_list.isEmpty){
      setState(() {
        _showDialog = false;
      });
    }
    return _list.length;
  }
  

  @override
  Widget build(BuildContext context){
    deviceSize = MediaQuery.of(context).size;
    
    return CommonScaffold(
      appTitle: "Career Plans",
      backGroundColor: Colors.white,
      actionFirstIcon: null,
      bColor: UIData.joltBlueAccent,
      bodyData: _showDialog ? new Center(
        child: CircularProgressIndicator(),
      ) : bodyData(context),
    );
  }

}