import 'package:flutter/material.dart';

class SignUp extends StatelessWidget {

  static final String _iconTitle = "res/j.png";

  @override
  Widget build(BuildContext context) {

    ///detail of book image and it's pages
    final signUpInfo = Column(
      children: <Widget>[
        Padding(
          padding: EdgeInsets.all(16.0),
          child: Hero(
            tag: _iconTitle,
            child: Material(
              child: Image(
                image: AssetImage(_iconTitle),
                width: 100.00,
                height: 100.00,
                color: null,
                fit: BoxFit.scaleDown,
                alignment: Alignment.center,
              ),
            ),
          ),
        ),
        text('Sign xxUp', color: Colors.black38, size: 18, isBold: true),
      ],
      crossAxisAlignment: CrossAxisAlignment.center,
    );

    return Scaffold(
      body:new Container(
        padding: const EdgeInsets.all(32.0),
        child: new Center(
          child: new Column(
            crossAxisAlignment: CrossAxisAlignment.center,
            children: <Widget>[signUpInfo],
          ),
        ),
      )
    );
  }

  ///create text widget
  text(String data,
      {Color color = Colors.black87,
        num size = 14,
        EdgeInsetsGeometry padding = EdgeInsets.zero,
        bool isBold = false}) =>
      Padding(
        padding: padding,
        child: Text(
          data,
          style: TextStyle(
              color: color,
              fontSize: size.toDouble(),
              fontWeight: isBold ? FontWeight.bold : FontWeight.normal),
        ),
      );
}

