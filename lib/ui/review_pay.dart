import 'package:firebase_database/firebase_database.dart';
import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:jolt/utils/career_session_manager.dart';
import 'package:jolt/utils/uidata.dart';
import 'package:font_awesome_flutter/font_awesome_flutter.dart';
import 'package:rave_flutter/rave_flutter.dart';
import 'package:shared_preferences/shared_preferences.dart';


class ReviewPage extends StatefulWidget {

  final String amt;
  final String discTitle;
  final String discDetails;
  ReviewPage({Key key, @required this.amt, @required this.discTitle, @required this.discDetails}) : super(key: key);

  @override
  State<StatefulWidget> createState() => new _ReviewPageState();
  
}

class _ReviewPageState extends State<ReviewPage> {

  var scaffoldKey = GlobalKey<ScaffoldState>();
  SessionManager _sessionManager;
  String _txref = "";

  Future<SessionManager> initialisePrefs() async {
    SharedPreferences prefs = await SharedPreferences.getInstance();
    SessionManager sessionManager = new SessionManager(prefs);
    return sessionManager;
  }

  @override
  void initState() {
    super.initState();
    initialisePrefs().then((sessionManager){
      setState(() {
          _sessionManager = sessionManager;
      });
    });
    SystemChrome.setSystemUIOverlayStyle(SystemUiOverlayStyle(
      statusBarColor: UIData.joltBlueAccent
    ));

    SystemChrome.setPreferredOrientations([
      DeviceOrientation.portraitUp,
      DeviceOrientation.portraitDown,
    ]);

  }

  @override
  Widget build(BuildContext context) {

    if(_sessionManager == null){
      
      return Scaffold(
        body: Container(
          alignment: Alignment.center,
          child: CircularProgressIndicator(),
        ),
      );
    }
    else{
      return SafeArea(
        child: Scaffold(
          key: scaffoldKey,
          backgroundColor: Color(0xfff2f5f8),
          resizeToAvoidBottomInset: false,
          floatingActionButton: Container(
            margin: EdgeInsets.only(left: UIData.percentWidth(context, 20), right: UIData.percentWidth(context, 20), top: UIData.percentHeight(context, 20), bottom: UIData.percentHeight(context, 20),),
            height: UIData.percentHeight(context, 100),
            width: UIData.percentWidth(context, 100),
            child: FloatingActionButton(
              child: Row(
                mainAxisAlignment: MainAxisAlignment.center,
                children: <Widget>[
                  Icon(
                    FontAwesomeIcons.userLock,
                    color: Colors.white,
                    size: UIData.percentHeight(context, 20),
                  ),
                  SizedBox(width: UIData.percentWidth(context, 10),),
                  Text(
                    "PAY",
                    style: TextStyle(
                      color: Colors.white,
                      fontFamily: "RalewayBold",
                      fontSize: UIData.percentHeight(context, 20)
                    ),
                  ),
                ],
              ),
              onPressed: () async{
                print ("Pay now");
                print ("Amount : ${widget.amt.split(' ')[1]}");
                print ("Currency : ${widget.amt.split(' ')[0]}");
                print ("Country : ${_sessionManager.getSessionDetails()['country'].toString().toLowerCase() == "nigeria" ? "NG" : "US"}");
                print ("Email : ${_sessionManager.getSessionDetails()['email'].toString()}");
                print ("Firstname : ${_sessionManager.getSessionDetails()['fullname'].toString().split(' ')[0]}");
                print ("Lastname : ${_sessionManager.getSessionDetails()['fullname'].toString().split(' ')[1] == null ? '' : _sessionManager.getSessionDetails()['fullname'].toString().split(' ')[1]}");
                var initializer = RavePayInitializer(
                    amount: double.parse(widget.amt.split(' ')[1].replaceAll(',', '')),
                    publicKey: "FLWPUBK-d950c6d586c6c77d276d4216785bbc43-X",
                    companyLogo: Image(
                      image: AssetImage(UIData.joltImage),
                    ),
                    companyName: Text(
                      "Transcend Proserv LLC",
                      style: TextStyle(
                          fontSize: UIData.percentHeight(context, 18),
                          fontFamily: "WorkSansMedium",
                          color: UIData.joltBlueAccent
                        ),
                      ),
                    encryptionKey: "181849ca29a3f4f12665bd93"
                  )..country = _sessionManager.getSessionDetails()['country'].toString().toLowerCase() == "nigeria" ? "NG" : "US"
                  ..currency = widget.amt.split(' ')[0]
                  ..email = _sessionManager.getSessionDetails()['email'].toString()
                  ..fName = _sessionManager.getSessionDetails()['fullname'].toString().split(' ')[0]
                  ..lName = _sessionManager.getSessionDetails()['fullname'].toString().split(' ')[1] == null ? '' : _sessionManager.getSessionDetails()['fullname'].toString().split(' ')[1]
                  ..narration = widget.discTitle
                  ..txRef = "TP-" + DateTime.now().millisecondsSinceEpoch.toString()
                  ..acceptAccountPayments = true
                  ..acceptCardPayments = true
                  ..displayEmail = false
                  ..displayAmount = false
                  ..staging = false
                  ..displayFee = true;

                RaveResult response = await RavePayManager().prompt(context: context, initializer: initializer);
                //print(response);
                if(response.status == RaveStatus.success){
                  Navigator.pop(context);
                  FirebaseDatabase.instance.reference().child("activities").child(_sessionManager.getSessionDetails()['user_id']).push().child(new DateTime.now().millisecondsSinceEpoch.toString()).set("disc_assessment_test_update");
                  showSuccess(context, "Thank you for purchasing our DISC Assessment Test. Our admin will send you a test link within the next 24hours. Please note that the link expires in 14days, kindly ensure to take the test within that time frame. Thanks again.");
                }
                scaffoldKey.currentState.showSnackBar(SnackBar(content: Text(response?.message)));
              },
              backgroundColor: Colors.green,
              focusColor: Colors.white,
              splashColor: Colors.white,
            ),
          ),
          body: ListView(
            children: <Widget>[
              Padding(
                padding: EdgeInsets.only(left: UIData.percentWidth(context, 25), right: UIData.percentWidth(context, 25), top: UIData.percentHeight(context, 25), bottom: UIData.percentHeight(context, 25),),
                child: Row(
                  mainAxisAlignment: MainAxisAlignment.spaceBetween,
                  children: <Widget>[
                    InkWell(
                      child: Container(
                        child: Container(
                          width: UIData.percentWidth(context, 50),
                          height: UIData.percentHeight(context, 50),
                          child: Icon(FontAwesomeIcons.arrowLeft, color: UIData.joltBlueAccent,),
                        ),
                      ),
                      onTap: (){
                        print ("back pressed");
                        Navigator.pop(context);
                      },
                    ),
                    Text(
                      "Review and Pay",
                      style: TextStyle(
                        fontSize: UIData.percentHeight(context, 30),
                        color: UIData.joltBlueAccent,
                        fontFamily: "WorkSansBold"
                      ),
                    ),
                    Icon(
                      Icons.forward,
                      color: Color(0xfff2f5f8),
                      size: UIData.percentHeight(context, 50),
                    )
                  ],
                ),
              ),

              SizedBox(height: UIData.percentHeight(context, 20),),

              Container(
                margin: EdgeInsets.only(bottom: UIData.percentHeight(context, 5)),
                decoration: BoxDecoration(
                  borderRadius: BorderRadius.circular(25),
                ),
                child: Stack(
                  children: <Widget>[
                    Container(
                      padding: EdgeInsets.symmetric(horizontal: UIData.percentWidth(context, 10), vertical: UIData.percentHeight(context, 25),),
                      margin: EdgeInsets.symmetric(horizontal: UIData.percentWidth(context, 50), vertical: UIData.percentHeight(context, 50)),
                      decoration: BoxDecoration(
                        color: Colors.white,
                        borderRadius: BorderRadius.circular(25),
                        boxShadow: [
                          BoxShadow(
                            color: Colors.black12,
                            blurRadius: 13.0,
                            offset: Offset(0, 13),
                          ),
                        ],
                      ),
                      child: Container(
                        child: Column(
                          crossAxisAlignment: CrossAxisAlignment.start,
                          children: <Widget>[
                            Padding(
                              padding: EdgeInsets.symmetric(horizontal: UIData.percentWidth(context, 30), vertical: UIData.percentHeight(context, 30)),
                              child: Row(
                                mainAxisAlignment: MainAxisAlignment.start,
                                children: <Widget>[
                                  Container(
                                    width: UIData.percentWidth(context, 70),
                                    height: UIData.percentHeight(context, 70),
                                    margin: EdgeInsets.only(top: UIData.percentHeight(context, 30)),
                                    child: CircleAvatar(
                                      backgroundImage: NetworkImage(_sessionManager.getSessionDetails()['avatar'])
                                    )
                                  ),
                                  SizedBox(width: UIData.percentWidth(context, 20),),
                                  Container(
                                    margin: EdgeInsets.only(top: UIData.percentHeight(context, 30)),
                                    child: Column(
                                    crossAxisAlignment: CrossAxisAlignment.start,
                                    children: <Widget>[
                                        Container(
                                          width: UIData.percentWidth(context, 200),
                                          child: Text(
                                          _sessionManager.getSessionDetails()['fullname'],
                                          style: TextStyle(
                                            color: UIData.joltBlueAccent,
                                            fontFamily: "WorkSansBold",
                                            fontSize: UIData.percentHeight(context, 22)
                                          ),
                                            maxLines: 3,
                                          ),
                                        ),
                                        SizedBox(height: UIData.percentHeight(context, 10),),
                                        Container(
                                          width: UIData.percentWidth(context, 200),
                                          child: Text(
                                          _sessionManager.getSessionDetails()['email'],
                                          style: TextStyle(
                                            color: UIData.joltBlueAccent,
                                            fontFamily: "WorkSansMedium",
                                            fontSize: UIData.percentHeight(context, 20)
                                          ),
                                            maxLines: 3,
                                          ),
                                        ),
                                        
                                      ],
                                    )
                                  ),
                                ],
                              ),
                            )
                          ],
                        )
                      )
                    ),
                    
                    Positioned(
                      top: 0,
                      left: UIData.percentWidth(context, 115),
                      child: Container(
                        width: UIData.percentWidth(context, 255),
                        padding: EdgeInsets.all(9.0),
                        child: Padding(
                          padding: EdgeInsets.symmetric(horizontal: UIData.percentWidth(context, 15), vertical: UIData.percentHeight(context, 10)),
                          child: Text(
                            widget.amt,
                            style: TextStyle(
                              fontFamily: "WorkSansBold",
                              fontSize: UIData.percentHeight(context, 30),
                              color: Colors.white
                            ),
                            textAlign: TextAlign.center,
                          ),
                        ),
                        decoration: BoxDecoration(
                          color: Colors.green,
                          borderRadius: BorderRadius.all(Radius.circular(100)),
                          boxShadow: [
                            BoxShadow(
                              color: Colors.black12,
                              blurRadius: 5.0,
                              offset: Offset(0, 5),
                            ),
                          ],
                        ),
                      ),
                    ),
                  ],
                ),
              ),

              SizedBox(height: UIData.percentHeight(context, 20),),

              // Container(
              //   padding: EdgeInsets.symmetric(horizontal: UIData.percentWidth(context, 10), ),
              //   margin: EdgeInsets.symmetric(horizontal: UIData.percentWidth(context, 25),),
              //   child: Column(
              //     crossAxisAlignment: CrossAxisAlignment.start,
              //     children: <Widget>[
              //       Text(
              //         "Paying Bills",
              //         style: TextStyle(
              //           fontFamily: "WorkSansBold",
              //           color: UIData.joltBlueAccent,
              //           fontSize: UIData.percentHeight(context, 22)
              //         ),
              //       ),
              //       SizedBox(height: UIData.percentHeight(context, 10),),
              //       Text.rich(
              //         TextSpan(
              //           style: TextStyle(
              //             fontSize: UIData.percentHeight(context, 20)
              //           ),
              //           children: <TextSpan>[
              //             new TextSpan(
              //               text: "You are covered by ",
              //               style: new TextStyle(
              //                 color: Colors.grey[600],
              //                 fontFamily: "WorkSansMedium"
              //               ),
              //             ),
              //             new TextSpan(
              //               text: "Buyer Protection",
              //               style: new TextStyle(
              //                 fontFamily: "WorkSansMedium",
              //                 color: Colors.blueAccent
              //               ),
              //             ),
              //             new TextSpan(
              //               text: ". To learn more, please go ahead and read our ",
              //               style: new TextStyle(
              //                 color: Colors.grey[600],
              //                 fontFamily: "WorkSansMedium"
              //               ),
              //             ),
              //             new TextSpan(
              //               text: "Terms of Service",
              //               style: new TextStyle(
              //                 fontFamily: "WorkSansMedium",
              //                 color: Colors.blueAccent
              //               ),
              //             ),
              //             new TextSpan(
              //               text: " and ",
              //               style: new TextStyle(
              //                 color: Colors.grey[600],
              //                 fontFamily: "WorkSansMedium"
              //               ),
              //             ),
              //             new TextSpan(
              //               text: "Privacy Policy",
              //               style: new TextStyle(
              //                 fontFamily: "WorkSansMedium",
              //                 color: Colors.blueAccent
              //               ),
              //             ),
              //           ],
              //         ),
              //       ),
              //     ],
              //   ),
              // ),
              // Container(
              //   margin: EdgeInsets.symmetric(horizontal: UIData.percentWidth(context, 20), vertical: UIData.percentHeight(context, 20)),
              //   height: UIData.percentHeight(context, 1),
              //   color: Colors.grey[400],
              // ),
              
              Container(
                padding: EdgeInsets.symmetric(horizontal: UIData.percentWidth(context, 10), ),
                margin: EdgeInsets.symmetric(horizontal: UIData.percentWidth(context, 25),),
                child: Column(
                  crossAxisAlignment: CrossAxisAlignment.start,
                  children: <Widget>[
                    Text(
                      widget.discTitle,
                      style: TextStyle(
                        fontFamily: "WorkSansBold",
                        color: UIData.joltBlueAccent,
                        fontSize: UIData.percentHeight(context, 22)
                      ),
                    ),
                    SizedBox(height: UIData.percentHeight(context, 15),),
                    Text(
                      widget.discDetails,
                      style: TextStyle(
                        fontFamily: "WorkSansMedium",
                        color: Colors.grey[600],
                        fontSize: UIData.percentHeight(context, 18)
                      ),
                    ),
                  ],
                ),
              ),
            ]
          ),
        )
      );
    }
    
  }

  Future<Null> showSuccess(BuildContext context, String msg){
    return showDialog(
      context: context,
      barrierDismissible: true,
      builder: (BuildContext context){
        return SimpleDialog(
            contentPadding: EdgeInsets.only(left: UIData.percentWidth(context, 0), right: UIData.percentWidth(context, 0), top: UIData.percentHeight(context, 0), bottom: UIData.percentHeight(context, 0),),
            children: <Widget>[
              new Container(
                height: UIData.percentHeight(context, 100),
                decoration: new BoxDecoration(
                  color: Colors.green,
                ),
                child: new Center(
                  child: Icon(
                    FontAwesomeIcons.solidCheckCircle,
                    color: Colors.white,
                    size: UIData.percentHeight(context, 50),
                  ),
                )),
              new SizedBox(
                height: UIData.percentHeight(context, 20),
              ),
              new Container(
                padding: EdgeInsets.only(left: UIData.percentWidth(context, 20), right: UIData.percentWidth(context, 20), top: UIData.percentHeight(context, 20), bottom: UIData.percentHeight(context, 20)),
                child: Column(
                  crossAxisAlignment: CrossAxisAlignment.start,
                  children: <Widget>[
                    Center(
                      child: Text(
                        "$msg",
                        style: TextStyle(
                          fontSize: UIData.percentHeight(context, 22),
                          fontFamily: "WorkSansMedium"
                        ),
                        textAlign: TextAlign.center,
                      ),
                    ),
                    new SizedBox(
                      height: UIData.percentHeight(context, 30),
                    ),
                  ],
                ),
              ),
            ],
        ); 
      }
    ); 
  }

  Future<Null> showError(BuildContext context, String msg){
    return showDialog(
      context: context,
      barrierDismissible: true,
      builder: (BuildContext context){
        return SimpleDialog(
            contentPadding: EdgeInsets.only(left: UIData.percentWidth(context, 0), right: UIData.percentWidth(context, 0), top: UIData.percentHeight(context, 0), bottom: UIData.percentHeight(context, 0),),
            children: <Widget>[
              new Container(
                height: UIData.percentHeight(context, 110),
                decoration: new BoxDecoration(
                  color: Colors.redAccent,
                ),
                child: new Center(
                  child: Icon(
                    FontAwesomeIcons.exclamationCircle,
                    color: Colors.white,
                    size: UIData.percentHeight(context, 50),
                  ),
                )),
              new SizedBox(
                height: UIData.percentHeight(context, 20),
              ),
              new Container(
                padding: EdgeInsets.only(left: UIData.percentWidth(context, 12), right: UIData.percentWidth(context, 12), top: UIData.percentHeight(context, 12), bottom: UIData.percentHeight(context, 12),),
                child: Column(
                  children: <Widget>[
                    new Center(
                        child: new Text(msg,
                          style: TextStyle(
                            fontSize: UIData.percentHeight(context, 22),
                            fontFamily: "WorkSansMedium"
                          ),
                          textAlign: TextAlign.center,
                        ),
                      ),
                    new SizedBox(
                      height: UIData.percentHeight(context, 20),
                    ),
                    new Padding(
                      padding: EdgeInsets.only(left: UIData.percentWidth(context, 10), right: UIData.percentWidth(context, 10), top: UIData.percentHeight(context, 10), bottom: UIData.percentHeight(context, 10),),
                    )
                      ],
                    ),
              ),
            ],
        ); 
      }
    ); 
  }


}