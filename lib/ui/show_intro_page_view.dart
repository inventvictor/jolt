import 'package:flutter/material.dart';
import 'intro_page_view.dart';
import 'package:jolt/views/common_scaffold.dart';
import 'package:shared_preferences/shared_preferences.dart';
import 'package:jolt/utils/career_session_manager.dart';

class ShowIntroPageView extends StatefulWidget {
  ShowIntroPageView();
  @override
  _ShowIntroPageViewState createState() => new _ShowIntroPageViewState();
}

class _ShowIntroPageViewState extends State<ShowIntroPageView> {

  SessionManager _sessionManager;

  Future<Object> initialisePrefs() async {
    SharedPreferences prefs = await SharedPreferences.getInstance();
    SessionManager sessionManager = new SessionManager(prefs);
    return sessionManager;
  }
  
  @override
  void initState() {
    super.initState();
    initialisePrefs().then((sessionManager){
      setState(() {
            _sessionManager = sessionManager;
        });
    });
  }

  @override
  Widget build(BuildContext context) {
    return CommonScaffold(
      appTitle: "Career Checklist",
      actionFirstIcon: null,
      showImageNotIcon: true,
      bColor: Colors.black,
      bodyData: new IntroPageView(),
      elevation: 0.0,
    );
  }
}