import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:font_awesome_flutter/font_awesome_flutter.dart';
import 'package:jolt/utils/uidata.dart';
import 'package:jolt/style/login_theme.dart' as Theme;
import 'package:jolt/utils/bubble_indication_painter.dart';
import 'package:jolt/utils/career_session_manager.dart';
import 'package:shared_preferences/shared_preferences.dart';
import 'package:jolt/auth/jolt_auth.dart';
import 'dart:ui';
import 'package:fluttertoast/fluttertoast.dart';
import 'package:firebase_auth/firebase_auth.dart';
import 'home.dart';
import 'package:firebase_database/firebase_database.dart';
import 'package:jolt/models/career_person.dart';
import 'dart:async';

class LoginPage extends StatefulWidget {
  LoginPage({this.auth});
  final AuthImpl auth;
  
  @override
  _LoginPageState createState() => new _LoginPageState();
}

class _LoginPageState extends State<LoginPage>
    with SingleTickerProviderStateMixin {

  final GlobalKey<ScaffoldState> _scaffoldKey = new GlobalKey<ScaffoldState>();

  final FocusNode myFocusNodeEmailLogin = FocusNode();
  final FocusNode myFocusNodePasswordLogin = FocusNode();

  final FocusNode myFocusNodePassword = FocusNode();
  final FocusNode myFocusNodeEmail = FocusNode();
  final FocusNode myFocusNodeName = FocusNode();

  TextEditingController loginEmailController = new TextEditingController();
  TextEditingController loginPasswordController = new TextEditingController();

  bool _obscureTextLogin = true;
  bool _obscureTextSignup = true;
  bool _obscureTextSignupConfirm = true;

  TextEditingController signupEmailController = new TextEditingController();
  TextEditingController signupNameController = new TextEditingController();
  TextEditingController signupPasswordController = new TextEditingController();
  TextEditingController signupConfirmPasswordController =
      new TextEditingController();

  PageController _pageController;

  Color left = Colors.black;
  Color right = Colors.white;

  SessionManager _sessionManager;

  Future<Object> initialisePrefs() async {
    SharedPreferences prefs = await SharedPreferences.getInstance();
    SessionManager sessionManager = new SessionManager(prefs);
    return sessionManager;
  }


  @override
  Widget build(BuildContext context) {
    // if(!_sessionManager.getSessionDetails()['is_logged_in']){
    //   //print(loginEmailController.text);
    //   //print(loginPasswordController.text);
    //   return _buildProgressDialog(context);
    // }
    // else{
      return new Scaffold(
        key: _scaffoldKey,
        body: NotificationListener<OverscrollIndicatorNotification>(
          onNotification: (overscroll) {
            overscroll.disallowGlow();
          },
          child: SingleChildScrollView(
                child: Container(
                  width: MediaQuery.of(context).size.width,
                  height: MediaQuery.of(context).size.height >= 775.0
                      ? MediaQuery.of(context).size.height
                      : 775.0,
                  decoration: new BoxDecoration(
                    gradient: new LinearGradient(
                        colors: [
                          Theme.Colors.loginGradientStart,
                          Theme.Colors.loginGradientEnd
                        ],
                        begin: const FractionalOffset(0.0, 0.0),
                        end: const FractionalOffset(1.0, 1.0),
                        stops: [0.0, 1.0],
                        tileMode: TileMode.clamp),
                  ),
                  child: Column(
                    mainAxisSize: MainAxisSize.max,
                    children: <Widget>[
                      Padding(
                        padding: EdgeInsets.only(top: UIData.percentHeight(context, 100)),
                        child: new Image(
                            width: UIData.percentWidth(context, 280),
                            //height: UIData.percentHeight(context, 211),
                            fit: BoxFit.fill,
                            image: new AssetImage('assets/images/logo.png')),
                      ),
                      Padding(
                        padding: EdgeInsets.only(top: UIData.percentHeight(context, 20)),
                        child: _buildMenuBar(context),
                      ),
                      Expanded(
                        flex: 2,
                        child: PageView(
                          controller: _pageController,
                          onPageChanged: (i) {
                            if (i == 0) {
                              setState(() {
                                right = Colors.white;
                                left = Colors.black;
                              });
                            } else if (i == 1) {
                              setState(() {
                                right = Colors.black;
                                left = Colors.white;
                              });
                            }
                          },
                          children: <Widget>[
                            new ConstrainedBox(
                              constraints: const BoxConstraints.expand(),
                              child: _buildSignIn(context),
                            ),
                            new ConstrainedBox(
                              constraints: const BoxConstraints.expand(),
                              child: _buildSignUp(context),
                            ),
                          ],
                        ),
                      ),
                    ],
                  ),
                ),
              ),
        ),
      );
    //}
  }

  @override
  void dispose() {
    myFocusNodePassword.dispose();
    myFocusNodeEmail.dispose();
    myFocusNodeName.dispose();
    _pageController?.dispose();
    super.dispose();
  }

  @override
  void initState() {
    super.initState();

    SystemChrome.setPreferredOrientations([
      DeviceOrientation.portraitUp,
      DeviceOrientation.portraitDown,
    ]);

    _pageController = PageController();
    initialisePrefs().then((sessionManager){
      setState(() {
            _sessionManager = sessionManager;
        });
    });
  }

  void showInSnackBar(String value) {
    FocusScope.of(context).requestFocus(new FocusNode());
    _scaffoldKey.currentState?.removeCurrentSnackBar();
    _scaffoldKey.currentState.showSnackBar(new SnackBar(
      content: new Text(
        value,
        textAlign: TextAlign.center,
        style: TextStyle(
            color: Colors.white,
            fontSize: UIData.percentHeight(context, 20),
            fontFamily: "WorkSansSemiBold"),
      ),
      backgroundColor: UIData.joltBlue,
      duration: Duration(seconds: 3),
    ));
  }

  Widget _buildMenuBar(BuildContext context) {
    return Container(
      width: UIData.percentWidth(context, 300),
      height: UIData.percentHeight(context, 50),
      decoration: BoxDecoration(
        color: Color(0x552B2B2B),
        borderRadius: BorderRadius.all(Radius.circular(25.0)),
      ),
      child: CustomPaint(
        painter: TabIndicationPainter(pageController: _pageController, context: context),
        child: Row(
          mainAxisAlignment: MainAxisAlignment.spaceEvenly,
          children: <Widget>[
            Expanded(
              child: FlatButton(
                splashColor: Colors.transparent,
                highlightColor: Colors.transparent,
                onPressed: _onSignInButtonPress,
                child: Text(
                  "Exists",
                  style: TextStyle(
                      color: left,
                      fontSize: UIData.percentHeight(context, 15),
                      fontFamily: "WorkSansSemiBold"),
                ),
              ),
            ),
            //Container(height: 33.0, width: 1.0, color: Colors.white),
            Expanded(
              child: FlatButton(
                splashColor: Colors.transparent,
                highlightColor: Colors.transparent,
                onPressed: _onSignUpButtonPress,
                child: Text(
                  "New",
                  style: TextStyle(
                      color: right,
                      fontSize: UIData.percentHeight(context, 15),
                      fontFamily: "WorkSansSemiBold"),
                ),
              ),
            ),
          ],
        ),
      ),
    );
  }

  Widget _buildProgressDialog(BuildContext context) {
    
    if(!_sessionManager.getSessionDetails()['is_logged_in']){
      //print("victor");
      return Scaffold(
        body: Stack(
          fit: StackFit.expand,
          children: <Widget>[
            _scaffoldKey.currentWidget,
            BackdropFilter(
              filter: ImageFilter.blur(sigmaX: 10.0, sigmaY: 10.0),
              child: Container(
                color: Colors.black.withOpacity(0.5),
                child: CircularProgressIndicator(),
                // TODO: child: _buildContent(),
              ),
            ),
          ],
        ),
      );
    }
  }

  Widget _buildSignIn(BuildContext context) {
    return Container(
      padding: EdgeInsets.only(top: UIData.percentHeight(context, 25)),
      child: Column(
        children: <Widget>[
          Stack(
            alignment: Alignment.topCenter,
            overflow: Overflow.visible,
            children: <Widget>[
              Card(
                elevation: UIData.percentHeight(context, 2),
                color: Colors.white,
                shape: RoundedRectangleBorder(
                  borderRadius: BorderRadius.circular(8.0),
                ),
                child: Container(
                  width: UIData.percentWidth(context, 350),
                  height: UIData.percentHeight(context, 220),
                  child: Column(
                    children: <Widget>[
                      Padding(
                        padding: EdgeInsets.only(
                            top: UIData.percentHeight(context, 20), bottom: UIData.percentHeight(context, 20), 
                            left: UIData.percentWidth(context, 25), right: UIData.percentWidth(context, 25)),
                        child: TextField(
                          focusNode: myFocusNodeEmailLogin,
                          controller: loginEmailController,
                          keyboardType: TextInputType.emailAddress,
                          style: TextStyle(
                              fontFamily: "WorkSansSemiBold",
                              fontSize: UIData.percentHeight(context, 20),
                              color: Colors.black),
                          decoration: InputDecoration(
                            border: InputBorder.none,
                            icon: Icon(
                              FontAwesomeIcons.envelope,
                              color: Colors.black,
                              size: UIData.percentHeight(context, 25),
                            ),
                            hintText: "Email Address",
                            hintStyle: TextStyle(
                                fontFamily: "WorkSansSemiBold", fontSize: UIData.percentHeight(context, 20)),
                          ),
                        ),
                      ),
                      Container(
                        width: UIData.percentWidth(context, 300),
                        height: UIData.percentHeight(context, 1),
                        color: Colors.grey[400],
                      ),
                      Padding(
                        padding: EdgeInsets.only(
                            top: UIData.percentHeight(context, 20), bottom: UIData.percentHeight(context, 20), 
                            left: UIData.percentWidth(context, 25), right: UIData.percentWidth(context, 25)),
                        child: TextField(
                          focusNode: myFocusNodePasswordLogin,
                          controller: loginPasswordController,
                          obscureText: _obscureTextLogin,
                          style: TextStyle(
                              fontFamily: "WorkSansSemiBold",
                              fontSize: UIData.percentHeight(context, 20),
                              color: Colors.black),
                          decoration: InputDecoration(
                            border: InputBorder.none,
                            icon: Icon(
                              FontAwesomeIcons.lock,
                              size: UIData.percentHeight(context, 25),
                              color: Colors.black,
                            ),
                            hintText: "Password",
                            hintStyle: TextStyle(
                                fontFamily: "WorkSansSemiBold", fontSize: UIData.percentHeight(context, 20)),
                            suffixIcon: GestureDetector(
                              onTap: _toggleLogin,
                              child: Icon(
                                FontAwesomeIcons.eye,
                                size: UIData.percentHeight(context, 18),
                                color: Colors.black,
                              ),
                            ),
                          ),
                        ),
                      ),
                    ],
                  ),
                ),
              ),
              Container(
                margin: EdgeInsets.only(top: UIData.percentHeight(context, 210)),
                decoration: new BoxDecoration(
                  borderRadius: BorderRadius.all(Radius.circular(5.0)),
                  boxShadow: <BoxShadow>[
                    BoxShadow(
                      color: Theme.Colors.loginGradientStart,
                      offset: Offset(1.0, 6.0),
                      blurRadius: 20.0,
                    ),
                    BoxShadow(
                      color: Theme.Colors.loginGradientEnd,
                      offset: Offset(1.0, 6.0),
                      blurRadius: 20.0,
                    ),
                  ],
                  gradient: new LinearGradient(
                      colors: [
                        Theme.Colors.loginGradientEnd,
                        Theme.Colors.loginGradientStart
                      ],
                      begin: const FractionalOffset(0.2, 0.2),
                      end: const FractionalOffset(1.0, 1.0),
                      stops: [0.0, 1.0],
                      tileMode: TileMode.clamp),
                ),
                child: MaterialButton(
                    highlightColor: Colors.transparent,
                    splashColor: Theme.Colors.loginGradientEnd,
                    //shape: RoundedRectangleBorder(borderRadius: BorderRadius.all(Radius.circular(5.0))),
                    child: Padding(
                      padding: EdgeInsets.symmetric( vertical: UIData.percentHeight(context, 10), horizontal: UIData.percentWidth(context, 32)),
                      child: Text(
                        "LOGIN",
                        style: TextStyle(
                            color: Colors.white,
                            fontSize: UIData.percentHeight(context, 20),
                            fontFamily: "WorkSansBold"),
                      ),
                    ),
                    onPressed: (){
                      //showInSnackBar("Login button pressed");
                      if(!isValidEmail(loginEmailController.text)){
                        Fluttertoast.showToast(
                            msg: "Your email is not valid",
                            toastLength: Toast.LENGTH_SHORT,
                            gravity: ToastGravity.CENTER,
                            timeInSecForIos: 1,
                            backgroundColor: Colors.red,
                            textColor: Colors.white,
                        );
                      }
                      else if(!isValidPassword(loginPasswordController.text)){
                        Fluttertoast.showToast(
                            msg: "Your password is too short",
                            toastLength: Toast.LENGTH_SHORT,
                            gravity: ToastGravity.CENTER,
                            timeInSecForIos: 1,
                            backgroundColor: Colors.red,
                            textColor: Colors.white
                        );
                      }
                      else{
                        showDialog(
                          context: context,
                          barrierDismissible: false,
                          builder: (BuildContext context){
                            return new WillPopScope(
                              onWillPop: (){
                                Future.value(false);
                              },
                              child: SimpleDialog(
                                contentPadding: EdgeInsets.all(0.0),
                                children: <Widget>[
                                  new Container(
                                    height: UIData.percentHeight(context, 75),
                                    decoration: new BoxDecoration(
                                      color: UIData.joltBlueAccent,
                                    ),
                                    child: new Center(
                                      child: Icon(
                                        FontAwesomeIcons.solidHeart,
                                        color: Colors.white,
                                      ),
                                    )),
                                  new SizedBox(
                                    height: UIData.percentHeight(context, 20),
                                  ),
                                  new Center(
                                    child: new Text("Hi there!",
                                      style: TextStyle(
                                        fontWeight: FontWeight.w700,
                                        fontSize: UIData.percentHeight(context, 20),
                                        fontFamily: "WorkSansSemiBold"
                                      ),
                                    ),
                                  ),
                                  new SizedBox(
                                    height: UIData.percentHeight(context, 10),
                                  ),
                                  new Center(
                                    child: CircularProgressIndicator(),
                                  ),
                                  new SizedBox(
                                    height: UIData.percentHeight(context, 20),
                                  ),
                                  new Center(
                                    child: new Text("Please wait...",
                                      style: TextStyle(
                                        fontSize: UIData.percentHeight(context, 20),
                                        fontFamily: "WorkSansSemiBold"
                                      ),
                                    ),
                                  ),
                                  new SizedBox(
                                    height: UIData.percentHeight(context, 20),
                                  ),
                                ],
                              ),
                            ); 
                          }
                        );
                        _logIn(context, loginEmailController.text, loginPasswordController.text);
                      }
                      //sessionManager.createLoginSession("victor", "victor", "victor", "victor", "victor", "victor", "country", 89, "22K+", "session", false);
                      //Navigator.of(context).pushNamed('/home');
                    }
                )
              ),
            ],
          ),
          Padding(
            padding: EdgeInsets.only(top: UIData.percentHeight(context, 10)),
            child: FlatButton(
                onPressed: () {},
                child: Text(
                  "Forgot Password?",
                  style: TextStyle(
                      decoration: TextDecoration.underline,
                      color: Colors.white,
                      fontSize: UIData.percentHeight(context, 20),
                      fontFamily: "WorkSansMedium"),
                )),
          ),
        ],
      ),
    );
  }

  Widget _buildSignUp(BuildContext context) {
    return Container(
      padding: EdgeInsets.only(top: UIData.percentHeight(context, 25)),
      child: Column(
        children: <Widget>[
          Stack(
            alignment: Alignment.topCenter,
            overflow: Overflow.visible,
            children: <Widget>[
              Card(
                elevation: UIData.percentHeight(context, 2),
                color: Colors.white,
                shape: RoundedRectangleBorder(
                  borderRadius: BorderRadius.circular(8.0),
                ),
                child: Container(
                  width: UIData.percentWidth(context, 350),
                  height: UIData.percentHeight(context, 450),
                  child: Column(
                    children: <Widget>[
                      Padding(
                        padding: EdgeInsets.only(
                            top: UIData.percentHeight(context, 20), bottom: UIData.percentHeight(context, 20), 
                            left: UIData.percentWidth(context, 25), right: UIData.percentWidth(context, 25)),
                        child: TextField(
                          focusNode: myFocusNodeName,
                          controller: signupNameController,
                          keyboardType: TextInputType.text,
                          textCapitalization: TextCapitalization.words,
                          style: TextStyle(
                              fontFamily: "WorkSansSemiBold",
                              fontSize: UIData.percentHeight(context, 20),
                              color: Colors.black),
                          decoration: InputDecoration(
                            border: InputBorder.none,
                            icon: Icon(
                              FontAwesomeIcons.user,
                              color: Colors.black,
                              size: UIData.percentHeight(context, 25),
                            ),
                            hintText: "Name",
                            hintStyle: TextStyle(
                                fontFamily: "WorkSansSemiBold", fontSize: UIData.percentHeight(context, 20)),
                          ),
                        ),
                      ),
                      Container(
                        width: UIData.percentWidth(context, 300),
                        height: UIData.percentHeight(context, 1),
                        color: Colors.grey[400],
                      ),
                      Padding(
                        padding: EdgeInsets.only(
                            top: UIData.percentHeight(context, 20), bottom: UIData.percentHeight(context, 20), 
                            left: UIData.percentWidth(context, 25), right: UIData.percentWidth(context, 25)),
                        child: TextField(
                          focusNode: myFocusNodeEmail,
                          controller: signupEmailController,
                          keyboardType: TextInputType.emailAddress,
                          style: TextStyle(
                              fontFamily: "WorkSansSemiBold",
                              fontSize: UIData.percentHeight(context, 20),
                              color: Colors.black),
                          decoration: InputDecoration(
                            border: InputBorder.none,
                            icon: Icon(
                              FontAwesomeIcons.envelope,
                              color: Colors.black,
                              size: UIData.percentHeight(context, 25),
                            ),
                            hintText: "Email Address",
                            hintStyle: TextStyle(
                                fontFamily: "WorkSansSemiBold", fontSize: UIData.percentHeight(context, 20)),
                          ),
                        ),
                      ),
                      Container(
                        width: UIData.percentWidth(context, 300),
                        height: UIData.percentHeight(context, 1),
                        color: Colors.grey[400],
                      ),
                      Padding(
                        padding: EdgeInsets.only(
                            top: UIData.percentHeight(context, 20), bottom: UIData.percentHeight(context, 20), 
                            left: UIData.percentWidth(context, 25), right: UIData.percentWidth(context, 25)),
                        child: TextField(
                          focusNode: myFocusNodePassword,
                          controller: signupPasswordController,
                          obscureText: _obscureTextSignup,
                          style: TextStyle(
                              fontFamily: "WorkSansSemiBold",
                              fontSize: UIData.percentHeight(context, 20),
                              color: Colors.black),
                          decoration: InputDecoration(
                            border: InputBorder.none,
                            icon: Icon(
                              FontAwesomeIcons.lock,
                              color: Colors.black,
                              size: UIData.percentHeight(context, 25),
                            ),
                            hintText: "Password",
                            hintStyle: TextStyle(
                                fontFamily: "WorkSansSemiBold", fontSize: UIData.percentHeight(context, 20)),
                            suffixIcon: GestureDetector(
                              onTap: _toggleSignup,
                              child: Icon(
                                FontAwesomeIcons.eye,
                                size: UIData.percentHeight(context, 18),
                                color: Colors.black,
                              ),
                            ),
                          ),
                        ),
                      ),
                      Container(
                        width: UIData.percentWidth(context, 300),
                        height: UIData.percentHeight(context, 1),
                        color: Colors.grey[400],
                      ),
                      Padding(
                        padding: EdgeInsets.only(
                            top: UIData.percentHeight(context, 20), bottom: UIData.percentHeight(context, 20), 
                            left: UIData.percentWidth(context, 25), right: UIData.percentWidth(context, 25)),
                        child: TextField(
                          controller: signupConfirmPasswordController,
                          obscureText: _obscureTextSignupConfirm,
                          style: TextStyle(
                              fontFamily: "WorkSansSemiBold",
                              fontSize: UIData.percentHeight(context, 20),
                              color: Colors.black),
                          decoration: InputDecoration(
                            border: InputBorder.none,
                            icon: Icon(
                              FontAwesomeIcons.lock,
                              color: Colors.black,
                              size: UIData.percentHeight(context, 25),
                            ),
                            hintText: "Confirmation",
                            hintStyle: TextStyle(
                                fontFamily: "WorkSansSemiBold", fontSize: UIData.percentHeight(context, 20)),
                            suffixIcon: GestureDetector(
                              onTap: _toggleSignupConfirm,
                              child: Icon(
                                FontAwesomeIcons.eye,
                                size: UIData.percentHeight(context, 18),
                                color: Colors.black,
                              ),
                            ),
                          ),
                        ),
                      ),
                    ],
                  ),
                ),
              ),
              Container(
                margin: EdgeInsets.only(top: UIData.percentHeight(context, 440)),
                decoration: new BoxDecoration(
                  borderRadius: BorderRadius.all(Radius.circular(5.0)),
                  boxShadow: <BoxShadow>[
                    BoxShadow(
                      color: Theme.Colors.loginGradientStart,
                      offset: Offset(1.0, 6.0),
                      blurRadius: 20.0,
                    ),
                    BoxShadow(
                      color: Theme.Colors.loginGradientEnd,
                      offset: Offset(1.0, 6.0),
                      blurRadius: 20.0,
                    ),
                  ],
                  gradient: new LinearGradient(
                      colors: [
                        Theme.Colors.loginGradientEnd,
                        Theme.Colors.loginGradientStart
                      ],
                      begin: const FractionalOffset(0.2, 0.2),
                      end: const FractionalOffset(1.0, 1.0),
                      stops: [0.0, 1.0],
                      tileMode: TileMode.clamp),
                ),
                child: MaterialButton(
                    highlightColor: Colors.transparent,
                    splashColor: Theme.Colors.loginGradientEnd,
                    //shape: RoundedRectangleBorder(borderRadius: BorderRadius.all(Radius.circular(5.0))),
                    child: Padding(
                      padding: EdgeInsets.symmetric(
                          vertical: UIData.percentHeight(context, 10), horizontal: UIData.percentWidth(context, 32)),
                      child: Text(
                        "SIGN UP",
                        style: TextStyle(
                            color: Colors.white,
                            fontSize: UIData.percentHeight(context, 20),
                            fontFamily: "WorkSansBold"),
                      ),
                    ),
                    onPressed: (){
                      //showInSnackBar("SignUp button pressed");
                      ////print(signupNameController.text);
                      if(!isValidFullName(signupNameController.text)){
                        Fluttertoast.showToast(
                            msg: "Your name is not valid",
                            toastLength: Toast.LENGTH_SHORT,
                            gravity: ToastGravity.CENTER,
                            timeInSecForIos: 1,
                            backgroundColor: Colors.red,
                            textColor: Colors.white
                        );
                      }
                      else if(!isValidEmail(signupEmailController.text)){
                        Fluttertoast.showToast(
                            msg: "Your email is not valid",
                            toastLength: Toast.LENGTH_SHORT,
                            gravity: ToastGravity.CENTER,
                            timeInSecForIos: 1,
                            backgroundColor: Colors.red,
                            textColor: Colors.white,
                        );
                      }
                      else if(!isValidPassword(signupPasswordController.text)){
                        Fluttertoast.showToast(
                            msg: "Your password is too short",
                            toastLength: Toast.LENGTH_SHORT,
                            gravity: ToastGravity.CENTER,
                            timeInSecForIos: 1,
                            backgroundColor: Colors.red,
                            textColor: Colors.white
                        );
                      }
                      else if(!isVaidConfrmPassword(signupPasswordController.text, signupConfirmPasswordController.text)){
                        Fluttertoast.showToast(
                            msg: "Check your confirmation password",
                            toastLength: Toast.LENGTH_SHORT,
                            gravity: ToastGravity.CENTER,
                            timeInSecForIos: 1,
                            backgroundColor: Colors.red,
                            textColor: Colors.white
                        );
                      }
                      else{
                        showDialog(
                          context: context,
                          barrierDismissible: false,
                          builder: (BuildContext context){
                            return new WillPopScope(
                              onWillPop: (){
                                Future.value(false);
                              },
                              child: SimpleDialog(
                                contentPadding: EdgeInsets.all(0.0),
                                children: <Widget>[
                                  new Container(
                                    height: UIData.percentHeight(context, 75),
                                    decoration: new BoxDecoration(
                                      color: UIData.joltBlueAccent,
                                    ),
                                    child: new Center(
                                      child: Icon(
                                        FontAwesomeIcons.user,
                                        color: Colors.white,
                                      ),
                                    )),
                                  new SizedBox(
                                    height: UIData.percentHeight(context, 20),
                                  ),
                                  new Center(
                                    child: CircularProgressIndicator(),
                                  ),
                                  new SizedBox(
                                    height: UIData.percentHeight(context, 20),
                                  ),
                                  new Center(
                                    child: new Text("Please wait...",
                                      style: TextStyle(
                                        fontSize: UIData.percentHeight(context, 20),
                                        fontFamily: "WorkSansSemiBold"
                                      ),
                                    ),
                                  ),
                                  new SizedBox(
                                    height: UIData.percentHeight(context, 20),
                                  ),
                                ],
                              ),
                            ); 
                          }
                        );
                        _signUp(context, signupNameController.text, signupEmailController.text, signupPasswordController.text);
                      }
                    }
                    )
              ),
            ],
          ),
        ],
      ),
    );
  }

  void _onSignInButtonPress() {
    _pageController.animateToPage(0,
        duration: Duration(milliseconds: 500), curve: Curves.decelerate);
  }

  void _onSignUpButtonPress() {
    _pageController?.animateToPage(1,
        duration: Duration(milliseconds: 500), curve: Curves.decelerate);
  }

  void _toggleLogin() {
    setState(() {
      _obscureTextLogin = !_obscureTextLogin;
    });
  }

  void _toggleSignup() {
    setState(() {
      _obscureTextSignup = !_obscureTextSignup;
    });
  }

  void _toggleSignupConfirm() {
    setState(() {
      _obscureTextSignupConfirm = !_obscureTextSignupConfirm;
    });
  }

  bool isValidEmail(String em) {
    String p = r'^(([^<>()[\]\\.,;:\s@\"]+(\.[^<>()[\]\\.,;:\s@\"]+)*)|(\".+\"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$';
    RegExp regExp = new RegExp(p);
    return regExp.hasMatch(em);
  } 

  bool isValidPassword(String pwd){
    bool b = false;
    if(pwd.length < 6){
      b = false;
    }
    else{
      b = true;
    }
    return b;
  }

  bool isVaidConfrmPassword(String pwd, String cPwd){
    bool b = false;
    if(pwd != cPwd){
      b = false;
    }
    else{
      b = true;
    }
    return b;
  }

  bool isValidFullName(String name){
    bool b = false;
    if(name.length < 1){
      b = false;
    }
    else{
      b = true;
    }
    return  b;
  }
  
  void _logIn(BuildContext context, String email, String password) {
    //print('login called...');
    widget.auth.signIn(email, password).then((retval){
      if(retval != null){
        //print(retval);
        if(retval is Exception){
          Exception e = new Exception(retval);
          //print(e.toString());
          if(e.toString().contains("network error")){
            Navigator.pop(context);
            return showError(context, "Please check your internet connection");
          }
          else if(e.toString().contains("The password is invalid or the user does not have a password") 
            || e.toString().contains("We have blocked all requests from this device due to unusual activity") 
            || e.toString().contains("Try again later")){
            Navigator.pop(context);
            return showError(context, "Email/password is incorrect");
          }
          else if(e.toString().contains("There is no user record corresponding to this identifier. The user may have been deleted")){
            Navigator.pop(context);
            return showError(context, "You are new here");
          }
        }
        else if(retval is DatabaseReference){
          DatabaseReference databaseReference = FirebaseDatabase.instance.reference().child("users");
          databaseReference.once().then((DataSnapshot snapshot){
                  Map<dynamic, dynamic> values = snapshot.value;
                    values.forEach((key,values) {
                      //print(values);
                      if(values['email'] == email){
                        CareerPerson careerPerson = new CareerPerson(values['avatar'], values['name'], values['email'], values['user_id'], values['phone'], values['desc'], values['city'], values['country'], values['age'], values['business_connects'], values['session']);
                        _sessionManager.createLoginSession(careerPerson.avatar, careerPerson.fullname, 
                            careerPerson.email, careerPerson.userID, careerPerson.phoneNumber, careerPerson.description, 
                            careerPerson.city, careerPerson.country, careerPerson.age, careerPerson.businessConnects, 
                            careerPerson.session, true, values['has_updated_profile']);
                        //print(careerPerson.fullname.toString().split(' ')[0]);
                        return goHome(careerPerson.fullname.toString().split(' ')[0]);
                      }
                    }
                  );
                }
              );
        }
      }
      else{
        //print('retval is null');
      }
      Navigator.pop(context);
    });
  }
  
  Future<Null> showError(BuildContext context, String msg){
    return showDialog(
      context: context,
      barrierDismissible: true,
      builder: (BuildContext context){
        return SimpleDialog(
            contentPadding: EdgeInsets.all(0.0),
            children: <Widget>[
              new Container(
                height: UIData.percentHeight(context, 75),
                decoration: new BoxDecoration(
                  color: Colors.redAccent,
                ),
                child: new Center(
                  child: Icon(
                    FontAwesomeIcons.exclamationCircle,
                    color: Colors.white,
                  ),
                )),
              new SizedBox(
                height: UIData.percentHeight(context, 20),
              ),
              new Container(
                padding: EdgeInsets.all(12.0),
                child: Column(
                  children: <Widget>[
                    new Center(
                        child: new Text(msg,
                          style: TextStyle(
                            fontSize: UIData.percentHeight(context, 20),
                            fontFamily: "WorkSansSemiBold"
                          ),
                          textAlign: TextAlign.center,
                        ),
                      ),
                    new SizedBox(
                      height: UIData.percentHeight(context, 20),
                    ),
                    new Padding(
                      padding: EdgeInsets.all(10.0),
                    )
                      ],
                    ),
              ),
            ],
        ); 
      }
    ); 
  }

  Future<Null> goHome(String firstname){
    //Navigator.pop(context);
    print (_sessionManager.getSessionDetails()['has_updated_profile']);
    return Navigator.pushReplacement(
                  context,
                  new MaterialPageRoute(
                    builder: (context) => new Home(showWelcome: true, showUpdateProfile: _sessionManager.getSessionDetails()['has_updated_profile'],firstname: firstname),
                  )
                );
  }

  void _signUp(BuildContext context, String name, String email, String password) {
    //print('signUp called...');
    widget.auth.signUp(name, email, password).then((retval){
      if(retval != null){
        //print(retval);
        if(retval is Exception){
          Exception e = new Exception(retval);
          //print(e.toString());
          if(e.toString().contains("network error")){
            Navigator.pop(context);
            return showError(context, "Please check your internet connection");
          }
        }
        else if(retval is String){
          if(retval == "You are already registered"){
            Navigator.pop(context);
            return showError(context, "You are already registered");
          }
        }
        else if(retval is DatabaseReference){
          DatabaseReference databaseReference = retval;
          //print("success");
          databaseReference.once().then((DataSnapshot snapshot){
            Map<dynamic, dynamic> values=snapshot.value;
              values.forEach((key,values) {
                //print(values);
                if(values['email'] == email){
                  _sessionManager.createLoginSession(values['avatar'], values['name'], values['email'], values['user_id'], values['phone'], values['desc'], values['city'], values['country'], values['age'], values['business_connects'], values['session'], true, false);
                  return goHome(values['name'].toString().split(' ')[0]);
                }
              });
          });
          //Navigator.pop(context);
          //return goHome();
        }
      }
      Navigator.pop(context);
    });
  }
}