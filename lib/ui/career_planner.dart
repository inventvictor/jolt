import 'package:flutter/material.dart';
import 'package:jolt/views/common_scaffold.dart';
import 'package:font_awesome_flutter/font_awesome_flutter.dart';
import 'package:jolt/utils/uidata.dart';
import 'pdf_screen.dart';
import 'package:jolt/views/document.dart';
import 'package:pdf/pdf.dart';
import 'package:fluttertoast/fluttertoast.dart';
import 'dart:async';
import 'package:shared_preferences/shared_preferences.dart';
import 'package:jolt/utils/career_session_manager.dart';
import 'dart:io';
import 'package:jolt/ui/career_planner_listview.dart';
import 'package:firebase_database/firebase_database.dart';

class CareerPlannerPage extends StatefulWidget{

  CareerPlannerPage({this.showDialog_});
  bool showDialog_ = true;
  

  State<StatefulWidget> createState(){
    return _CareerPlannerPageState();
  }
}

class _CareerPlannerPageState extends State<CareerPlannerPage>{
  Size deviceSize;
  var wwdiwtbTextField = new TextEditingController();
  var wdintbmisTextField = new TextEditingController();
  var wdintmmssTextField = new TextEditingController();
  var twoYrTextField = new TextEditingController();
  var fiveYrTextField = new TextEditingController();
  var tenYrTextField = new TextEditingController();
  var wsdihnTextField = new TextEditingController();
  var cptTextField = new TextEditingController();
  var wsdinTextField = new TextEditingController();
  var odnTextField = new TextEditingController();

  Future pshowDialog(BuildContext context){
    return showDialog(
          context: context,
          builder: (BuildContext context){
            return SimpleDialog(
              semanticLabel: 'Instructions',
              contentPadding: EdgeInsets.symmetric(vertical: UIData.percentHeight(context, 30), horizontal: UIData.percentWidth(context, 30)),
              children: <Widget>[
                  new Text(
                    'Instructions',
                    style: TextStyle(
                      color: UIData.joltBlueAccent,
                      fontFamily: "WorkSansMedium",
                      fontSize: UIData.percentHeight(context, 20)
                    ),
                  ),
                  SizedBox(
                    height: UIData.percentHeight(context, 15),
                  ),
                  new Text(
                    'Ask yourself the following questions \r\n 1. Who am I or Who do I need to become? \r\n 2. What are my strengths and weaknesses \r\n 3. What are my values ?',
                    style: TextStyle(
                      color: Colors.black,
                      fontFamily: "WorkSansMedium",
                      fontSize: UIData.percentHeight(context, 18)
                    ),
                  ),
                  SizedBox(
                    height: UIData.percentHeight(context, 10)
                  ),
                  RaisedButton(
                        elevation: UIData.percentHeight(context, 10),
                        child: Text('Okay, Thanks',
                        style: TextStyle(
                          color: UIData.joltBlueAccent,
                          fontFamily: "WorkSansMedium",
                          fontSize: UIData.percentHeight(context, 18)
                        ),),
                        color: Colors.white,
                        onPressed: (){
                          Navigator.pop(context);
                        },
                      )
                ],
              
            );
          }
        );
  }

  Widget bodyData(BuildContext context){
    
    return new Scaffold(
      resizeToAvoidBottomPadding : false,
      backgroundColor: Colors.white,
      body: new Container(
        padding: EdgeInsets.symmetric(vertical: UIData.percentHeight(context, 20), horizontal: UIData.percentWidth(context, 20)),
          child: new Form(
            child: new ListView(
              children: <Widget>[
                  Align(
                    alignment: Alignment.centerLeft,
                    child: Container(
                      child: new Text(
                        "IDEAL SELF",
                        style: new TextStyle(
                          color: UIData.joltBlueAccent,
                          fontFamily: "WorkSansBold",
                          fontSize: UIData.percentHeight(context, 18)
                        ),
                      ),
                    ),
                  ),
                  new Padding(
                    padding: EdgeInsets.symmetric(vertical: UIData.percentHeight(context, 10), horizontal: UIData.percentWidth(context, 10)),
                  ),
                  new TextFormField(
                    decoration: InputDecoration(
                        border: new OutlineInputBorder(
                            borderSide:
                                new BorderSide(color: Colors.teal)),
                        labelText: "What/Who do I want to be",
                        contentPadding: EdgeInsets.only(
                            left: UIData.percentWidth(context, 16),
                            top: UIData.percentHeight(context, 20),
                            right: UIData.percentWidth(context, 16),
                            bottom: UIData.percentHeight(context, 5))),
                    controller: wwdiwtbTextField,
                    
                    style: TextStyle(
                      fontFamily: "WorkSansMedium",
                      fontSize: UIData.percentHeight(context, 18),
                      color: Colors.black,
                    ),
                    textCapitalization: TextCapitalization.sentences,
                    maxLines: null,
                    keyboardType: TextInputType.multiline,
                  ),
                  
                  SizedBox(
                    height: UIData.percentHeight(context, 30)
                  ),

                  Align(
                    alignment: Alignment.centerLeft,
                    child: Container(
                      child: new Text(
                        "IDEAL ATTRIBUTES",
                        style: new TextStyle(
                          color: UIData.joltBlueAccent,
                          fontFamily: "WorkSansBold",
                          fontSize: UIData.percentHeight(context, 18)
                        ),
                      ),
                    ),
                  ),
                  new Padding(
                    padding: EdgeInsets.symmetric(vertical: UIData.percentHeight(context, 10), horizontal: UIData.percentWidth(context, 10)),
                  ),
                  new TextFormField(
                    decoration: InputDecoration(
                        border: new OutlineInputBorder(
                            borderSide:
                                new BorderSide(color: Colors.teal)),
                        labelText: "What do I need to be my Ideal Self",
                        contentPadding: EdgeInsets.only(
                            left: UIData.percentWidth(context, 16),
                            top: UIData.percentHeight(context, 20),
                            right: UIData.percentWidth(context, 16),
                            bottom: UIData.percentHeight(context, 5))),
                    controller: wdintbmisTextField,
                    
                    style: TextStyle(
                      fontFamily: "WorkSansMedium",
                      fontSize: UIData.percentHeight(context, 18),
                      color: Colors.black,
                    ),
                    textCapitalization: TextCapitalization.sentences,
                    maxLines: null,
                    keyboardType: TextInputType.multiline,
                  ),

                  SizedBox(
                    height: UIData.percentHeight(context, 30)
                  ),
                  Align(
                    alignment: Alignment.centerLeft,
                    child: Container(
                      child: new Text(
                        "TRAINING NEEDS",
                        style: new TextStyle(
                          color: UIData.joltBlueAccent,
                          fontFamily: "WorkSansBold",
                          fontSize: UIData.percentHeight(context, 18)
                        ),
                      ),
                    ),
                  ),
                  new Padding(
                    padding: EdgeInsets.symmetric(vertical: UIData.percentHeight(context, 10), horizontal: UIData.percentWidth(context, 10)),
                  ),
                  new TextFormField(
                    decoration: InputDecoration(
                        border: new OutlineInputBorder(
                            borderSide:
                                new BorderSide(color: Colors.teal)),
                        labelText: "What do I need to maximize my skills",
                        contentPadding: EdgeInsets.only(
                            left: UIData.percentWidth(context, 16),
                            top: UIData.percentHeight(context, 20),
                            right: UIData.percentWidth(context, 16),
                            bottom: UIData.percentHeight(context, 5))),
                    controller: wdintmmssTextField,
                    
                    style: TextStyle(
                      fontFamily: "WorkSansMedium",
                      fontSize: UIData.percentHeight(context, 18),
                      color: Colors.black,
                    ),
                    textCapitalization: TextCapitalization.sentences,
                    maxLines: null,
                    keyboardType: TextInputType.multiline,
                  ),
                  SizedBox(
                    height: UIData.percentHeight(context, 25),
                  ),
                  Align(
                    alignment: Alignment.centerLeft,
                    child: Container(
                      child: new Text(
                        "Specific Career goals/Ideal goals".toUpperCase(),
                        style: new TextStyle(
                          color: UIData.joltBlueAccent,
                          fontFamily: "WorkSansBold",
                          fontSize: UIData.percentHeight(context, 18)
                        ),
                      ),
                    ),
                  ),
                  new Padding(
                    padding: EdgeInsets.symmetric(vertical: UIData.percentHeight(context, 10), horizontal: UIData.percentWidth(context, 10)),
                  ),
                  new TextFormField(
                    decoration: InputDecoration(
                        border: new OutlineInputBorder(
                            borderSide:
                                new BorderSide(color: Colors.teal)),
                        labelText: "2 years",
                        contentPadding: EdgeInsets.only(
                            left: UIData.percentWidth(context, 16),
                            top: UIData.percentHeight(context, 20),
                            right: UIData.percentWidth(context, 16),
                            bottom: UIData.percentHeight(context, 5))),
                    controller: twoYrTextField,
                    
                    style: TextStyle(
                      fontFamily: "WorkSansSemiBold",
                      fontSize: UIData.percentHeight(context, 18),
                      color: Colors.black,
                    ),
                    textCapitalization: TextCapitalization.sentences,
                    maxLines: null,
                    keyboardType: TextInputType.multiline,
                  ),
                  SizedBox(
                    height: UIData.percentHeight(context, 15),
                  ),
                  new Padding(
                    padding: EdgeInsets.symmetric(vertical: UIData.percentHeight(context, 10), horizontal: UIData.percentWidth(context, 10)),
                  ),
                  new TextFormField(
                    decoration: InputDecoration(
                        border: new OutlineInputBorder(
                            borderSide:
                                new BorderSide(color: Colors.teal)),
                        labelText: "5 years",
                        contentPadding: EdgeInsets.only(
                            left: UIData.percentWidth(context, 16),
                            top: UIData.percentHeight(context, 20),
                            right: UIData.percentWidth(context, 16),
                            bottom: UIData.percentHeight(context, 5))),
                    controller: fiveYrTextField,
                    
                    style: TextStyle(
                      fontFamily: "WorkSansSemiBold",
                      fontSize: UIData.percentHeight(context, 18),
                      color: Colors.black,
                    ),
                    textCapitalization: TextCapitalization.sentences,
                    maxLines: null,
                    keyboardType: TextInputType.multiline,
                  ),
                  SizedBox(
                    height: UIData.percentHeight(context, 15)
                  ),
                  new Padding(
                    padding: EdgeInsets.symmetric(vertical: UIData.percentHeight(context, 10), horizontal: UIData.percentWidth(context, 10)),
                  ),
                  new TextFormField(
                    decoration: InputDecoration(
                        border: new OutlineInputBorder(
                            borderSide:
                                new BorderSide(color: Colors.teal)),
                        labelText: "10 years",
                        contentPadding: EdgeInsets.only(
                            left: UIData.percentWidth(context, 16),
                            top: UIData.percentHeight(context, 20),
                            right: UIData.percentWidth(context, 16),
                            bottom: UIData.percentHeight(context, 5))),
                    controller: tenYrTextField,
                    
                    style: TextStyle(
                      fontFamily: "WorkSansSemiBold",
                      fontSize: UIData.percentHeight(context, 18),
                      color: Colors.black,
                    ),
                    textCapitalization: TextCapitalization.sentences,
                    maxLines: null,
                    keyboardType: TextInputType.multiline,
                  ),
                  SizedBox(
                    height: UIData.percentHeight(context, 30),
                  ),
                  Align(
                    alignment: Alignment.centerLeft,
                    child: Container(
                      child: new Text(
                        "IDEAL STRENGTHS",
                        style: new TextStyle(
                          color: UIData.joltBlueAccent,
                          fontFamily: "WorkSansBold",
                          fontSize: UIData.percentHeight(context, 18)
                        ),
                      ),
                    ),
                  ),
                  new Padding(
                    padding: EdgeInsets.symmetric(vertical: UIData.percentHeight(context, 10), horizontal: UIData.percentWidth(context, 10)),
                  ),
                  new TextFormField(
                    decoration: InputDecoration(
                        border: new OutlineInputBorder(
                            borderSide:
                                new BorderSide(color: Colors.teal)),
                        labelText: "What strength do I have now",
                        contentPadding: EdgeInsets.only(
                            left: UIData.percentWidth(context, 16),
                            top: UIData.percentHeight(context, 20),
                            right: UIData.percentWidth(context, 16),
                            bottom: UIData.percentHeight(context, 5))),
                    controller: wsdihnTextField,
                    
                    style: TextStyle(
                      fontFamily: "WorkSansMedium",
                      fontSize: UIData.percentHeight(context, 18),
                      color: Colors.black,
                    ),
                    textCapitalization: TextCapitalization.sentences,
                    maxLines: null,
                    keyboardType: TextInputType.multiline,
                  ),

                  SizedBox(
                    height: UIData.percentHeight(context, 30)
                  ),
                  Align(
                    alignment: Alignment.centerLeft,
                    child: Container(
                      child: new Text(
                        "CAREER PATH/TRACK",
                        style: new TextStyle(
                          color: UIData.joltBlueAccent,
                          fontFamily: "WorkSansBold",
                          fontSize: UIData.percentHeight(context, 18)
                        ),
                      ),
                    ),
                  ),
                  new Padding(
                    padding: EdgeInsets.symmetric(vertical: UIData.percentHeight(context, 10), horizontal: UIData.percentWidth(context, 10)),
                  ),
                  new TextFormField(
                    decoration: InputDecoration(
                        border: new OutlineInputBorder(
                            borderSide:
                                new BorderSide(color: Colors.teal)),
                        contentPadding: EdgeInsets.only(
                            left: UIData.percentWidth(context, 16),
                            top: UIData.percentHeight(context, 20),
                            right: UIData.percentWidth(context, 16),
                            bottom: UIData.percentHeight(context, 5))),
                    controller: cptTextField,
                    
                    style: TextStyle(
                      fontFamily: "WorkSansMedium",
                      fontSize: UIData.percentHeight(context, 18),
                      color: Colors.black,
                    ),
                    textCapitalization: TextCapitalization.sentences,
                    maxLines: null,
                    keyboardType: TextInputType.multiline,
                  ),
                  SizedBox(
                    height: UIData.percentHeight(context, 30)
                  ),
                  Align(
                    alignment: Alignment.centerLeft,
                    child: Container(
                      child: new Text(
                        "DEVELOPMENT OPPORTUNITIES",
                        style: new TextStyle(
                          color: UIData.joltBlueAccent,
                          fontFamily: "WorkSansBold",
                          fontSize: UIData.percentHeight(context, 18)
                        ),
                      ),
                    ),
                  ),
                  new Padding(
                    padding: EdgeInsets.symmetric(vertical: UIData.percentHeight(context, 10), horizontal: UIData.percentWidth(context, 10)),
                  ),
                  new TextFormField(
                    decoration: InputDecoration(
                        border: new OutlineInputBorder(
                            borderSide:
                                new BorderSide(color: Colors.teal)),
                        labelText: "What strength do I need",
                        contentPadding: EdgeInsets.only(
                            left: UIData.percentWidth(context, 16),
                            top: UIData.percentHeight(context, 20),
                            right: UIData.percentWidth(context, 16),
                            bottom: UIData.percentHeight(context, 5))),
                    controller: wsdinTextField,
                    
                    style: TextStyle(
                      fontFamily: "WorkSansMedium",
                      fontSize: UIData.percentHeight(context, 18),
                      color: Colors.black,
                    ),
                    textCapitalization: TextCapitalization.sentences,
                    maxLines: null,
                    keyboardType: TextInputType.multiline,
                  ),
                  SizedBox(
                    height: UIData.percentHeight(context, 30),
                  ),
                  Align(
                    alignment: Alignment.centerLeft,
                    child: Container(
                      child: new Text(
                        "OTHER DEVELOPMENTAL NEEDS",
                        style: new TextStyle(
                          color: UIData.joltBlueAccent,
                          fontFamily: "WorkSansBold",
                          fontSize: UIData.percentHeight(context, 18)
                        ),
                      ),
                    ),
                  ),
                  new Padding(
                    padding: EdgeInsets.symmetric(vertical: UIData.percentHeight(context, 10), horizontal: UIData.percentWidth(context, 10)),
                  ),
                  new TextFormField(
                    decoration: InputDecoration(
                        border: new OutlineInputBorder(
                            borderSide:
                                new BorderSide(color: Colors.teal)),
                        contentPadding: EdgeInsets.only(
                            left: UIData.percentWidth(context, 16),
                            top: UIData.percentHeight(context, 20),
                            right: UIData.percentWidth(context, 16),
                            bottom: UIData.percentHeight(context, 5))),
                    controller: odnTextField,
                    
                    style: TextStyle(
                      fontFamily: "WorkSansMedium",
                      fontSize: UIData.percentHeight(context, 18),
                      color: Colors.black,
                    ),
                    textCapitalization: TextCapitalization.sentences,
                    maxLines: null,
                    keyboardType: TextInputType.multiline,
                  ),
                  SizedBox(
                    height: UIData.percentHeight(context, 20),
                  ),
                  RaisedButton(
                        elevation: UIData.percentHeight(context, 1),
                        child: Text('DONE',
                          style: TextStyle(
                            color: Colors.white,
                            fontWeight: FontWeight.bold,
                            fontFamily: "WorkSansMedium",
                            fontSize: UIData.percentHeight(context, 18)
                          ),
                        ),
                        color: UIData.joltBlueAccent,
                        onPressed: () async {
                          if(isValidwwdiwtb(wwdiwtbTextField.text) && isValidcpt(cptTextField.text) && isValidfiveYr(fiveYrTextField.text) && isValidodn(odnTextField.text)
                          && isValidtenYr(tenYrTextField.text) && isValidtwoYr(twoYrTextField.text) && isValidwdintbmis(wdintbmisTextField.text) && isValidwdintmmss(wdintmmssTextField.text)
                          && isValidwsdihn(wsdihnTextField.text) && isValidwsdin(wsdinTextField.text)){
                            try {
                              final result = await InternetAddress.lookup('google.com');
                              if (result.isNotEmpty && result[0].rawAddress.isNotEmpty) {
                                  //print('create pdf');
                                  await FirebaseDatabase.instance.reference().child("activities").child(_sessionManager.getSessionDetails()['user_id']).push().child(new DateTime.now().millisecondsSinceEpoch.toString()).set("careerplan_update");
                                  generateDocument(context, PdfPageFormat.a4, _sessionManager.getSessionDetails()['fullname'], _sessionManager.getSessionDetails()['description'], wwdiwtbTextField.text, wdintbmisTextField.text, wdintmmssTextField.text, twoYrTextField.text, fiveYrTextField.text, tenYrTextField.text, wsdihnTextField.text, cptTextField.text, wsdinTextField.text, odnTextField.text).then((retval){
                                      Navigator.push(
                                      context,
                                      MaterialPageRoute(builder: (context) => PDFScreen(pathPDF: retval)
                                      ),
                                    );  
                                  });
                              }
                            } on SocketException catch (_) {
                                //print('not connected');
                                generateDocument(context, PdfPageFormat.a4, _sessionManager.getSessionDetails()['fullname'], _sessionManager.getSessionDetails()['description'], wwdiwtbTextField.text, wdintbmisTextField.text, wdintmmssTextField.text, twoYrTextField.text, fiveYrTextField.text, tenYrTextField.text, wsdihnTextField.text, cptTextField.text, wsdinTextField.text, odnTextField.text).then((retval){
                                      Navigator.push(
                                      context,
                                      MaterialPageRoute(builder: (context) => PDFScreen(pathPDF: retval)
                                      ),
                                    );  
                                  });
                            }
                            
                          }
                          else{
                                Fluttertoast.showToast(
                                  msg: "Please fill in all details correctly",
                                  toastLength: Toast.LENGTH_SHORT,
                                  gravity: ToastGravity.CENTER,
                                  timeInSecForIos: 1,
                                  backgroundColor: Colors.red,
                                  textColor: Colors.white
                                );
                          }
                          
                        },
                      ),
                ],
              )
          ),
      )
      
    );
  }

  bool isValidwdintbmis(String text){
    bool b = false;
    if(text.length < 1){
      b = false;
    }
    else{
      b = true;
    }
    return  b;
  }

  bool isValidwwdiwtb(String text){
    bool b = false;
    if(text.length < 1){
      b = false;
    }
    else{
      b = true;
    }
    return  b;
  }
  bool isValidwdintmmss(String text){
    bool b = false;
    if(text.length < 1){
      b = false;
    }
    else{
      b = true;
    }
    return  b;
  }
  bool isValidtwoYr(String text){
    bool b = false;
    if(text.length < 1){
      b = false;
    }
    else{
      b = true;
    }
    return  b;
  }
  bool isValidfiveYr(String text){
    bool b = false;
    if(text.length < 1){
      b = false;
    }
    else{
      b = true;
    }
    return  b;
  }
  bool isValidtenYr(String text){
    bool b = false;
    if(text.length < 1){
      b = false;
    }
    else{
      b = true;
    }
    return  b;
  }
  bool isValidwsdihn(String text){
    bool b = false;
    if(text.length < 1){
      b = false;
    }
    else{
      b = true;
    }
    return  b;
  }
  bool isValidcpt(String text){
    bool b = false;
    if(text.length < 1){
      b = false;
    }
    else{
      b = true;
    }
    return  b;
  }
  bool isValidwsdin(String text){
    bool b = false;
    if(text.length < 1){
      b = false;
    }
    else{
      b = true;
    }
    return  b;
  }
  bool isValidodn(String text){
    bool b = false;
    if(text.length < 1){
      b = false;
    }
    else{
      b = true;
    }
    return  b;
  }
  
  SessionManager _sessionManager;
  SharedPreferences _prefs;

  Future<Object> initialisePrefs() async {
    _prefs = await SharedPreferences.getInstance();
    SessionManager sessionManager = new SessionManager(_prefs);
    return sessionManager;
  }
  @override
  void initState() {
    super.initState();
    initialisePrefs().then((sessionManager){
      setState(() {
            _sessionManager = sessionManager;
        });
    });
    if(widget.showDialog_){
      //print("showDialog_");
      new Future.delayed(new Duration(seconds:0), () {
        pshowDialog(context);
      });
    }
  }
  

  @override
  Widget build(BuildContext context) {
    deviceSize = MediaQuery.of(context).size;
    
    return CommonScaffold(
      appTitle: "Career Planner",
      backGroundColor: Colors.white,
      actionFirstIcon: FontAwesomeIcons.listUl,
      actionFirstIconFunc: (){
        Navigator.push(
              context,
              MaterialPageRoute(builder: (context) => CareerPlannerList()
              ),
            );  
      },
      bColor: UIData.joltBlueAccent,
      bodyData: bodyData(context),
    );
  }
}