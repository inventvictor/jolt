import 'package:flutter/material.dart';
import 'package:flutter_country_picker/flutter_country_picker.dart';
import 'package:jolt/utils/uidata.dart';
import 'view_photo.dart';
import 'package:jolt/utils/career_session_manager.dart';
import 'package:shared_preferences/shared_preferences.dart';
import 'package:fluttertoast/fluttertoast.dart';
import 'dart:io';
import 'package:firebase_database/firebase_database.dart';
import 'package:font_awesome_flutter/font_awesome_flutter.dart';

class EditProfilePage extends StatefulWidget {
  //EditProfilePage({Key key, @required this.careerP}) : super(key: key);
  EditProfilePage({Key key}) : super(key: key);

  @override
  State<StatefulWidget> createState() => new _EditProfilePageState();
  
}

// enum ImagePickState{
//     picked,
//     cropped
//   }
  
class _EditProfilePageState extends State<EditProfilePage> {
  var emailField = new TextEditingController();
  var nameField = new TextEditingController();
  var phoneField = new TextEditingController();
  var descField = new TextEditingController();
  var cityField = new TextEditingController();
  var countryField = new TextEditingController();
  var ageField = new TextEditingController();

  Country _selected;
  
  updateEmail(){
    setState(() {
          emailField.text = _sessionManager.getSessionDetails()['email'];
        });
  }

  updateName(){
    setState(() {
          nameField.text = _sessionManager.getSessionDetails()['fullname'];
        });
  }

  updateDesc(){
    setState(() {
          descField.text = _sessionManager.getSessionDetails()['description'];
        });
  }

  updatePhone(){
    setState(() {
          phoneField.text = _sessionManager.getSessionDetails()['phone'];
        });
  }

  updateCity(){
    setState(() {
          cityField.text = _sessionManager.getSessionDetails()['city'];
        });
  }

  updateCountry(){
    setState(() {
          countryField.text = _sessionManager.getSessionDetails()['country'];
        });
  }

  updateAge(){
    setState(() {
          ageField.text = _sessionManager.getSessionDetails()['age'];
        });
  }

  final GlobalKey<FormState> _formKey = new GlobalKey<FormState>();

  Widget editAvatar(BuildContext context) => new GestureDetector(
    child: Container(
        width: UIData.percentWidth(context, 120),
        height: UIData.percentHeight(context, 120),
        color: Colors.white,
        child: FittedBox(
          fit: BoxFit.contain,
          child: Column(
            mainAxisAlignment: MainAxisAlignment.spaceEvenly,
            children: <Widget>[
              Container(
                decoration: BoxDecoration(
                    borderRadius: BorderRadius.circular(50.0),
                    border: Border.all(width: 2.0, color: UIData.joltBlueAccent)),
                child: CircleAvatar(
                  radius: 40.0,
                  backgroundImage: NetworkImage(_sessionManager.getSessionDetails()['avatar'])
                  )
              ),
            ],
          ),
        ),
      ),
      onTap: (){
        //print('Avatar has been pressed');
        Navigator.push(
            context,
            new MaterialPageRoute(
              builder: (context) => new ViewPhotoPage(imageFilePath: _sessionManager.getSessionDetails()['avatar'],),
            ), //MaterialPageRoute
          );
      },
  );
  
  // void _showJOLTPermission(BuildContext context) {
  //   showDialog(
  //     context: context,
  //     builder: (BuildContext context){
  //       return AlertDialog(
  //         title: Text('JOLT Permissions'),
  //         content: Text('Please grant JOLT the necessary permissions to access photos, media and files on your device.'),
  //         actions: <Widget>[
  //           new FlatButton(
  //             child: new Text('GOT IT'),
  //             onPressed: (){
  //               Navigator.of(context).pop();
  //               _pickImage();
  //             },
  //           ),
  //         ],
  //       );
  //     }
  //   );
  // }

  SessionManager _sessionManager;
  SharedPreferences _prefs;

  Future<Object> initialisePrefs() async {
    _prefs = await SharedPreferences.getInstance();
    SessionManager sessionManager = new SessionManager(_prefs);
    return sessionManager;
  }
  @override
  void initState() {
    super.initState();
    initialisePrefs().then((sessionManager){
      if(sessionManager != null){
        setState(() {
            _sessionManager = sessionManager;
        });
        emailField.text = _sessionManager.getSessionDetails()['email'];
        nameField.text = _sessionManager.getSessionDetails()['fullname'];
        descField.text = _sessionManager.getSessionDetails()['description'];
        phoneField.text = _sessionManager.getSessionDetails()['phone'];
        cityField.text = _sessionManager.getSessionDetails()['city'];
        countryField.text = _sessionManager.getSessionDetails()['country'];
        ageField.text = _sessionManager.getSessionDetails()['age'];
      }
    });
  }

  @override
  Widget build(BuildContext context) {
    
    final Size screenSize = MediaQuery.of(context).size;
    try{
      if (_sessionManager.getSessionDetails() != null){
        return new Scaffold(
          appBar: new AppBar(
            title: new Text('Edit Profile', style: TextStyle(fontSize: UIData.percentHeight(context, 25)),),
            backgroundColor: UIData.joltBlueAccent,
          ),
          body: new Container(
            color: Colors.white,
            padding: EdgeInsets.symmetric(vertical: UIData.percentHeight(context, 20), horizontal: UIData.percentWidth(context, 20)),
            child: new Form(
              key: this._formKey,
              child: new ListView(
                children: <Widget>[
                  editAvatar(context),
                  SizedBox(
                    height: UIData.percentHeight(context, 10),
                  ),
                  new TextFormField(
                    controller: nameField,
                    keyboardType: TextInputType.text, // Use email input type for emails.
                    decoration: new InputDecoration(
                      hintText: 'e.g Bakare Ayomide',
                      labelText: 'Full Name',
                    )
                  ),
                  SizedBox(
                    height: UIData.percentHeight(context, 10),
                  ),
                  new FocusScope(
                    node: new FocusScopeNode(),
                    child: new TextFormField(
                      controller: emailField,
                      decoration: new InputDecoration(
                        hintText: 'you@example.com',
                        labelText: 'E-mail Address',
                      ),
                    ),
                  ),
                  // new TextFormField(
                  //   controller: emailField,
                  //   keyboardType: TextInputType.emailAddress, // Use email input type for emails.
                  //   decoration: new InputDecoration(
                  //     hintText: 'you@example.com',
                  //     labelText: 'E-mail Address',
                  //   )
                  // ),
                  SizedBox(
                    height: UIData.percentHeight(context, 10),
                  ),
                  new TextFormField(
                    controller: descField,
                    maxLength: 50,
                    keyboardType: TextInputType.text, // Use email input type for emails.
                    decoration: new InputDecoration(
                      hintText: 'e.g Software Developer',
                      labelText: 'Brief Description',
                    )
                  ),
                  SizedBox(
                    height: UIData.percentHeight(context, 10),
                  ),
                  new TextFormField(
                    controller: phoneField,
                    keyboardType: TextInputType.number, // Use email input type for emails.
                    decoration: new InputDecoration(
                      hintText: 'e.g 08184563756',
                      labelText: 'Phone Number',
                    )
                  ),
                  SizedBox(
                    height: UIData.percentHeight(context, 10),
                  ),
                  new TextFormField(
                    controller: cityField,
                    keyboardType: TextInputType.text, // Use email input type for emails.
                    decoration: new InputDecoration(
                      hintText: 'e.g Victoria Island, Lagos',
                      labelText: 'City',
                    )
                  ),
                  SizedBox(
                    height: UIData.percentHeight(context, 10),
                  ),
                  Row(
                    mainAxisAlignment: MainAxisAlignment.spaceBetween,
                    children: <Widget>[
                      Container(
                        width: UIData.percentWidth(context, 200),
                        child: TextFormField(
                          controller: countryField,
                          readOnly: true,
                          keyboardType: TextInputType.text, // Use email input type for emails.
                          decoration: new InputDecoration(
                            hintText: 'e.g Nigeria',
                            labelText: 'Country',
                          )
                        ),
                      ),
                      CountryPicker(
                        dense: false,
                        showFlag: true,  //displays flag, true by default
                        showDialingCode: false, //displays dialing code, false by default
                        showName: true, //displays country name, true by default
                        showCurrency: false, //eg. 'British pound'
                        showCurrencyISO: false,
                        onChanged: (Country country) {
                          setState(() {
                            _selected = country;
                            countryField.text = country.name;
                          });
                        },
                        selectedCountry: _selected,
                      )
                    ],
                  ),
                  SizedBox(
                    height: UIData.percentHeight(context, 10),
                  ),
                  new TextFormField(
                    controller: ageField,
                    keyboardType: TextInputType.number, // Use email input type for emails.
                    decoration: new InputDecoration(
                      hintText: 'e.g 34',
                      labelText: 'Age',
                    )
                  ),
                  SizedBox(
                    height: UIData.percentHeight(context, 10),
                  ),
                  // new TextFormField(
                  //   obscureText: true, // Use secure text for passwords.
                  //   decoration: new InputDecoration(
                  //     hintText: 'Password',
                  //     labelText: 'Enter your password'
                  //   )
                  // ),
                  // SizedBox(
                  //   height: 10.0,
                  // ),
                  new Container(
                    width: screenSize.width,
                    child: new RaisedButton(
                      child: new Text(
                        'Update'.toUpperCase(),
                        style: new TextStyle(
                          fontSize: UIData.percentHeight(context, 20),
                          color: Colors.white,
                          fontWeight: FontWeight.w700,
                          letterSpacing: 2.0
                        ),
                      ),
                      onPressed: (){
                        //print("update profile button pressed!");
                        if(!isValidEmail(emailField.text)){
                            Fluttertoast.showToast(
                                msg: "Please enter valid email",
                                toastLength: Toast.LENGTH_SHORT,
                                gravity: ToastGravity.CENTER,
                                timeInSecForIos: 1,
                                backgroundColor: Colors.red,
                                textColor: Colors.white,
                            );
                          }
                          else if(!isValidFullName(nameField.text)){
                            Fluttertoast.showToast(
                                msg: "Please enter valid fullname",
                                toastLength: Toast.LENGTH_SHORT,
                                gravity: ToastGravity.CENTER,
                                timeInSecForIos: 1,
                                backgroundColor: Colors.red,
                                textColor: Colors.white
                            );
                          }
                          else if(!isValidBriefDesc(descField.text)){
                            Fluttertoast.showToast(
                                msg: "Please enter valid description",
                                toastLength: Toast.LENGTH_SHORT,
                                gravity: ToastGravity.CENTER,
                                timeInSecForIos: 1,
                                backgroundColor: Colors.red,
                                textColor: Colors.white
                            );
                          }
                          else if(!isValidPhone(phoneField.text)){
                            Fluttertoast.showToast(
                                msg: "Please enter valid phone number",
                                toastLength: Toast.LENGTH_SHORT,
                                gravity: ToastGravity.CENTER,
                                timeInSecForIos: 1,
                                backgroundColor: Colors.red,
                                textColor: Colors.white
                            );
                          }
                          else if(!isValidCity(cityField.text)){
                            Fluttertoast.showToast(
                                msg: "Please enter valid city",
                                toastLength: Toast.LENGTH_SHORT,
                                gravity: ToastGravity.CENTER,
                                timeInSecForIos: 1,
                                backgroundColor: Colors.red,
                                textColor: Colors.white
                            );
                          }
                          // else if(!isValidCountry(countryField.text)){
                          //   Fluttertoast.showToast(
                          //       msg: "Please ennter valid country",
                          //       toastLength: Toast.LENGTH_SHORT,
                          //       gravity: ToastGravity.CENTER,
                          //       timeInSecForIos: 1,
                          //       backgroundColor: Colors.red,
                          //       textColor: Colors.white
                          //   );
                          // }
                          else if(!isValidAge(ageField.text)){
                            Fluttertoast.showToast(
                                msg: "Please enter valid age",
                                toastLength: Toast.LENGTH_SHORT,
                                gravity: ToastGravity.CENTER,
                                timeInSecForIos: 1,
                                backgroundColor: Colors.red,
                                textColor: Colors.white
                            );
                          }
                          else{
                            updateProfile(nameField.text, emailField.text, phoneField.text, descField.text, cityField.text, countryField.text, ageField.text);
                          }
                      },
                      color: UIData.joltBlue,
                    ),
                    margin: new EdgeInsets.only(
                      top: 20.0
                    ),
                  )
                ],
              ),
            )
          ),
        );
      }
      else{
        return Container(
          child: Padding(
              padding: EdgeInsets.symmetric(vertical: UIData.percentHeight(context, 5), horizontal: UIData.percentWidth(context, 5)),
              child: Center(
                child: CircularProgressIndicator()
              ),
            )
          );
      }
    }
    catch(e){
      return Container(
          child: Padding(
              padding: EdgeInsets.symmetric(vertical: UIData.percentHeight(context, 5), horizontal: UIData.percentWidth(context, 5)),
              child: Center(
                child: CircularProgressIndicator()
              ),
            )
          );
    }
  }

  Future<Null> updateProfile(String name, String email, String phone, String desc, String city, String country, String age) async{
    try {
        final result = await InternetAddress.lookup('google.com');
        if (result.isNotEmpty && result[0].rawAddress.isNotEmpty) {
            showProgress(context);
            await FirebaseDatabase.instance.reference().child("users").child(_sessionManager.getSessionDetails()['user_id']).set({
              'name': name,
              'email': email,
              'avatar': _sessionManager.getSessionDetails()['avatar'],
              'phone': phone,
              'user_id': _sessionManager.getSessionDetails()['user_id'],
              'desc': desc,
              'city': city,
              'country': country,
              'age': age,
              'business_connects': _sessionManager.getSessionDetails()['business_connects'],
              'session': _sessionManager.getSessionDetails()['session'],
              'has_updated_profile':true
            });
            await FirebaseDatabase.instance.reference().child("activities").child(_sessionManager.getSessionDetails()['user_id']).push().child(
                      new DateTime.now().millisecondsSinceEpoch.toString()).set("profile_update");
            await _sessionManager.setUpdatedProfile(_prefs, true);
            await _sessionManager.setFullname(_prefs, name);
            await _sessionManager.setEmail(_prefs, email);
            await _sessionManager.setPhone(_prefs, phone);
            await _sessionManager.setDesc(_prefs, desc);
            await _sessionManager.setCity(_prefs, city);
            await _sessionManager.setCountry(_prefs, country);
            await _sessionManager.setAge(_prefs, age);
            Navigator.pop(context);
            showSuccess(context, "Updated Successfully");
        }
      } on SocketException catch (_) {
          //print('not connected');
          showError(context, 'Something went wrong. Check network!');
      }
  }

  Future<Null> showProgress(BuildContext context){
      return showDialog(
                context: context,
                barrierDismissible: false,
                builder: (BuildContext context){
                  return new WillPopScope(
                    onWillPop: (){
                      Future.value(false);
                    },
                    child: SimpleDialog(
                      contentPadding: EdgeInsets.all(0.0),
                      children: <Widget>[
                        new Container(
                          height: UIData.percentHeight(context, 75),
                          decoration: new BoxDecoration(
                            color: UIData.joltBlueAccent,
                          ),
                          child: new Center(
                            child: Icon(
                              FontAwesomeIcons.solidUser,
                              color: Colors.white,
                              size: UIData.percentHeight(context, 20),
                            ),
                          )),
                        new SizedBox(
                          height: UIData.percentHeight(context, 20),
                        ),
                        new Center(
                          child: CircularProgressIndicator(),
                        ),
                        new SizedBox(
                          height: UIData.percentHeight(context, 20),
                        ),
                        new Center(
                          child: new Text("Updating, please wait...",
                            style: TextStyle(
                              fontSize: UIData.percentHeight(context, 20),
                              fontFamily: "WorkSansSemiBold"
                            ),
                          ),
                        ),
                        new SizedBox(
                          height: UIData.percentHeight(context, 20), 
                        ),
                      ],
                    ),
                  ); 
                }
              );
      }

  Future<Null> showSuccess(BuildContext context, String msg){
    return showDialog(
      context: context,
      barrierDismissible: true,
      builder: (BuildContext context){
        return SimpleDialog(
            contentPadding: EdgeInsets.all(0.0),
            children: <Widget>[
              new Container(
                height: UIData.percentHeight(context, 75),
                decoration: new BoxDecoration(
                  color: Colors.greenAccent,
                ),
                child: new Center(
                  child: Icon(
                    FontAwesomeIcons.userCheck,
                    color: Colors.white,
                  ),
                )),
              new SizedBox(
                height: UIData.percentHeight(context, 20),
              ),
              new Container(
                padding: EdgeInsets.symmetric(vertical: UIData.percentHeight(context, 12), horizontal: UIData.percentWidth(context, 12)),
                child: Column(
                  children: <Widget>[
                    new Center(
                        child: new Text(msg,
                          style: TextStyle(
                            fontSize: UIData.percentHeight(context, 20),
                            fontFamily: "WorkSansSemiBold"
                          ),
                          textAlign: TextAlign.center,
                        ),
                      ),
                    new SizedBox(
                      height: UIData.percentHeight(context, 20),
                    ),
                    new Padding(
                      padding: EdgeInsets.symmetric(vertical: UIData.percentHeight(context, 10), horizontal: UIData.percentWidth(context, 10)),
                    )
                      ],
                    ),
              ),
            ],
        ); 
      }
    ); 
  }

  Future<Null> showError(BuildContext context, String msg){
    return showDialog(
      context: context,
      barrierDismissible: true,
      builder: (BuildContext context){
        return SimpleDialog(
            contentPadding: EdgeInsets.all(0.0),
            children: <Widget>[
              new Container(
                height: UIData.percentHeight(context, 75),
                decoration: new BoxDecoration(
                  color: Colors.redAccent,
                ),
                child: new Center(
                  child: Icon(
                    FontAwesomeIcons.exclamationCircle,
                    color: Colors.white,
                    size: UIData.percentHeight(context, 20),
                  ),
                )),
              new SizedBox(
                height: UIData.percentHeight(context, 20),
              ),
              new Container(
                padding: EdgeInsets.symmetric(vertical: UIData.percentHeight(context, 12), horizontal: UIData.percentWidth(context, 12)),
                child: Column(
                  children: <Widget>[
                    new Center(
                        child: new Text(msg,
                          style: TextStyle(
                            fontSize: UIData.percentHeight(context, 20),
                            fontFamily: "WorkSansSemiBold"
                          ),
                          textAlign: TextAlign.center,
                        ),
                      ),
                    new SizedBox(
                      height: UIData.percentHeight(context, 20),
                    ),
                    new Padding(
                      padding: EdgeInsets.symmetric(vertical: UIData.percentHeight(context, 10), horizontal: UIData.percentWidth(context, 10)),
                    )
                      ],
                    ),
              ),
            ],
        ); 
      }
    ); 
  }

  bool isValidEmail(String em) {
    String p = r'^(([^<>()[\]\\.,;:\s@\"]+(\.[^<>()[\]\\.,;:\s@\"]+)*)|(\".+\"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$';
    RegExp regExp = new RegExp(p);
    return regExp.hasMatch(em);
  }

  bool isValidFullName(String name){
    bool b = false;
    if(name.length < 1){
      b = false;
    }
    else{
      b = true;
    }
    return  b;
  }

  bool isValidBriefDesc(String desc){
    bool b = false;
    if(desc.length < 1 && desc.length > 100){
      b = false;
    }
    else{
      b = true;
    }
    return  b;
  }

  bool isValidPhone(String phone){
    bool b = false;
    if(phone.length < 1){
      b = false;
    }
    else{
      b = true;
    }
    return  b;
  }

  bool isValidCity(String city){
    bool b = false;
    if(city.length < 1){
      b = false;
    }
    else{
      b = true;
    }
    return  b;
  }

  bool isValidCountry(String country){
    bool b = false;
    if(country.length < 1){
      b = false;
    }
    else{
      b = true;
    }
    return  b;
  }

  bool isValidAge(String age){
    bool b = false;
    if(age.length < 1){
      b = false;
    }
    else{
      b = true;
    }
    return  b;
  }
}