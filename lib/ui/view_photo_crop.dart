
import 'dart:io';
import 'dart:ui' as ui;
import 'package:firebase_database/firebase_database.dart';
import 'package:firebase_storage/firebase_storage.dart';
import 'package:font_awesome_flutter/font_awesome_flutter.dart';
import 'package:image_gallery_saver/image_gallery_saver.dart';
import 'package:intl/intl.dart';
import 'package:jolt/utils/career_session_manager.dart';
import 'package:jolt/utils/centered_slider_track_shape.dart';
import 'package:flutter/material.dart';
import 'package:crop/crop.dart';
import 'package:jolt/utils/uidata.dart';
import 'package:path_provider/path_provider.dart';
import 'package:permission_handler/permission_handler.dart';
import 'package:shared_preferences/shared_preferences.dart';

class ViewPhotoCropPage extends StatefulWidget {

  File imageFile;
  ViewPhotoCropPage({Key key, @required this.imageFile}) : super(key: key);

  @override
  _ViewPhotoCropPageState createState() => _ViewPhotoCropPageState();
}

class _ViewPhotoCropPageState extends State<ViewPhotoCropPage> {
  final controller = CropController(aspectRatio: 1000 / 667.0);
  double _rotation = 0;
  CropShape shape = CropShape.box;

  void _cropImage() async {
    final pixelRatio = MediaQuery.of(context).devicePixelRatio;
    final cropped = await controller.crop(pixelRatio: pixelRatio);

    Navigator.of(context).push(
      MaterialPageRoute(
        builder: (context) => Scaffold(
          appBar: AppBar(
            title: Text('Crop Result', style: TextStyle(
                            fontFamily: "WorkSansSemiBold"
                          )),
            centerTitle: true,
            actions: [
              Builder(
                builder: (context) => IconButton(
                  icon: Icon(FontAwesomeIcons.upload),
                  onPressed: () async {
                    if (cropped != null) {
                      print('victor');
                      var croppedImagePath = await _saveImg(cropped);
                      try {
                        final result = await InternetAddress.lookup('google.com');
                        if (result.isNotEmpty && result[0].rawAddress.isNotEmpty) {
                            //upload to firebase storage here
                            DateTime now = new DateTime.now();
                            var datestamp = new DateFormat("yyyyMMdd'T'HHmmss");
                            String formatted = datestamp.format(now);
                            final StorageReference firebaseStorageRef = FirebaseStorage.instance.ref().child(_sessionManager.getSessionDetails()['user_id']+"_"+formatted);
                            final StorageUploadTask task = firebaseStorageRef.putFile(File(croppedImagePath));
                            //progress dialog should sit here
                            if(task.isInProgress){
                              showProgress(context);
                            }
                            var dowurl = await (await task.onComplete).ref.getDownloadURL();
                            Navigator.pop(context);
                            Scaffold.of(context).showSnackBar(
                              SnackBar(
                                content: Text('Uploaded Successfully.'),
                              ),
                            );
                            // setState(() {
                            //         widget.imageFilePath = dowurl.toString();
                            //   });
                            await _sessionManager.setAvatar(_prefs, dowurl.toString());
                            await FirebaseDatabase.instance.reference().child("users").child(_sessionManager.getSessionDetails()['user_id']).set({
                              'name': _sessionManager.getSessionDetails()['fullname'],
                              'email': _sessionManager.getSessionDetails()['email'],
                              'avatar': dowurl.toString(),
                              'phone': _sessionManager.getSessionDetails()['phone'],
                              'user_id': _sessionManager.getSessionDetails()['user_id'],
                              'desc': _sessionManager.getSessionDetails()['description'],
                              'city': _sessionManager.getSessionDetails()['city'],
                              'country': _sessionManager.getSessionDetails()['country'],
                              'age': _sessionManager.getSessionDetails()['age'],
                              'business_connects': _sessionManager.getSessionDetails()['business_connects'],
                              'session': _sessionManager.getSessionDetails()['session'],
                              'has_updated_profile':_sessionManager.getSessionDetails()['has_updated_profile']
                            });
                            await FirebaseDatabase.instance.reference().child("activities").child(_sessionManager.getSessionDetails()['user_id']).push().child(
                                    new DateTime.now().millisecondsSinceEpoch.toString()).set("profilephoto_update");
                            Navigator.pop(context);
                        }
                      } on SocketException catch (_) {
                          //print('not connected');
                          showError(context, 'Something went wrong. Check network!');
                      }
                      
                    }
                    // final status = await Permission.storage.request();
                    // if (status == PermissionStatus.granted) {
                    //   //await _saveScreenShot(cropped);
                    //   Scaffold.of(context).showSnackBar(
                    //     SnackBar(
                    //       content: Text('Saved to gallery.'),
                    //     ),
                    //   );
                    // }
                  },
                ),
              ),
            ],
          ),
          body: Center(
            child: RawImage(
              image: cropped,
            ),
          ),
        ),
        fullscreenDialog: true,
      ),
    );
  }

  Future<Null> showError(BuildContext context, String msg){
    return showDialog(
      context: context,
      barrierDismissible: true,
      builder: (BuildContext context){
        return SimpleDialog(
            contentPadding: EdgeInsets.all(0.0),
            children: <Widget>[
              new Container(
                height: UIData.percentHeight(context, 75),
                decoration: new BoxDecoration(
                  color: Colors.redAccent,
                ),
                child: new Center(
                  child: Icon(
                    FontAwesomeIcons.exclamationCircle,
                    color: Colors.white,
                  ),
                )),
              new SizedBox(
                height: UIData.percentHeight(context, 20),
              ),
              new Container(
                padding: EdgeInsets.symmetric(vertical: UIData.percentHeight(context, 12), horizontal: UIData.percentWidth(context, 12)),
                child: Column(
                  children: <Widget>[
                    new Center(
                        child: new Text(msg,
                          style: TextStyle(
                            fontSize: UIData.percentHeight(context, 20),
                            fontFamily: "WorkSansSemiBold"
                          ),
                          textAlign: TextAlign.center,
                        ),
                      ),
                    new SizedBox(
                      height: UIData.percentHeight(context, 20),
                    ),
                    new Padding(
                      padding: EdgeInsets.symmetric(vertical: UIData.percentHeight(context, 10), horizontal: UIData.percentWidth(context, 10)),
                    )
                      ],
                    ),
              ),
            ],
        ); 
      }
    ); 
  }

  Future<Null> showProgress(BuildContext context){
      return showDialog(
                context: context,
                barrierDismissible: false,
                builder: (BuildContext context){
                  return new WillPopScope(
                    onWillPop: (){
                      Future.value(false);
                    },
                    child: SimpleDialog(
                      contentPadding: EdgeInsets.all(0.0),
                      children: <Widget>[
                        new Container(
                          height: UIData.percentHeight(context, 75),
                          decoration: new BoxDecoration(
                            color: UIData.joltBlueAccent,
                          ),
                          child: new Center(
                            child: Icon(
                              FontAwesomeIcons.solidFileImage,
                              color: Colors.white,
                            ),
                          )),
                        new SizedBox(
                          height: UIData.percentHeight(context, 20),
                        ),
                        new Center(
                          child: CircularProgressIndicator(),
                        ),
                        new SizedBox(
                          height: UIData.percentHeight(context, 20),
                        ),
                        new Center(
                          child: new Text("Uploading, please wait...",
                            style: TextStyle(
                              fontSize: UIData.percentHeight(context, 20),
                              fontFamily: "WorkSansSemiBold"
                            ),
                          ),
                        ),
                        new SizedBox(
                          height: UIData.percentHeight(context, 20),
                        ),
                      ],
                    ),
                  ); 
                }
              );
    }

  
  SessionManager _sessionManager;
  SharedPreferences _prefs;

  Future<Object> initialisePrefs() async {
    _prefs = await SharedPreferences.getInstance();
    SessionManager sessionManager = new SessionManager(_prefs);
    return sessionManager;
  }

  @override
  void initState() {
    super.initState();
    initialisePrefs().then((sessionManager){
      setState(() {
            _sessionManager = sessionManager;
        });
    });
  }


  @override
  Widget build(BuildContext context) {
    final theme = Theme.of(context);
    return Scaffold(
      appBar: AppBar(
        title: Text('Crop Image', style: TextStyle(
                            fontFamily: "WorkSansSemiBold"
                          )),
        centerTitle: true,
        // leading: IconButton(
        //   icon: Icon(FontAwesome.github),
        //   onPressed: () {
        //     launch('https://github.com/xclud/flutter_crop');
        //   },
        // ),
        actions: <Widget>[
          IconButton(
            onPressed: _cropImage,
            tooltip: 'Crop',
            icon: Icon(Icons.crop),
          )
        ],
      ),
      body: Column(
        children: <Widget>[
          Expanded(
            child: Container(
              color: Colors.black,
              padding: EdgeInsets.all(8),
              child: Crop(
                controller: controller,
                shape: shape,
                child: Align(
                  widthFactor: 0.5,
                  child: Image.file(
                    widget.imageFile,
                    fit: BoxFit.cover,
                  ),
                ),
                helper: Container(
                  decoration: BoxDecoration(
                    border: Border.all(color: Colors.white, width: 2),
                  ),
                )
                // child: Image.file(
                //   widget.imageFile,
                //   fit: BoxFit.cover,
                // ), //It's very important to set fit: BoxFit.cover.
                // foreground: IgnorePointer(
                //   child: Container(
                //     alignment: Alignment.bottomRight,
                //     child: Text(
                //       'Foreground Object',
                //       style: TextStyle(color: Colors.red),
                //     ),
                //   ),
                // ),
                // helper: shape == CropShape.box
                //     ? Container(
                //         decoration: BoxDecoration(
                //           border: Border.all(color: Colors.white, width: 2),
                //         ),
                //       )
                //     : null,
              ),
            ),
          ),
          Row(
            children: <Widget>[
              IconButton(
                icon: Icon(Icons.undo),
                tooltip: 'Undo',
                onPressed: () {
                  controller.rotation = 0;
                  controller.scale = 1;
                  controller.offset = Offset.zero;
                  setState(() {
                    _rotation = 0;
                  });
                },
              ),
              Expanded(
                child: SliderTheme(
                  data: theme.sliderTheme.copyWith(
                    trackShape: CenteredRectangularSliderTrackShape(),
                  ),
                  child: Slider(
                    divisions: 361,
                    value: _rotation,
                    min: -180,
                    max: 180,
                    label: '$_rotation°',
                    onChanged: (n) {
                      setState(() {
                        _rotation = n.roundToDouble();
                        controller.rotation = _rotation;
                      });
                    },
                  ),
                ),
              ),
              PopupMenuButton<CropShape>(
                icon: Icon(Icons.crop_free),
                itemBuilder: (context) => [
                  PopupMenuItem(
                    child: Text("Box"),
                    value: CropShape.box,
                  ),
                  PopupMenuItem(
                    child: Text("Oval"),
                    value: CropShape.oval,
                  ),
                ],
                tooltip: 'Crop Shape',
                onSelected: (x) {
                  setState(() {
                    shape = x;
                  });
                },
              ),
              PopupMenuButton<double>(
                icon: Icon(Icons.aspect_ratio),
                itemBuilder: (context) => [
                  PopupMenuItem(
                    child: Text("Original"),
                    value: 1000 / 667.0,
                  ),
                  PopupMenuDivider(),
                  PopupMenuItem(
                    child: Text("16:9"),
                    value: 16.0 / 9.0,
                  ),
                  PopupMenuItem(
                    child: Text("4:3"),
                    value: 4.0 / 3.0,
                  ),
                  PopupMenuItem(
                    child: Text("1:1"),
                    value: 1,
                  ),
                  PopupMenuItem(
                    child: Text("3:4"),
                    value: 3.0 / 4.0,
                  ),
                  PopupMenuItem(
                    child: Text("9:16"),
                    value: 9.0 / 16.0,
                  ),
                ],
                tooltip: 'Aspect Ratio',
                onSelected: (x) {
                  controller.aspectRatio = x;
                  setState(() {});
                },
              ),
            ],
          ),
        ],
      ),
    );
  }
}

Future<dynamic> _saveImg(ui.Image img) async {
  var byteData = await img.toByteData(format: ui.ImageByteFormat.png);
  var buffer = byteData.buffer.asUint8List();
  //final result = await ImageGallerySaver.saveImage(buffer);
  //print(result);

  // return result;

  Directory tempDir = await getTemporaryDirectory();
  String tempPath = tempDir.path;
  var genfile = File("$tempPath" + "/jolt_cropped_" + new DateTime.now().millisecondsSinceEpoch.toString() + ".png");
  await genfile.writeAsBytes(buffer);
  print(genfile.path);
  return genfile.path;
}

