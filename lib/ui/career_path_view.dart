import 'package:flutter/material.dart';
import 'package:font_awesome_flutter/font_awesome_flutter.dart';
import 'package:jolt/models/career_path.dart';
import 'package:jolt/auth/jolt_auth.dart';
import 'package:jolt/utils/uidata.dart';
import 'package:url_launcher/url_launcher.dart';
import 'package:jolt/views/common_scaffold.dart';
import 'package:shared_preferences/shared_preferences.dart';
import 'package:jolt/utils/career_session_manager.dart';
import 'dart:async';
import 'package:flutter_html/flutter_html.dart';
import 'package:html/dom.dart' as dom;

class CareerPathViewPage extends StatefulWidget {
  final CareerPath careerPath;
  final AuthImpl auth;
  CareerPathViewPage({this.careerPath, this.auth});

  @override
  _CareerPathViewPageState createState() => new _CareerPathViewPageState();
}


class _CareerPathViewPageState extends State<CareerPathViewPage> {
  Size deviceSize;
  bool _showDialog = true;
  bool hasLikedPost = false;

  SessionManager _sessionManager;
  IconData iconData = FontAwesomeIcons.heart;

  Future<Object> initialisePrefs() async {
    SharedPreferences prefs = await SharedPreferences.getInstance();
    SessionManager sessionManager = new SessionManager(prefs);
    return sessionManager;
  }

  Widget header(BuildContext context) {
    try{
      return Container(
        width: double.infinity,
        color: UIData.joltBlueAccent,
        child: Padding(
          padding: EdgeInsets.symmetric(vertical: UIData.percentHeight(context, 10), horizontal: UIData.percentWidth(context, 10)),
          child: ListTile(
            leading: CircleAvatar(
              radius: 30.0,
              backgroundImage: NetworkImage(widget.careerPath.imgUrl)
            ),
            title: Padding(
              padding: EdgeInsets.symmetric( vertical: UIData.percentHeight(context, 30)),
              child: Text(widget.careerPath.title,
                textAlign: TextAlign.center,
                style: TextStyle(
                  fontSize: UIData.percentHeight(context, 20),
                  fontFamily: "WorkSansBold",
                  color: Colors.white
                ),
              ),
            ),
          )
        ),
      );
    }
    catch(e){
    }
    
  } 

  Widget bodyData(BuildContext context) => SingleChildScrollView(
        child: Column(
          children: <Widget>[
            header(context),
            SafeArea(
               child: Html(
                 data: widget.careerPath.content,
                 //Optional parameters:
                 padding: EdgeInsets.symmetric(horizontal: UIData.percentWidth(context, 10), vertical: UIData.percentHeight(context, 5)),
                 defaultTextStyle: TextStyle(
                   fontSize: UIData.percentHeight(context, 20),
                   fontFamily: "WorkSansMedium",
                 ),
                 useRichText: false,
                 linkStyle: const TextStyle(
                   color: UIData.joltBlueAccent,
                   decorationColor: UIData.joltBlueAccent,
                   decoration: TextDecoration.underline,
                 ),
                 onLinkTap: (url) {
                   //print("Opening $url...");
                   launch(url);
                 },
                 onImageTap: (src) {
                   //print(src);
                 },
                 //Must have useRichText set to false for this to work
                 customRender: (node, children) {
                   if (node is dom.Element) {
                     switch (node.localName) {
                       case "custom_tag":
                         return Column(children: children);
                     }
                   }
                   return null;
                 },
                 customTextAlign: (dom.Node node) {
                   if (node is dom.Element) {
                     switch (node.localName) {
                       case "p":
                         return TextAlign.justify;
                     }
                   }
                   return null;
                 },
                 customTextStyle: (dom.Node node, TextStyle baseStyle) {
                   if (node is dom.Element) {
                     switch (node.localName) {
                       case "p":
                         return baseStyle.merge(TextStyle(height: 2, fontSize: 20));
                     }
                   }
                   return baseStyle;
                 },
               ),
            ),
          ],
        ),
      );

  
  
  @override
  void initState() {
    super.initState();
    initialisePrefs().then((sessionManager){
      if(sessionManager != null){
        setState(() {
            _sessionManager = sessionManager;
        });
      }
    });
  }

  @override
  Widget build(BuildContext context) {
    deviceSize = MediaQuery.of(context).size;
    try{
      if (_sessionManager.getSessionDetails() != null){
        widget.auth.hasLikedPost(widget.careerPath.id, _sessionManager.getSessionDetails()['user_id']).then((retVal){
          if(retVal == 1){
            setState(() {
              hasLikedPost = true;
            });
          }
        });
        return CommonScaffold(
          appTitle: "Career Path",
          actionFirstIcon:  hasLikedPost ? FontAwesomeIcons.solidHeart : iconData,
          actionFirstIconFunc: (){
            if(hasLikedPost){
              hasLikedPost = false;
              widget.auth.disLikePost(widget.careerPath.id, _sessionManager.getSessionDetails()['user_id'], widget.careerPath.title).then((retVal){
                setState(() {
                  hasLikedPost = false;
                });
              });
            }
            else{
              iconData = FontAwesomeIcons.solidHeart;
              widget.auth.likePost(widget.careerPath.id, _sessionManager.getSessionDetails()['user_id'], widget.careerPath.title).then((retVal){
                setState(() {
                  iconData = FontAwesomeIcons.solidHeart;
                });
              });
            }
          },
          bColor: UIData.joltBlueAccent,
          bodyData: bodyData(context),
        );
      }
      else{
        return Container(
            child: Padding(
                padding: EdgeInsets.symmetric(vertical: UIData.percentHeight(context, 5), horizontal: UIData.percentWidth(context, 5)),
                child: Center(
                  child: CircularProgressIndicator()
                ),
              )
            );
      }
    }
    catch(e){
      return Container(
            child: Padding(
                padding: EdgeInsets.symmetric(vertical: UIData.percentHeight(context, 5), horizontal: UIData.percentWidth(context, 5)),
                child: Center(
                  child: CircularProgressIndicator()
                ),
              )
            );
    }
  }
}
