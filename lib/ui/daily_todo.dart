import 'dart:async';

import 'package:firebase_auth/firebase_auth.dart';
import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:font_awesome_flutter/font_awesome_flutter.dart';
import 'package:jolt/utils/uidata.dart';
import 'page_done.dart';
import 'page_settings.dart';
import 'page_task.dart';
import 'package:jolt/views/common_scaffold.dart';
import 'package:jolt/utils/career_session_manager.dart';
import 'package:shared_preferences/shared_preferences.dart';

final FirebaseAuth _auth = FirebaseAuth.instance;

FirebaseUser _currentUser;

Future<FirebaseUser> getCurrentUser() async {
  final user = await FirebaseAuth.instance.currentUser();
  return user;
}

class DailyTodoPage extends StatefulWidget {
  final FirebaseUser user;

  DailyTodoPage({Key key, this.user}) : super(key: key);


  @override
  State<StatefulWidget> createState() => _DailyTodoPageState();
}



class TaskistApp extends StatelessWidget {

  @override
  Widget build(BuildContext context) {
    return CommonScaffold(
      appTitle: "Daily Todo",
      actionFirstIcon: null,
      showImageNotIcon: true,
      bColor: UIData.joltBlueAccent,
      bodyData: DailyTodoPage(),
    );
  }
}

class _DailyTodoPageState extends State<DailyTodoPage>
    with SingleTickerProviderStateMixin {
  int _currentIndex = 1;

  List<Widget> _children = new List<Widget>();

  @override
  Widget build(BuildContext context) {
    print (_currentIndex);
    return Scaffold(
      bottomNavigationBar: BottomNavigationBar(
        onTap: onTabTapped,
        currentIndex: _currentIndex,
        fixedColor: UIData.joltBlueAccent,
        items: <BottomNavigationBarItem>[
          BottomNavigationBarItem(
              icon: new Icon(FontAwesomeIcons.solidCalendarCheck),
              title: new Text("Completed", style: TextStyle(fontSize: UIData.percentHeight(context, 20), fontFamily: "WorkSansBold"),)),
          BottomNavigationBarItem(
              icon: new Icon(FontAwesomeIcons.solidCalendar), 
              title: new Text("Lists", style: TextStyle(fontSize: UIData.percentHeight(context, 20), fontFamily: "WorkSansBold"))),
          // BottomNavigationBarItem(
          //     icon: new Icon(FontAwesomeIcons.slidersH), title: new Text(""))
        ],
      ),
      body: _children.length == 0 ? Container(
          child: Padding(
              padding: EdgeInsets.symmetric(vertical: UIData.percentHeight(context, 5), horizontal: UIData.percentWidth(context, 5)),
              child: Center(
                child: CircularProgressIndicator()
              ),
            )
          ) : _children[_currentIndex],
    );
  }

  @override
  void dispose() {
    super.dispose();
  }

  SessionManager _sessionManager;

  Future<Object> initialisePrefs() async {
    SharedPreferences prefs = await SharedPreferences.getInstance();
    SessionManager sessionManager = new SessionManager(prefs);
    return sessionManager;
  }

  @override
  void initState() {
    super.initState();

    SystemChrome.setPreferredOrientations([
      DeviceOrientation.portraitUp,
      DeviceOrientation.portraitDown,
    ]);
    
    
    initialisePrefs().then((sessionManager){
      setState(() {
            _sessionManager = sessionManager;
        });
    });
    print (_currentIndex);
    print (_currentIndex);
    print (_children);
    getCurrentUser().then((cUser){
      setState(() {
              _currentUser = cUser;
              _children = [
                DonePage(
                  user: _currentUser,
                  sessionManager: _sessionManager,
                ),
                TaskPage(
                  user: _currentUser,
                  sessionManager: _sessionManager,
                ),
                SettingsPage(
                  user: _currentUser,
                )
              ];
            });
      //print(_currentUser.uid);
      print (_children);
      //print("victor");
    });
  }

  void onTabTapped(int index) {
    setState(() {
      _currentIndex = index;
    });
  }
}