import 'dart:math';

import 'package:firebase_database/firebase_database.dart';
import 'package:flutter/material.dart';
import 'package:flutter_custom_clippers/flutter_custom_clippers.dart';
import 'package:fluttertoast/fluttertoast.dart';
import 'package:font_awesome_flutter/font_awesome_flutter.dart';
import 'package:jolt/models/assessment.dart';
import 'package:jolt/views/common_scaffold.dart';
import 'package:jolt/ui/infograph_page.dart';
import 'package:jolt/auth/jolt_auth.dart';
import 'package:jolt/utils/career_session_manager.dart';
import 'package:jolt/utils/uidata.dart';
import 'package:shared_preferences/shared_preferences.dart';

class AssessmentPage extends StatefulWidget {

  final AuthImpl auth;
  AssessmentPage({this.auth});

  @override
  _AssessmentPageState createState() => _AssessmentPageState();
}

class _AssessmentPageState extends State<AssessmentPage> {

  Size deviceSize;
  bool _showDialog = true;
  bool hasLikedPost = false;

  SessionManager _sessionManager;
  IconData iconData = FontAwesomeIcons.heart;

  QNumber selectedNumber;
  QNumber qNumber;
  List<QNumber> qNumbers = [QNumber("1"), QNumber("2"), QNumber("3"), QNumber("4")];

  List<Assessment> assessmentList = [];

  List<Map<String,String>> answers = [];

  int _index = 0;

  Future<Object> initialisePrefs() async {
    SharedPreferences prefs = await SharedPreferences.getInstance();
    SessionManager sessionManager = new SessionManager(prefs);
    return sessionManager;
  }

  @override
  void initState() {
    super.initState();
    initialisePrefs().then((sessionManager){
      if(sessionManager != null){
        setState(() {
            _sessionManager = sessionManager;
        });
      }
    });

    getAssessmentTestLists(widget.auth).then((retVal){
      if(retVal != null){
        setState(() {
          _showDialog = false;
          assessmentList = retVal;
          assessmentList.sort((a, b) => a.id.compareTo(b.id));
        });
      }
    });

    
  }


  @override
  Widget build(BuildContext context){
    return WillPopScope(
      onWillPop: _onWillPop,
      child: CommonScaffold(
        appTitle: "Assessment Test",
        backGroundColor: Colors.white,
        actionFirstIcon: null,
        bColor: UIData.joltBlueAccent,
        bodyData: _showDialog ? new Center(
          child: CircularProgressIndicator(),
        ) : Stack(
          children: <Widget>[
            ClipPath(
              clipper: OvalBottomBorderClipper(),
              child: Container(
                decoration: BoxDecoration(
                  color: Theme.of(context).primaryColor
                ),
                height: UIData.percentHeight(context, 250),
              ),
            ),
            Padding(
              padding: EdgeInsets.symmetric(horizontal: UIData.percentWidth(context, 16), vertical: UIData.percentHeight(context, 16)),
              child: Column(
                children: <Widget>[
                  Row(
                    children: <Widget>[
                      CircleAvatar(
                        backgroundColor: Colors.white70,
                        child: Text(assessmentList.elementAt(_index).id.toString(),),
                      ),
                      SizedBox(width: 16.0),
                      Expanded(
                        child: Text(assessmentList.elementAt(_index).topQuestion,
                          softWrap: true,
                          style: TextStyle(
                            fontFamily: "WorkSansBold",
                            fontSize: UIData.percentHeight(context, 25),
                            color: Colors.white
                          ),),
                      ),
                    ],
                  ),

                  SizedBox(height: UIData.percentHeight(context, 20)),
                  Card(
                    child: Column(
                      mainAxisSize: MainAxisSize.min,
                      children: createRadioListUsers()
                    ),
                  ),
                  SizedBox(height: UIData.percentHeight(context, 20)),
                  Row(
                    children: <Widget>[
                      CircleAvatar(
                        backgroundColor: UIData.joltBlueAccent,
                        child: Text(assessmentList.elementAt(_index).id.toString(), style: TextStyle(color: Colors.white),),
                      ),
                      SizedBox(width: 16.0),
                      Expanded(
                        child: Text(assessmentList.elementAt(_index).bottomQuestion,
                          softWrap: true,
                          style: TextStyle(
                            fontFamily: "WorkSansBold",
                            fontSize: UIData.percentHeight(context, 25),
                            color: UIData.joltBlueAccent
                          ),),
                      ),
                    ],
                  ),
                  Expanded(
                    child: Container(
                      alignment: Alignment.bottomCenter,
                      child: new RaisedButton(
                        shape: new RoundedRectangleBorder(borderRadius: new BorderRadius.circular(7.0)),
                        child: Text(
                          _index != 13 ? "Next":"Submit",
                          style: TextStyle(color: Colors.white, fontFamily: "WorkSansSemiBold", fontSize: UIData.percentHeight(context, 18)),
                        ),
                        color: UIData.joltBlueAccent,
                        elevation: UIData.percentHeight(context, 8),
                        splashColor: UIData.joltBlueAccent,
                        onPressed: (){
                          if(selectedNumber == null){
                            Fluttertoast.showToast(
                              msg: "   Please select a range   ",
                              toastLength: Toast.LENGTH_SHORT,
                              gravity: ToastGravity.CENTER,
                              timeInSecForIos: 1,
                              backgroundColor: Colors.red,
                              textColor: Colors.white,
                            );
                          }
                          else{
                            setState((){
                              answers.add({assessmentList.elementAt(_index).id.toString() : selectedNumber.number.toString()});
                              if(_index == 13){
                                print (answers);
                                FirebaseDatabase.instance.reference().child("activities").child(_sessionManager.getSessionDetails()['user_id']).push().child(new DateTime.now().millisecondsSinceEpoch.toString()).set("assessment_test_update");
                                String val = getAssessmentResults(answers);
                                Navigator.of(context).pop();
                                Navigator.push(
                                  context,
                                  new MaterialPageRoute(
                                    builder: (context) => new Infography(personality: val,)
                                  ),
                                );
                              }
                              else{
                                _index += 1;
                              }
                              selectedNumber = null;
                            });
                          }
                        },
                      ),
                    ),
                  )
                ],
              ),
            )
          ],
        ),
      ),
    );
  }

  // void _nextSubmit() {
  //   if(_answers[_currentIndex] == null) {
  //     _key.currentState.showSnackBar(SnackBar(
  //       content: Text("You must select an answer to continue."),
  //     ));
  //     return;
  //   }
  //   if(_currentIndex < (widget.questions.length - 1)){
  //     setState(() {
  //         _currentIndex++;
  //     });
  //   } else {
  //     Navigator.of(context).pushReplacement(MaterialPageRoute(
  //       builder: (_) => QuizFinishedPage(questions: widget.questions, answers: _answers)
  //     ));
  //   }
  // }

  Future<List<Assessment>> getAssessmentTestLists(AuthImpl authImpl) async{
    return authImpl.grabAssessments(context);
  }

  void setSelectedNumber(QNumber number) {
    setState(() {
      selectedNumber = number;
    });
  }

  String getAssessmentResults(List<Map<String, String>> answers){
    int j = 7;
    List<Map<String, String>> results = [];
    List<String> personalityList = [];
    for (int i = 0; i < (answers.length)/2; i++){
      results.add({answers.elementAt(i).values.elementAt(0) : answers.elementAt(j).values.elementAt(0)});
      j += 1;
    }
    print (results);
    for(int i = 0; i < results.length; i++){
      if((results.elementAt(i).keys.elementAt(0) == "3" || results.elementAt(i).keys.elementAt(0) == "4") && (results.elementAt(i).values.elementAt(0) == "1" || results.elementAt(i).values.elementAt(0) == "2")){
        print ("Leader");
        personalityList.add("Leader");
      }
      else if ((results.elementAt(i).keys.elementAt(0) == "1" || results.elementAt(i).keys.elementAt(0) == "2") && (results.elementAt(i).values.elementAt(0) == "1" || results.elementAt(i).values.elementAt(0) == "2")){
        print ("Diligent");
        personalityList.add("Diligent");
      }
      else if ((results.elementAt(i).keys.elementAt(0) == "1" || results.elementAt(i).keys.elementAt(0) == "2") && (results.elementAt(i).values.elementAt(0) == "3" || results.elementAt(i).values.elementAt(0) == "4")){
        print ("Stable");
        personalityList.add("Stable");
      }
      else if ((results.elementAt(i).keys.elementAt(0) == "3" || results.elementAt(i).keys.elementAt(0) == "4") && (results.elementAt(i).values.elementAt(0) == "3" || results.elementAt(i).values.elementAt(0) == "4")){
        print ("Visionary");
        personalityList.add("Visionary");
      }
    }
    var map = Map();
    personalityList.forEach((x) => map[x] = !map.containsKey(x) ? (1) : (map[x] + 1));
    //print(map);
    List<int> xval = [];
    map.forEach((key, value){
      xval.add(value);
    });
    int xxval = xval.reduce(max);
    List<String> personality = [];
    map.forEach((key, value){
      if(value == xxval){
        personality.add(key);
        return;
      }
    });
    print ("Your personality is ${personality[0]}");
    return personality[0];
  }
  
  List<Widget> createRadioListUsers() {
    List<Widget> widgets = [];
    for (QNumber qNumber in qNumbers) {
      widgets.add(
        RadioListTile(
          value: qNumber,
          groupValue: selectedNumber,
          title: Text(qNumber.number),
          onChanged: (currentNumber) {
            //print("Current Number ${currentNumber.number}");
            setSelectedNumber(currentNumber);
          },
          selected: selectedNumber == qNumber,
        ),
      );
    }
    return widgets;
  }

  Future<bool> _onWillPop() async {
    return showDialog<bool>(
      context: context,
      builder: (_) {
        return AlertDialog(
          content: Text("Are you sure you want to quit? All your progress will be lost.", style: TextStyle(fontFamily: "WorkSansMedium", fontSize: UIData.percentHeight(context, 20))),
          title: Text("Warning!", style: TextStyle(color: Colors.redAccent, fontFamily: "WorkSansBold", fontSize: UIData.percentHeight(context, 25))),
          actions: <Widget>[
            FlatButton(
              child: Text("Yes", style: TextStyle(fontFamily: "WorkSansSemiBold", fontSize: UIData.percentHeight(context, 20)),),
              onPressed: (){
                Navigator.pop(context,true);
              },
            ),
            FlatButton(
              child: Text("No", style: TextStyle(fontFamily: "WorkSansSemiBold", fontSize: UIData.percentHeight(context, 20))),
              onPressed: (){
                Navigator.pop(context,false);
              },
            ),
          ],
        );
      }
    );
  }
}

class QNumber{
  String number;

  QNumber(
    this.number
  );
}
