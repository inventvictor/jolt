import 'package:flutter/material.dart';
import 'package:jolt/views/intro_views.dart';
import 'package:jolt/ui/home.dart';
import 'package:jolt/ui/my_profile_page.dart';
import 'package:jolt/views/root_page.dart';
import 'package:jolt/ui/new_employee_page.dart';
import 'package:jolt/auth/jolt_auth.dart';
import 'package:jolt/ui/career_planner.dart';

void main() => runApp(new MyApp());

class MyApp extends StatelessWidget {
  Map<int, Color> color = {
    50:Color.fromRGBO(36,111,132, .1),
    100:Color.fromRGBO(36,111,132, .2),
    200:Color.fromRGBO(36,111,132, .3),
    300:Color.fromRGBO(36,111,132, .4),
    400:Color.fromRGBO(36,111,132, .5),
    500:Color.fromRGBO(36,111,132, .6),
    600:Color.fromRGBO(36,111,132, .7),
    700:Color.fromRGBO(36,111,132, .8),
    800:Color.fromRGBO(36,111,132, .9),
    900:Color.fromRGBO(36,111,132, 1),
  };
  @override
  Widget build(BuildContext context) {
    return new MaterialApp(
      title: 'JOLT',
      debugShowCheckedModeBanner: false,
      theme: new ThemeData(
        primarySwatch: MaterialColor(0xFF246F84, color),
        platform: TargetPlatform.iOS,
      ),
      home: RootPage(),
      routes: <String, WidgetBuilder> {
        '/intro_views': (BuildContext context) => new JolTCareerIntros(),
        '/home' : (BuildContext context) => new Home(showWelcome: false,),
        '/career_planner' : (BuildContext context) => new CareerPlannerPage(showDialog_: false,),
        '/my_profile': (BuildContext context) => new MyProfilePage(auth: new Auth(),),
        '/new_employee': (BuildContext context) => new NewEmployeePage(),
        //'/view_photo': (BuildContext context) => new ViewPhotoPage()
      },
    );
  }
}

