import 'dart:async';
import 'package:firebase_auth/firebase_auth.dart';
import 'package:firebase_database/firebase_database.dart';
import 'package:jolt/models/assessment.dart';
import 'package:jolt/models/career_advice.dart';
import 'package:jolt/models/career_path.dart';
import 'package:jolt/models/job_model.dart';
import 'package:jolt/utils/uidata.dart';
import 'package:simple_gravatar/simple_gravatar.dart';
import 'package:jolt/views/activity_tile.dart';
import 'package:intl/intl.dart';
import 'package:flutter/material.dart';
import 'package:font_awesome_flutter/font_awesome_flutter.dart';
import 'package:googleapis/jobs/v2.dart';
import 'package:googleapis_auth/auth_io.dart';

abstract class AuthImpl {
  Future<dynamic> signIn(String email, String password);

  Future<dynamic> signUp(String name, String email, String password);

  Future<dynamic> getCurrentUser();

  Future<void> testing123(bool t);

  Future<void> signOut();

  Future<List<ActivityTile>> grabActivities(BuildContext context, String user_id);

  Future<List<CareerPath>> grabCareerPath();

  Future<List<CareerAdvice>> grabCareerAdvice();

  Future<int> likePost(int postId, String userId, String postTitle, {bool isCareerPath = true});
  
  Future<int> hasLikedPost(int postId, String userId, {bool isCareerPath = true});

  Future<int> disLikePost(int postId, String userId, String postTitle, {bool isCareerPath = true});

  Future<List<Assessment>> grabAssessments(BuildContext context);

  Future<List<String>> grabJobSections(BuildContext context);

  Future<Map<dynamic, dynamic>> grabJobsBySection(BuildContext context, String section);

  Future<Map<dynamic, dynamic>> getJobsSearchBySection(BuildContext context, String section, String query);
}

class Auth implements AuthImpl {
  final FirebaseAuth _firebaseAuth = FirebaseAuth.instance;

  Future<dynamic> signIn(String email, String password) async {
    dynamic retVal;
    FirebaseUser user;
    try{
      await _firebaseAuth.signInWithEmailAndPassword(
        email: email, password: password).then((retV){
          user = retV.user;
        });
      retVal = await getDBRefFromRTDB();
    }
    catch(e){
      retVal = e;
    }
    
    return retVal;
  }

  Future<DatabaseReference> addUserToRTDB(String name, FirebaseUser user, String avatar) async{
    final userReference = FirebaseDatabase.instance.reference().child("users");
    userReference.child(user.uid).set({
      'name': name,
      'email': user.email,
      'avatar': avatar,
      'phone': "",
      'user_id': user.uid,
      'desc': "",
      'city': "",
      'country': "",
      'age': "",
      'business_connects': "",
      'session': "career",
      'has_updated_profile': false
    });
    return userReference;
  }

  Future<DatabaseReference> getDBRefFromRTDB() async{
    DatabaseReference databaseReference = FirebaseDatabase.instance.reference().child("users");
    return databaseReference;
  }
  
  Future<dynamic> signUp(String name, String email, String password) async {
    dynamic retVal;
    FirebaseUser user;
    try{
      List<String> providers = await _firebaseAuth.fetchSignInMethodsForEmail(email: email);
      if(providers != null && providers.length > 0){
        retVal = "You are already registered";
      }
      else{
        await _firebaseAuth.createUserWithEmailAndPassword(
        email: email, password: password).then((retV){
          user = retV.user;
        });
        dynamic imgUrl = await getGravatar(email);
        retVal = await addUserToRTDB(name, user, imgUrl as String);
      }
    }
    catch(e){
      retVal = e;
    }
    
    return retVal;
  }

  Future<dynamic> getGravatar(String email) async{
    dynamic gravatar = Gravatar(email);
    dynamic url = await gravatar.imageUrl(
      size: 100,
      defaultImage: GravatarImage.retro,
      rating: GravatarRating.pg,
      fileExtension: true,
    );
    return url;
  }

  Future<dynamic> getCurrentUser() async {
    dynamic retVal;
    try{
      FirebaseUser user = await _firebaseAuth.currentUser();
      retVal = user;
    }
    catch(e){
      retVal = e;
    }
    
    return retVal;
  }

  Future<void> testing123(bool t) async{
    t = false;
    new Future.delayed(new Duration(seconds: 5), (){
      
    });
  }

  Future<void> signOut() async {
    return _firebaseAuth.signOut();
  }

  Future<List<CareerPath>> grabCareerPath() async{
    List<CareerPath> careerPaths = [];
    await FirebaseDatabase.instance.reference().child("career_path").limitToFirst(5).once().then((DataSnapshot snapshot) {
      Map<dynamic, dynamic> values = snapshot.value;
      if(values != null){
        values.forEach((key, value){
          careerPaths.add(new CareerPath(value["id"], value["title"], value["content"], value["img"], value["excerpt"], value["likes"]));
        });
      }
    });
    return careerPaths;
  }

  Future<List<CareerAdvice>> grabCareerAdvice() async{
    List<CareerAdvice> careerAdvice = [];
    await FirebaseDatabase.instance.reference().child("career_advice").once().then((DataSnapshot snapshot) {
      Map<dynamic, dynamic> values = snapshot.value;
      if(values != null){
        values.forEach((key, value){
          careerAdvice.add(new CareerAdvice(value["id"], value["title"], value["content"], value["img"], value["excerpt"], value["likes"]));
        });
      }
    });
    return careerAdvice;
  }

  Future<int> likePost(int postId, String userId, String postTitle, {bool isCareerPath = true}) async{
    int prevLikes = 0;
    if(isCareerPath){
      await FirebaseDatabase.instance.reference().child("posts_liked").child(userId).child(postTitle).set(postId);
      await FirebaseDatabase.instance.reference().child("career_path").child("Post $postId").once().then((DataSnapshot snapshot){
        Map<dynamic, dynamic> values = snapshot.value;
        if(values != null){
          values.forEach((key, value){
            if(key == "likes"){
              prevLikes = value;
            }
          });
        }
      });
      await FirebaseDatabase.instance.reference().child("career_path").child("Post $postId").child("likes").set(prevLikes+1);
      return 1;
    }
    else{
      await FirebaseDatabase.instance.reference().child("posts_liked_career_advice").child(userId).child(postTitle).set(postId);
      await FirebaseDatabase.instance.reference().child("career_advice").child("Post $postId").once().then((DataSnapshot snapshot){
        Map<dynamic, dynamic> values = snapshot.value;
        if(values != null){
          values.forEach((key, value){
            if(key == "likes"){
              prevLikes = value;
            }
          });
        }
      });
      await FirebaseDatabase.instance.reference().child("career_advice").child("Post $postId").child("likes").set(prevLikes+1);
      return 1;
    }
    
  }

  Future<int> disLikePost(int postId, String userId, String postTitle, {bool isCareerPath = true}) async{
    int prevLikes = 0;
    if(isCareerPath){
      await FirebaseDatabase.instance.reference().child("posts_liked").child(userId).child(postTitle).remove();
      await FirebaseDatabase.instance.reference().child("career_path").child("Post $postId").once().then((DataSnapshot snapshot){
        Map<dynamic, dynamic> values = snapshot.value;
        if(values != null){
          values.forEach((key, value){
            if(key == "likes"){
              prevLikes = value;
            }
          });
        }
      });
      await FirebaseDatabase.instance.reference().child("career_path").child("Post $postId").child("likes").set(prevLikes-1);
      return 1;
    }
    else{
      await FirebaseDatabase.instance.reference().child("posts_liked_career_advice").child(userId).child(postTitle).remove();
      await FirebaseDatabase.instance.reference().child("career_advice").child("Post $postId").once().then((DataSnapshot snapshot){
        Map<dynamic, dynamic> values = snapshot.value;
        if(values != null){
          values.forEach((key, value){
            if(key == "likes"){
              prevLikes = value;
            }
          });
        }
      });
      await FirebaseDatabase.instance.reference().child("career_advice").child("Post $postId").child("likes").set(prevLikes-1);
      return 1;
    }
    
  }

  Future<int> hasLikedPost(int postId, String userId, {bool isCareerPath = true}) async{
    int retVal = 0;
    if(isCareerPath){
      await FirebaseDatabase.instance.reference().child("posts_liked").child(userId).once().then((DataSnapshot snapshot){
          Map<dynamic, dynamic> values = snapshot.value;
          if (values != null){
            values.forEach((key, val){
                if(val == postId){
                  retVal = 1;
                }
            });
          }
      });
      return retVal;
    }
    else{
      await FirebaseDatabase.instance.reference().child("posts_liked_career_advice").child(userId).once().then((DataSnapshot snapshot){
          Map<dynamic, dynamic> values = snapshot.value;
          if (values != null){
            values.forEach((key, val){
                if(val == postId){
                  retVal = 1;
                }
            });
          }
      });
      return retVal;
    }
  }

  Future<List<Assessment>> grabAssessments(BuildContext context) async{
    List<Assessment> assessmentList = new List<Assessment>();
    await FirebaseDatabase.instance.reference().child("assessment_test").once().then((DataSnapshot snapshot){
          Map<dynamic, dynamic> values = snapshot.value;
          if (values != null){
            values.forEach((key, val){
              String topQ = val.toString().replaceAll('{', '').replaceAll('}', '').split(',').elementAt(0).split(':').elementAt(1).toString();
              String bottomQ = val.toString().replaceAll('{', '').replaceAll('}', '').split(',').elementAt(1).split(':').elementAt(1).toString();
              assessmentList.add(new Assessment(int.parse(key.toString().replaceAll('Q', '')), topQ, bottomQ, {"1": false, "2": false, "3": false, "4": false}));
                
            });
          }
      });
    return assessmentList;
  }
  
  Future<List<ActivityTile>> grabActivities(BuildContext context, String user_id) async{
    List<ActivityTile> activityTiles = [];
    await FirebaseDatabase.instance.reference().child("activities").child(user_id).once().then((DataSnapshot snapshot){
            Map<dynamic, dynamic> values = snapshot.value;
            if (values != null){
              values.forEach((key,val){
                String k = val.toString().replaceAll('{', '').replaceAll('}', '').split(':').removeAt(0).toString();
                String v = val.toString().replaceAll('{', '').replaceAll('}', '').split(':').removeAt(1).toString();
                if(v == " profile_update"){
                  final f = new DateFormat('yyyy/MM/dd hh:mm');
                  activityTiles.add(new ActivityTile(activityIcon: Icon(FontAwesomeIcons.solidUser, color: Colors.deepPurple, size: UIData.percentHeight(context, 60),),
                                      activityTitle: 'Profile Update',
                                      activitySubtitle: 'You updated your profile 😁',
                                      activitySubtitleColor: Colors.black,
                                      activityTitleColor: Colors.deepPurpleAccent,
                                      timestamp: f.format(DateTime.fromMillisecondsSinceEpoch(int.parse(val.toString().replaceAll('{', '').replaceAll('}', '').split(':').removeAt(0))))
                                    )
                    
                  );
                }
                else if(v == " careerchecklist_update"){
                  final f = new DateFormat('yyyy/MM/dd hh:mm');
                  activityTiles.add(new ActivityTile(activityIcon: Icon(FontAwesomeIcons.solidCheckCircle, color: Colors.deepOrange, size: UIData.percentHeight(context, 60)),
                                      activityTitle: 'Career Checklist Update',
                                      activitySubtitle: 'You updated your career checklist 👊',
                                      activitySubtitleColor: Colors.black,
                                      activityTitleColor: Colors.deepOrangeAccent,
                                      timestamp: f.format(DateTime.fromMillisecondsSinceEpoch(int.parse(val.toString().replaceAll('{', '').replaceAll('}', '').split(':').removeAt(0))))
                                    )
                    
                  );
                }
                else if(v == " careerplan_update"){
                  final f = new DateFormat('yyyy/MM/dd hh:mm');
                  activityTiles.add(new ActivityTile(activityIcon: Icon(FontAwesomeIcons.solidCalendarCheck, color: UIData.joltBlue, size: UIData.percentHeight(context, 60)),
                                      activityTitle: 'Career Plan Update',
                                      activitySubtitle: 'You updated your career plan 🥳',
                                      activitySubtitleColor: Colors.black,
                                      activityTitleColor: UIData.joltBlueAccent,
                                      timestamp: f.format(DateTime.fromMillisecondsSinceEpoch(int.parse(val.toString().replaceAll('{', '').replaceAll('}', '').split(':').removeAt(0))))
                                    )
                    
                  );
                }
                else if(v == " profilephoto_update"){
                  final f = new DateFormat('yyyy/MM/dd hh:mm');
                  activityTiles.add(new ActivityTile(activityIcon: Icon(FontAwesomeIcons.solidImage, color: Colors.red, size: UIData.percentHeight(context, 60)),
                                      activityTitle: 'Profile Photo Update',
                                      activitySubtitle: 'You updated your profile photo 😊',
                                      activitySubtitleColor: Colors.black,
                                      activityTitleColor: Colors.redAccent,
                                      timestamp: f.format(DateTime.fromMillisecondsSinceEpoch(int.parse(val.toString().replaceAll('{', '').replaceAll('}', '').split(':').removeAt(0))))
                                    )
                    
                  );
                }
                else if(v == " assessment_test_update"){
                  final f = new DateFormat('yyyy/MM/dd hh:mm');
                  activityTiles.add(new ActivityTile(activityIcon: Icon(FontAwesomeIcons.award, color: Colors.red, size: UIData.percentHeight(context, 60)),
                                      activityTitle: 'Assessment Test',
                                      activitySubtitle: 'Great! You took the Basic Assessment Test 🏆',
                                      activitySubtitleColor: Colors.black,
                                      activityTitleColor: Colors.redAccent,
                                      timestamp: f.format(DateTime.fromMillisecondsSinceEpoch(int.parse(val.toString().replaceAll('{', '').replaceAll('}', '').split(':').removeAt(0))))
                                    )
                    
                  );
                }
                else if(v == " disc_assessment_test_update"){
                  final f = new DateFormat('yyyy/MM/dd hh:mm');
                  activityTiles.add(new ActivityTile(activityIcon: Icon(FontAwesomeIcons.solidHeart, color: Colors.red, size: UIData.percentHeight(context, 60)),
                                      activityTitle: 'DISC Assessment Test',
                                      activitySubtitle: 'Thank you! You purchased a DISC Assessment Test. 👍',
                                      activitySubtitleColor: Colors.black,
                                      activityTitleColor: Colors.indigo,
                                      timestamp: f.format(DateTime.fromMillisecondsSinceEpoch(int.parse(val.toString().replaceAll('{', '').replaceAll('}', '').split(':').removeAt(0))))
                                    )
                    
                  );
                }
                activityTiles.sort((b, a) => a.timestamp.compareTo(b.timestamp));
              });
            }
            else{
              activityTiles = [];
            }
      });
    return activityTiles;
    
  }

  Future<List<String>> grabJobSections(BuildContext context) async{
    List<String> jobSections = [];
    await FirebaseDatabase.instance.reference().child("jobs").once().then((DataSnapshot snapshot){
            Map<dynamic, dynamic> values = snapshot.value;
            if (values != null){
              values.forEach((key,val){
                jobSections.add(key);
              });
            }
            else{
              jobSections = [];
            }
      });
    return jobSections;
  }

  Future<Map<dynamic, dynamic>> grabJobsBySection(BuildContext context, String section) async{
    Map<dynamic, dynamic> jobs = {};
    await FirebaseDatabase.instance.reference().child("jobs").child(section).once().then((DataSnapshot snapshot){
            Map<dynamic, dynamic> values = snapshot.value;
            if (values != null){
              jobs = values;
            }
            else{
              jobs = {};
            }
      });
    return jobs;
  }

  Future<Map<dynamic, dynamic>> getJobsSearchBySection(BuildContext context, String section, String query) async{
    Map<dynamic, dynamic> jobs = {};
    await FirebaseDatabase.instance.reference().child("jobs").child(section).once().then((DataSnapshot snapshot){
            Map<dynamic, dynamic> values = snapshot.value;
            if (values != null){
              values.forEach((key,val){
                if(val['location'].toString().toLowerCase().contains(query.toLowerCase()) || val['title'].toString().toLowerCase().contains(query.toLowerCase())){
                  jobs[key] = val;
                }
              });
            }
            else{
              jobs = {};
            }
      });
    return jobs;
  }

  // Future<Map<dynamic, dynamic>> fetchJobs(BuildContext context) async{

  //   final _credentials = new ServiceAccountCredentials.fromJson(r'''
  //   {
  //     "type": "service_account",
  //     "project_id": "jolt-f19ca",
  //     "private_key_id": "c525fcd58b1e7d3323c00131e794285a3d2f55d8",
  //     "private_key": "-----BEGIN PRIVATE KEY-----\nMIIEvQIBADANBgkqhkiG9w0BAQEFAASCBKcwggSjAgEAAoIBAQCPF/yLr2SE+AFd\nXoMGWkgci8Stb1O9D2NCGsyNP+jHEhtbcV2H681NrbWIXkLpKeNgIi3dl5rvCeL6\njbWoHDO/vNm6D6lPi6yAkFV5ecGjnXgCgBx0ZoUZrjPdftn4a9gIh7Pab8Sz4F2f\n5W05V0nrFM0c5Sf/M+PLp6R0LaxCWnWECIIO5MXrMROqC6/nxYqcrMpKNHxPe+/c\nX9keEuILCf0C4oErkKIMurrAP8A3m6CbULwQLlnABdHOBs1iK0/gu9CUvGHRejO4\nXzW+v3nuB4P+0CDE6cGDSq4FG+W+60VG2WcT+/LRdbwDEAqD1r1wIdUW+Ndg4dvS\nz7pUOBkLAgMBAAECggEANJPTMz40eJkp4zVVfnGQ2pBOqY1k3ux3A7SWIXZwoSjZ\njX13qPgvHRItSBiejE7cYZfdc3T/tEJVOMv2Cfmm2BZbh/62Lui67xhZl8Nxr50u\nGfkZu3lqAZgdLY8tJxb1L5kxMBtuUsunixeBTRbyLaMchLf8vMxDsLJ9/kRD7bMD\nncOQW5pCCE3i/g+aV4nIp1nP/p+zwjbphhCy0VEw6Y00keGWBjjGtwT1/Cn2Aj13\ncK2mqgCvXZMGfjmJF9Gl0gcui0hf5QBCXKHGKqhQkN3HldCvyNqmqxtfPGod74Yf\nUmPbvD+TOybVwuMYxGlI0Ea13CKS1n7USD1SRPynIQKBgQDCCHv1g9oqTnASuF8Q\n+O39yOaiNWCbR3Kqdudi4PivXkqamQ9Cjkdb5THc71ZYR6YCU/GQ6nhPjL46RRgV\nx+fvnBOk0MxXVuu6Tioy3coucGW2RfRT+ms3ckzECjFYyeUdwa4XB+InX8JKDgCo\ncTDyAA54uQ6Oh1pNz76aRjseoQKBgQC8ytwYHwYarGiwUavZ4r91TKTG30GDNrBG\nwIcxP6NC14v6FcZW7Js2lQmXjJDIuagRhyw7U3lPxa4w6prFeOflDY0SXkeNstc9\nWyxi5hPELAprJn+k3aWQxZSSr5YhoVohXrm9g30vKRyaSfix4+83IRegsalh7s+e\nkFCPcsl0KwKBgQCHn0aFpVOonorXiudoBZbT/Mi4OHb+EkKjw9iX1If+i/m5daKk\n8q4OHXVs6B2CNxJiLip7EGhnsgiE0iazkrcbVAym9wrdQXQjFN+KDlxzeTye2uac\njDzE1K+zlyxG01fCJM56apg4zUYQXRk4o2WCwWdcGn2CQjlsifi//Lb2AQKBgG7Z\nbYF8NclTkeR1D8JPMLte1SEv1PeGFNp2EOsJwoJsrPOZ9Wr0Ye4adBdXoWs6F0Sz\naXUr/f6owy5VkaqA4yfKXRp74OeCKn8MGXO4Q8LbWQKxCCF1QtFOMX4vOqdkZddS\nFxz/J3GZYgH1jOBmRP7Jn2Xq6tllruIbOjH/4+khAoGAZVIF80zLVQ0bRzZeUNBs\nEjlLcqBQRrrDc7Brc13Gl/1ZTdli63nbCb+89CA+ylTG6rytRsAS5MGe8kixVl0d\nnY0zzvib+xQyfyimF62l99LkHBtEE08vik8SfvF3zuvrzNJg5mJ5Uhs+mfc5ZI49\n4NgwrB6aGFZQOI1YWFPp0MQ=\n-----END PRIVATE KEY-----\n",
  //     "client_email": "jolt-472@jolt-f19ca.iam.gserviceaccount.com",
  //     "client_id": "116481772164585558502",
  //     "auth_uri": "https://accounts.google.com/o/oauth2/auth",
  //     "token_uri": "https://oauth2.googleapis.com/token",
  //     "auth_provider_x509_cert_url": "https://www.googleapis.com/oauth2/v1/certs",
  //     "client_x509_cert_url": "https://www.googleapis.com/robot/v1/metadata/x509/jolt-472%40jolt-f19ca.iam.gserviceaccount.com"
  //   }
  //   ''');

  //   const _SCOPES = const [JobsApi.JobsScope];

  //   clientViaServiceAccount(_credentials, _SCOPES).then((http_client) {
  //     var jobsList = new JobsApi(http_client);
  //     jobsList.jobs.list()
  //     storage.buckets.list('dart-on-cloud').then((buckets) {
  //       print("Received ${buckets.items.length} bucket names:");
  //       for (var file in buckets.items) {
  //         print(file.name);
  //       }
  //     });
  //   });
  // }
}